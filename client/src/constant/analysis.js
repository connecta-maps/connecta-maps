import TypeStatic from '../module/ToolAnalyses/AnalysisForm/RendererTab/AnalysisType/TypeStatic';
import TypeThematic from '../module/ToolAnalyses/AnalysisForm/RendererTab/AnalysisType/TypeThematic';
import TypeChart from '../module/ToolAnalyses/AnalysisForm/RendererTab/AnalysisType/TypeChart';
import TypeCluster from '../module/ToolAnalyses/AnalysisForm/RendererTab/AnalysisType/TypeCluster';
import TypeHeatmap from '../module/ToolAnalyses/AnalysisForm/RendererTab/AnalysisType/TypeHeatmap';
import {locale} from '../i18n/Locale';
import SimpleRendererLegend from "../module/ToolAnalyses/AnalysisBox/SimpleRendererLegend";
import ThematicLegend from "../module/ToolAnalyses/AnalysisBox/ThematicLegend";
import ClusterLegend from "../module/ToolAnalyses/AnalysisBox/ClusterLegend";
import HeatmapLegend from "../module/ToolAnalyses/AnalysisBox/HeatmapLegend";
import ChartLegend from "../module/ToolAnalyses/AnalysisBox/ChartLegend";

export const ANALYSIS = {

    DOWNLOAD_FILE : 'DOWNLOAD_FILE'

};

export const ANALYSIS_TYPE = {
    STATIC : 'simpleRenderer',
    THEMATIC : 'thematic',
    CLUSTER : 'cluster',
    CHART : 'chart',
    HEATMAP : 'heatmap'
};

export const GEOMETRY_TYPE = {
    POINT: "esriGeometryPoint",
    MULTIPOINT: "esriGeometryMultipoint",
    LINE: "esriGeometryLine",
    PATH: "esriGeometryPath",
    POLYLINE: "esriGeometryPolyline",
    POLYGON: "esriGeometryPolygon"
};

export const OPERATION_NAME = {
    QUERY : 'query',
    GET_MAP : 'getMap'
};

export const ANALYSIS_TYPE_CONFIG = {
    defaultType : ANALYSIS_TYPE.STATIC,
    types : {
        [ANALYSIS_TYPE.STATIC] : {
            Class : TypeStatic,
            LegendComponent : SimpleRendererLegend
        },
        [ANALYSIS_TYPE.THEMATIC] : {
            Class : TypeThematic,
            LegendComponent : ThematicLegend
        },
       /* [ANALYSIS_TYPE.CHART] : {
            Class : TypeChart,
            LegendComponent : ChartLegend
        },*/
        [ANALYSIS_TYPE.CLUSTER] : {
            Class : TypeCluster,
            LegendComponent : ClusterLegend
        },
        [ANALYSIS_TYPE.HEATMAP] : {
            Class : TypeHeatmap,
            LegendComponent : HeatmapLegend
        }
    }
};

export const CLASSIFICATION_METHODS = [
    {
        name : 'EqInterval',
        alias : locale.getLabel('L0048')
    },
    {
        name : 'Quantile',
        alias : locale.getLabel('L0049')
    },
    {
        name : 'StdDeviation',
        alias : locale.getLabel('L0050')
    },
    {
        name : 'ArithmeticProgression',
        alias : locale.getLabel('L0051')
    },
    {
        name : 'GeometricProgression',
        alias : locale.getLabel('L0052')
    },
    {
        name : 'Jenks',
        alias : locale.getLabel('L0053')
    },
    {
        name : 'UniqueValues',
        alias : locale.getLabel('L0090')
    }
];

export const DEFAULT_ANALYSIS_CONFIG = {
    enable : true,
    type : 'ITEM',
    title : '',
    description : '',
    richLayerId : '',
    tags : [],
    refreshTime : 0,
    opacity : 0.75,
    currentType : ANALYSIS_TYPE_CONFIG.defaultType,
    filterConfig : [],
    visibleScaleRange : {},
    trackingConfig : {
        filterField : "",
        temporalDimensionField : "",
        colors : [
            "#1d9a7b",
            "#000"],
    },
    configRenderer : {
        simpleRenderer : {
            fill : '#1d9a7b',
            outlineColor : '#000',
            size : 4,
            customSymbology : {
                fileName : "",
                base64 : "",
                iconText: "",
                fontSize: 14,
                fontColor: "#000"
            }
        },
        thematic : {
            fill : {
                classificationMethod : '',
                classificationField : '',
                breakCount : 4,
                colorRamp : [
                    "#15c400",
                    "#ff1200"
                ]
            },
            customBreaks : [],
            size : {
                minSize : 10,
                maxSize : 60,
                classificationMethod : '',
                classificationField : '',
                breakCount : 4
            },
            uniqueValues : {
                field : "",
                colorRamp : [
                    "#15c400",
                    "#ff1200"
                ]
            }
        },
        cluster : {
            uniqueSize : false,
            outlineColor : "#000",
            colorRamp : [
                "#15c400",
                "#ff1200"
            ],
            breakCount : 4,
            size : 12,
            distance : 12
        },
        heatmap : {
            blurRadius : 10,
            colors : [
                '#15c400',
                '#ff1200'
            ],
            field : ''
        },
        /*chart : {
            size : 50,
            metric : '',
            dimension : ''
        }*/
    },
    outFieldsConfig : {}
};

Object.freeze(DEFAULT_ANALYSIS_CONFIG);