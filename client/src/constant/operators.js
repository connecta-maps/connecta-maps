import {locale} from '../i18n/Locale';

export const OPERATORS_NAMES = {
    EQUAL : "EQUAL",
    DIFFERENT : "DIFFERENT",
    GREATER : "GREATER",
    GREATER_OR_EQUAL : "GREATER_OR_EQUAL",
    LESS : "LESS",
    LESS_OR_EQUAL : "LESS_OR_EQUAL",
    LIKE : "LIKE",
    BETWEEN : "BETWEEN",
    NOT_BETWEEN : "NOT_BETWEEN",
    DISTINCT : "DISTINCT",
    IN : "IN"
};

const OPERATORS = [
    {
        name : OPERATORS_NAMES.EQUAL,
        alias : locale.getLabel('L0038')
    },
    {
        name : OPERATORS_NAMES.DIFFERENT,
        alias : locale.getLabel('L0039')
    },
    {
        name : OPERATORS_NAMES.GREATER,
        alias : locale.getLabel('L0040'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.GREATER_OR_EQUAL,
        alias : locale.getLabel('L0041'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.LESS,
        alias : locale.getLabel('L0042'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.LESS_OR_EQUAL,
        alias : locale.getLabel('L0043'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.LIKE,
        alias : locale.getLabel('L0044'),
        exclusion : ['number', 'date']
    },
    {
        name : OPERATORS_NAMES.BETWEEN,
        alias : locale.getLabel('L0045'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.NOT_BETWEEN,
        alias : locale.getLabel('L0046'),
        exclusion : ['string']
    },
    {
        name : OPERATORS_NAMES.IN,
        alias : locale.getLabel('L0061')
    }
];

export default OPERATORS;

