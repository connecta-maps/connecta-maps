export const GEOMETRY_TYPE = {

    POINT : "esriGeometryPoint",
    POLYGON : "esriGeometryPolygon"

};
