let data = {};
let listeners = {};
let registries = {}; // 0 = registry, mantém a quantidade de vezes que um data é valorado

export const MapsDataLoader = {

    /**
     * Registra uma intenção de posteriormente valorar dados neste DataLoader.
     * Este método é útil para garantir que uma chave seja registrada no DataLoader antes dela ser valorada.
     * Método também mantém a quantidade de vezes que uma chave é valorada.
     * @param {String} key
     * @returns {{set: (function(*=))}}
     */
    registry(key) {
        let scope = this;
        registries[key] = 0;
        return {
            set(value) {
                registries[key] += 1;
                scope.set(key, value);
            }
        };
    },

    /**
     * Recupera a quantidade de vezes que uma chave foi valorada.
     * @param {String} key
     * @returns {*}
     */
    getRegistryCount(key) {
        return registries[key];
    },

    /**
     * Recupera todas as intenções registradas.
     * @returns {{}}
     */
    getRegistries() {
        return registries;
    },

    /**
     * Recupera todos os dados já armazenados
     * @returns {{}}
     */
    getData() {
        return data;
    },

    /**
     *
     * @param key
     */
    remove(key) {
        if (listeners[key]) {
            let i = listeners[key].length - 1;
            while (i >= 0) {
                listeners[key].splice(i, 1);
                i -= 1;
            }
        }

        delete data[key];
    },

    /**
     * Define dados numa chave.
     * @param {String} key
     * @param {*} value
     */
    set(key, value) {
        let old = data[key];

        data[key] = value;

        if (listeners[key]) {
            for (let i in listeners[key]) {
                listeners[key][i](value, old);
            }
        }
    },

    /**
     * Recupera os dados de uma chave
     * @param {String} key
     * @returns {*}
     */
    get(key) {
        return data[key];
    },

    /**
     * Escuta alterações feitas nos dados de uma chave.
     * @param {String} key
     * @param {Function} cb
     * @returns {{remove: (function())}}
     */
    watch(key, cb) {
        if (!listeners[key]) {
            listeners[key] = [];
        }

        let index = listeners[key].length;
        listeners[key].push(cb);

        return {
            remove : () => {
                listeners[key].splice(index, 1);
            }
        };
    }

};

window['MapsDataLoader'] = MapsDataLoader;