import {labels as labelsEnUS} from "./en-us/labels";
import {labels as labelsPtBR} from "./pt-br/labels";
import {messages as messagesEnUS} from "./en-us/messages";
import {messages as messagesPtBR} from "./pt-br/messages";
import maskBRL from "./pt-br/mask";
import maskEnUS from "./en-us/mask";
import Converter from "../util/Converter";

const labels = {
    'pt-br' : labelsPtBR,
    'en-us' : labelsEnUS
};

const mask = {
    'pt-br' : maskBRL,
    'en-us' : maskEnUS
};

const messages = {
    'pt-br' : messagesPtBR,
    'en-us' : messagesEnUS
};

class Locale {

    _currentLanguage;

    constructor() {
        let navigatorLanguage = window.navigator.language.toLowerCase().replace(/\s+/g, '');
        this._currentLanguage = navigatorLanguage || 'pt-br';
    }

    /**
     *
     * @param {String} code
     * @param {Object|Array} [params]
     * @return {String}
     */
    getLabel(code, params) {
        return this._get('labels', code, params);
    }

    /**
     *
     * @param {String} code
     * @param {Object|Array} [params]
     * @return {String}
     */
    getMessage(code, params) {
        return this._get('messages', code, params);
    }

    /**
     * Retorna os padrões de máscaras para cada região
     * @returns {*}
     */
    getMaskDefinitions() {
        return mask[this._currentLanguage];
    }

    get labels() {
        return labels[this._currentLanguage];
    }

    get messages() {
        return messages[this._currentLanguage];
    }

    /**
     *
     * @param {String} language
     */
    setLanguage(language) {
        this._currentLanguage = language;
    }

    _get(type, code, params = []) {
        let values = this[type];
        return Converter.substitute(values[code], params);
    }

}

export const locale = new Locale();
window['mapsLocale'] = locale;