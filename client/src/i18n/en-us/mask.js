let mask = {
    number: {
        decimalSeparator: '.',
        millerSeparator: ',',
    }
};

mask.percentage = Object.assign({}, mask.number, {
    suffix : '%',
    toFixed : '3'
});

mask.money = Object.assign({}, mask.number, {
    prefix : '$',
    toFixed : '2'
});

mask.date = {
    pattern : 'month/day/year'
};

export default mask;