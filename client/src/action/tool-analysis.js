import ActionBase from "../_base/abstract/ActionBase";
import {ANALYSIS} from "../constant/analysis";

export default class ToolAnalysisAction extends ActionBase {

    /**
     *
     * @param {IAnalysis} analysis
     * @param {String} outputFormat
     */
    downloadFile({analysis, outputFormat}) {
        this.dispatch({
            type : ANALYSIS.DOWNLOAD_FILE,
            data : {analysis, outputFormat}
        });
    }

}