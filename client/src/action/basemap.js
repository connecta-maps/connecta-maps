import ActionBase from "../_base/abstract/ActionBase";
import {BASEMAP} from "../constant/basemap";

export default class BasemapAction extends ActionBase {

    loadDefaultBasemaps() {
        this.dispatch({
            type : BASEMAP.LOAD_DEFAULT_BASEMAPS
        });
    }

    /**
     *
     * @param {String} name
     */
    changeCurrentBasemap(name) {
        this.dispatch({
            type : BASEMAP.CHANGE_CURRENT_BASEMAP,
            data : name
        });
    }

}