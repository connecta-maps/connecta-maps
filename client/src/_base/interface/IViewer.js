
class IViewer {

    /**
     * @type {IProject}
     */
    project;

    /**
     * @type {String}
     */
    _id;

    /**
     * @type {String}
     */
    domainId;
    
    /**
     * @type {String}
     */
    initialRichLayerId;

    /**
     * @type {Object}
     */
    popupConfig = {
        /**
         * @type {Boolean}
         */
        enabled : false,

        /**
         * @type {String}
         * @enum ['tooltip']
         */
        positioning : ''
    };

    /**
     * @type {String}
     */
    title;

    /**
     * @type {IToolsConfig}
     */
    toolsConfig;

    /**
     * @type {String}
     * @enum ['MAP']
     */
    type;

    /**
     * @type {String}
     * @enum {'connecta', 'obiee'}
     */
    viewContext;

    /**
     * @type {IRichLayerInfo[]}
     */
    richLayersInfo;

}

class IOutField {

    /**
     * @type {String}
     */
    name;

    /**
     * @type {String}
     */
    alias;

    /**
     * @type {String}
     */
    valueType;

}

class IRichLayerInfo {

    /**
     * @type {String}
     */
    richLayerId;

    /**
     * @type {IOutField[]}
     */
    outFields;

}

class IProject {

    /**
     * @type {String[]}
     */
    basemaps;

    /**
     * @type {String}
     */
    domainId;

    /**
     * @type {{
     *  center : { lat:number, lng:number },
     *  maxZoom : number,
     *  minZoom : number,
     *  zoom : number,
     *  dynamicNavigation : boolean
     * }}
     */
    mapConfig;

    /**
     * @type {IRichLayer[]}
     */
    richLayers;

    /**
     * @type {String}
     * @enum {'connecta', 'obiee'}
     */
    serviceType;

    /**
     * @type {String}
     */
    title;

    /**
     * @type {{ name:string, _id : string }[]}
     */
    tools;
}

class IToolsConfig {

    /**
     * @type {IAnalysis[]}
     */
    analysisConfig;

    /**
     * @type {ILayerConfig[]}
     */
    layersConfig;

}