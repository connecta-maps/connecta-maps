
class IFilterConfig {
    /** @type {String} */
    operator;
    /** @type {String} */
    field;
    /** @type {*} */
    value;
    /** @type {*[]} */
    values;
}

/**
 * @class
 * @classdesc Classe responsável em mapear os atributos de uma Análise.
 *
 * <i>
 * Não deve ser usada diretamente, existe apenas para fins de documentação.
 * </i>
 */
class IAnalysis {

    /**
     *
     * @type {String}
     */
    _id;

    /**
     *
     * @type {Boolean}
     */
    visible;

    /**
     *
     * @type {Number}
     */
    opacity;

    /**
     * @type {String[]}
     */
    tags;


    /**
     * @type {Boolean}
     */
    shown;

    /**
     *
     * @type {String}
     */
    currentType;

    /**
     *
     * @type {Boolean}
     */
    enable;

    /**
     *
     * @type {String}
     */
    richLayerId;

    /**
     *
     * @type {String}
     */
    title;

    /**
     *
     * @type {Object}
     */
    configRenderer = {

        simpleRenderer : {
            fill : String,
            outlineColor : String,
            size : Number,
            customSymbology : {
                fileName : String,
                base64 : String,
                iconText: String,
                fontSize: Number,
                fontColor: String
            }
        },
        cluster : {
            uniqueSize : false,
            outlineColor : String,
            colorRamp : [String],
            breakCount : Number,
            size : Number,
            distance : Number
        },
        heatmap : {
            blurRadius : Number,
            colors : [String],
            field : ''
        },
        chart : {
            size : Number,
            metric : '',
            dimension : ''
        },

        thematic : {

            uniqueValues : {
                field : String,
                colorRamp : [String]
            },

            customBreaks : [ICustomBreak],

            /**
             * @type {String[]}
             */
            colorRamp : Array,

            fill : {

                /**
                 * @type {Number}
                 */
                breakCount : Number,

                /**
                 * @type {String}
                 */
                classificationMethod : String,

                /**
                 * @type {String}
                 */
                classificationField : String
            },
            size : {
                /**
                 * @type {Number}
                 */
                maxSize: Number,

                /**
                 * @type {Number}
                 */
                minSize: Number,

                /**
                 * @type {Number}
                 */
                breakCount : Number,

                /**
                 * @type {String}
                 */
                classificationMethod : String,

                /**
                 * @type {String}
                 */
                classificationField : String
            }
        }

    };

    /**
     * @type {Array}
     */
    filterConfig;


    timelineColumn;

    /**
     * @type {Object}
     */
    trackingConfig = {

        /**
         * @type {String}
         */
        filterField : String,

        /**
         * @type {String}
         */
        temporalDimensionField : String
    };

    /**
     * @type {{ [fieldName] : IPopupFieldConfig }}
     */
    outFieldsConfig;
}

class IPopupFieldConfig {

    /**
     * @type {Boolean}
     */
    checked;

    /**
     * @type {String}
     */
    label;

    /**
     * @type {string}
     * @enum 'number' | 'date'
     */
    typeMask;

    /**
     * @type {INumberConfig | IDateConfig}
     */
    config;

}

class INumberConfig {
    /**
     * @type {String}
     */
    decimalSeparator;
    /**
     * @type {String}
     */
    millerSeparator;
    /**
     * Count of decimal places
     * @type {Number}
     */
    toFixed;

    /**
     * @type {String}
     */
    prefix;

    /**
     * @type {String}
     */
    suffix;
}

class IDateConfig {

    /**
     * @type {String}
     */
    pattern;

}

class ICustomBreak {
    /** @type {Boolean} */
    visible;
    /** @type {Number|String} */
    breakIdentifier;
    symbology = {
        fillColor : String,
        borderColor : String,
        borderSize : Number
    };
}