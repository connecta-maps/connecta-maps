/**
 *
 * @returns {{title: string, serverType: string, dsn: string, user: string, password: string}}
 */
const getDefault = () => {
    return {
        title : '',
        serverType : '',
        dsn : '',
        user : '',
        password : ''
    }
};

export default class ISpatialDataSource {

    static getDefault = getDefault;

    _id;

    /**
     * Identificador do tipo de servidor espacial que este documento representará.
     * @type {String}
     */
    serverType;

    /**
     *
     * @type {String}
     */
    title;

    /**
     *
     * @type {String}
     */
    description;

    /**
     * DSN de acesso a origem dos dados espaciais.
     * @type {String}
     */
    dsn;

    /**
     *
     * @type {String}
     */
    user;

    /**
     *
     * @type {String}
     */
    password;

    /**
     *
     * @type {String}
     */
    domainId;

}