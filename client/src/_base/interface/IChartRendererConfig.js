
class IChartRendererConfig {

    /**
     * @type {String}
     */
    shape; // pie or donut

    /**
     * @type {Number}
     */
    size;

    /**
     * @type {String}
     */
    dimension;

    /**
     * @type {String}
     */
    metric;

}
