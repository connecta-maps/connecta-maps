import {scopeSingletonFactory} from "../../helper/scopeSingletonFactory";
import ScopeSingletonBase from "./ScopeSingletonBase";

export default class ServiceBase extends ScopeSingletonBase {

    $prepare() {
        scopeSingletonFactory(this);
    }

}