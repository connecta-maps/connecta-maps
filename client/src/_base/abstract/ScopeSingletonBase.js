
export default class ScopeSingletonBase {

    /**
     * @type {ConnectaMaps}
     */
    $parent;

    /**
     * @type {AppStore}
     */
    $appStore;

    /**
     * @type {AppAction}
     */
    $appAction;

    /**
     * @type {MapStore}
     */
    $mapStore;

    /**
     * @type {MapAction}
     */
    $mapAction;

    /**
     * @type {ViewerStore}
     */
    $viewerStore;

    /**
     * @type {ViewerAction}
     */
    $viewerAction;

    /**
     * @type {AppService}
     */
    $appService;

    /**
     * @type {GeoService}
     */
    $geoService;

    /**
     * @type {BasemapAction}
     */
    $basemapAction;

    /**
     * @type {BasemapStore}
     */
    $basemapStore;

    /**
     * @type {ToolAnalysisStore}
     */
    $toolAnalysisStore;

    /**
     * @type {ToolAnalysisAction}
     */
    $toolAnalysisAction;

    constructor(parent) {
        this.$parent = parent;
    }

}