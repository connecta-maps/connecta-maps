import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";
import {locale} from "../i18n/Locale";
import LayerForm from "./ToolLayers/LayerForm";
import {VIEWER} from "../constant/viewer";
import update from "react-addons-update";
import {MAP_CONSTANT} from "../constant/map";
import ToolAnalysisGroupContainer from "./SortableTree";

class ToolLayers extends ModuleBase {

    /**
     * @type {LayerBox[]}
     */
    _layersBoxesInstance = [];

    constructor (props, context) {
        super(props, context);

        this.state = {
            layer: null,
            layersConfig : [],
            layersFormIsOpened : false,
            staticLayers : [],
            spatialDataSources : [],
            treeIsBuilt: true
        };
    }

    componentDidMount () {
        this.own(
            this.$viewerStore.listen(VIEWER.ADD_LAYER_BOX, this.putLayerBoxInstance),
            this.$viewerStore.listen(VIEWER.REMOVE_LAYER_BOX, this.removeLayerBoxInstance),

            this.$viewerStore.listen(VIEWER.GET_LAYER_CONFIG, this.onGetLayersConfig, true),
            this.$viewerStore.listen(VIEWER.SAVE_NEW_LAYER, this.onSuccessSaveLayer),
            this.$viewerStore.listen(VIEWER.ENABLE_STATIC_LAYER, this.executeOnLayerBox.bind(this, 'onLayerChange')),
            this.$viewerStore.listen(VIEWER.REORDER_STATIC_LAYERS, this.onUpdateViewer),
            this.$viewerStore.listen(VIEWER.DELETE_STATIC_LAYER, this.onUpdateViewer),
            this.$mapStore.listen(MAP_CONSTANT.STATIC_LAYER_IS_BUILDING, this.executeOnLayerBox.bind(this, 'handleStaticLayerIsBuilding')),
            this.$mapStore.listen(MAP_CONSTANT.STATIC_LAYER_BUILD_SUCCESS, this.executeOnLayerBox.bind(this, 'handleStaticLayerBuildSuccess'))
        );

        let promise = this.$appService.getSpatialDatasources();
        promise.then((response) => {
            this.setState(update(this.state, {
                spatialDataSources : {
                    $set : response.data
                }
            }));
        });
    }

    putLayerBoxInstance = (instance) => {
        this._layersBoxesInstance.push(instance);
    };

    removeLayerBoxInstance = (instance) => {
        let index = this._layersBoxesInstance.indexOf(instance);

        if(index !== -1) {
            this._layersBoxesInstance.splice(index, 1);
        }
    };

    onSuccessSaveLayer = (layers) => {
        this.handleLayersFormToggle(false);
        this.onUpdateViewer(layers);
        this.setState({layer: null});
    };

    onUpdateViewer = (layersConfig) => {
       let state = update(this.state, {
           layersConfig: {
               $set: layersConfig
           }
       });

       this.setState(state, this.rebuildTree);
    };

    executeOnLayerBox = (...args) => {
        let methodName = args[0];
        args.splice(0, 1);

        for (let instance of this._layersBoxesInstance) {
            if (instance[methodName]) {
                instance[methodName].apply(instance, args);
            }
        }
    };

    onGetLayersConfig = (layersConfig) => {
        let state = update(this.state, {
            layersConfig: {
                $set: layersConfig || []
            }
        });

        this.setState(state, this.rebuildTree);
    };

    handleClickCancelLayer = () => {
        this.handleLayersFormToggle(false);
        this.setState({layer: null});
    };

    handleClickOnSaveLayer = () => {
        this.$viewerAction.clickOnSaveNewLayer();
    };

    handleLayersFormToggle = (toggle) => {
        this.setState({
            layersFormIsOpened : toggle !== undefined ? toggle : !this.state.layersFormIsOpened
        });
    };

    handleOpenForm = (groupId) => {
        this.setState({ groupId });
        this.handleLayersFormToggle(true);
    };

    handleChangeSpatialDataSource = (dataSourceId) => {
        this.setState({formIsValid: false});

        let promise = this.$appService.getLayersByDataSourceId(dataSourceId);

         promise.then((response) => {
             this.setState(update(this.state, {
                 staticLayers : {
                     $set : response.data
                 }
             }));
         });
    };

    handleLayerFormDestroy = () => {
        let newState = update(this.state, {
            staticLayers : {
                $set : []
            }
        });

        this.setState(newState);
    };

    handleDeleteStaticLayer = (id) => {
        this.$viewerAction.deleteStaticLayer(id);
    };

    handleClickDeleteGroup = (groupId) => {
        this.$viewerAction.deleteGroup(groupId, true);
    };

    handleFormIsValid = (valid) => {
        this.setState({
            formIsValid : valid
        });
    };

    rebuildTree(){
        if(this.state.treeIsBuilt){
            this.setState({
                treeIsBuilt: false
            }, () => {
                this.setState({
                    treeIsBuilt: true
                });
            });
        }
    }

    render () {
        return (
            <section className={this.appClass + '-tool-layers'}>
                {this.state.treeIsBuilt &&
                (<ToolAnalysisGroupContainer isLayers={true}
                                             items={this.state.layersConfig}
                                             onAddItemClick={this.handleOpenForm}
                                             onRemoveGroup={this.handleClickDeleteGroup}
                                             onRemoveItem={this.handleDeleteStaticLayer}/>)
                }

                <Dialog title={locale.getLabel(this.state.layer && this.state.layer._id ? 'L1013' : 'L1012')}
                        modal={true}
                        autoDetectWindowHeight={true}
                        contentStyle={{maxHeight : '550px'}}
                        open={this.state.layersFormIsOpened}
                        actions={[
                            <FlatButton
                                label={locale.getLabel('L0037')}
                                primary={true}
                                onTouchTap={this.handleClickCancelLayer}>
                            </FlatButton>,
                            <FlatButton
                                disabled={!this.state.formIsValid}
                                label={locale.getLabel('L0036')}
                                primary={true}
                                keyboardFocused={true}
                                onTouchTap={this.handleClickOnSaveLayer}>
                            </FlatButton>
                        ]}>

                    <LayerForm spatialDataSources={this.state.spatialDataSources}
                               onChangeSpatialDataSource={this.handleChangeSpatialDataSource}
                               onDestroy={this.handleLayerFormDestroy}
                               layer={this.state.layer}
                               onValidateForm={this.handleFormIsValid}
                               layers={this.state.staticLayers}
                               groupId={this.state.groupId} />
                </Dialog>

            </section>
        );
    }
}

export default ToolLayers;




