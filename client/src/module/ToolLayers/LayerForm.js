import React from 'react';
import PropTypes from "prop-types";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {locale} from '../../i18n/Locale';
import update from 'react-addons-update';
import {OPERATION_NAME} from '../../constant/analysis';
import ModuleBase from "../../_base/abstract/ModuleBase";
import {VIEWER} from "../../constant/viewer";

export default class LayerForm extends ModuleBase {

    static propTypes = {
        spatialDataSources : PropTypes.array,
        layers : PropTypes.array,
        layer : PropTypes.object,
        onChangeSpatialDataSource : PropTypes.func,
        onDestroy: PropTypes.func,
        onValidateForm : PropTypes.func
    };

    constructor (props, context) {
        super(props, context);

        this.state = {
            layer : Object.assign({}, {
                layerId : '',
                operationName : 'getMap',
                capabilities: []
            }, props.layer)
        };
    }

    componentDidMount(){
        this.own(
            this.$viewerStore.listen(VIEWER.CLICK_ON_SAVE_NEW_LAYER, this.onSaveNewLayer)
        );

        if(this.state.layer._id){
            this.props.onChangeSpatialDataSource(this.state.layer.spatialDataSourceId);
        }
    }

    componentWillUnmount() {
        this.props.onDestroy();
    }

    onSaveNewLayer = () => {
        setTimeout(() => {
            this.$viewerAction.saveNewLayer(this.state.layer, this.props.groupId);
        }, 0);
    };

    handleSpatialDataSourceChange = (event, index, value) => {
        let newState = update(this.state, {
            layer : {
                capabilities: [],
                spatialDataSourceId : {
                    $set : value
                }
            }
        });

        this.setState(newState);
        this.props.onChangeSpatialDataSource(newState.layer.spatialDataSourceId);
    };

    handleLayerChange = (event, index, value) => {
        let newState = update(this.state, {
            layer : {
                layerId : {
                    $set : value
                },
                title : {
                    $set : this.props.layers[index].title
                },
                capabilities: {
                    $set: this.props.layers[index].capabilities || []
                }
            }
        });

        if (!newState.layer.capabilities.length || (newState.layer.capabilities.length === 1 && newState.layer.capabilities[0] === OPERATION_NAME.QUERY)) {
            setTimeout(() => {
                this.props.onValidateForm(false);
                this.$appAction.alertWarn('APP_WARN_01');
            }, 0);
        }else{
            this.props.onValidateForm(true);
        }

        this.setState(newState);
    };

    render () {
        return (
            <div className="cmaps-layer-form-container">
                <div style={{display : 'flex', width : '100%'}}>
                    <SelectField floatingLabelText={locale.getLabel('L0069')}
                                 style={{width : '50%', marginTop : '14px', marginLeft : '5px'}}
                                 value={this.state.layer.spatialDataSourceId}
                                 onChange={this.handleSpatialDataSourceChange}
                                 autoWidth={true}>

                        {this.props.spatialDataSources.map((spatialDataSource, index) => {
                            return <MenuItem key={index} value={spatialDataSource._id} primaryText={spatialDataSource.title} />
                        })}

                    </SelectField>

                    <SelectField floatingLabelText={locale.getLabel('L0070')}
                                 style={{width : '50%', marginTop : '14px', marginLeft : '5px'}}
                                 value={this.state.layer.layerId}
                                 onChange={this.handleLayerChange}
                                 autoWidth={true}>

                        {this.props.layers.map((layer, index) => {
                            return <MenuItem key={index} value={layer._id} primaryText={layer.title} />
                        })}

                    </SelectField>
                </div>
            </div>
        );
    }
}
