import React from "react";
import ModuleBase from "../../../_base/abstract/ModuleBase";

class HeatmapLegend extends ModuleBase {

    shouldComponentUpdate(nextProps) {
        let update = false;
        let props = this.props;

        if (!nextProps) {
            return update;
        }

        if (props.legend !== nextProps.legend) {
            update = true;
        }

        return update;
    }

	render() {
		return (
			this.props.legend &&
			<div style={{display: 'block'}}
				 className={this.appClass + '-heatmap-legend ' + this.appClass + '-legend-container'}>
				<div className={this.appClass + '-gradient'} style={{background: 'linear-gradient(to right, ' + this.props.legend.colors.join(',') + ')'}}/>
				<div style={{display: 'flex', justifyContent: 'space-between'}}>
					<span style={{margin: '0 10px'}} >{this.props.legend.min}</span>
					<span style={{margin: '0 10px'}}>{this.props.legend.max}</span>
				</div>
			</div>
		)

	}

}

export default HeatmapLegend;
