import React from "react";
import {locale} from "../../../../i18n/Locale";

export const legendDecorators = {
    esriGeometryPoint : (props) => {
        return (
            <div
                ref={el => props.onLoad && props.onLoad(el)}
                className={'cmaps-thematic-color-legend'}
                onClick={props.onClick || function () {}}
            >
                <div className={'cmaps-legend-color-point'}
                     style={{background: props.color}}/>
                {props.label &&
                <span
                    style={{
                        wordBreak: 'break-all',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        display: 'block',
                        overflow: 'hidden'
                    }}
                    data-tip={props.label}
                    title={props.label}
                >{props.label}</span>
                }
                {props.count !== void 0 && (
                    <span style={{
                        fontSize : '0.8em',
                        flex: 1,
                        textAlign: 'right',
                        whiteSpace: 'nowrap'
                    }}> ({props.count + ' ' + locale.getLabel('L2020')})</span>
                )}
            </div>
        );
    },
    esriGeometryPolygon : (props) => {
        return (
            <div
                ref={el => props.onLoad && props.onLoad(el)}
                className={'cmaps-thematic-color-legend'}
                onClick={props.onClick || function () {}}
            >
                <div className={'cmaps-legend-color-polygon'}
                     style={{background: props.color}}/>
                {props.label &&
                <span
                    style={{
                        wordBreak: 'break-all',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        width: 145,
                        display: 'block',
                        overflow: 'hidden'
                    }}
                    data-tip={props.label}
                    title={props.label}
                >{props.label}</span>
                }
                {props.count !== void 0 && (
                    <span style={{ fontSize : '0.8em' }}> ({props.count + ' ' + locale.getLabel('L2020')})</span>
                )}
            </div>
        );
    },
    esriGeometryLine : (props) => {
        return (
            <div
                ref={el => props.onLoad && props.onLoad(el)}
                className={'cmaps-thematic-color-legend'}
                onClick={props.onClick || function () {}}
            >
                <div className={'cmaps-legend-color-line'}
                     style={{background: props.color}}/>
                {props.label &&
                <span
                    style={{
                        wordBreak: 'break-all',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        width: 145,
                        display: 'block',
                        overflow: 'hidden'
                    }}
                    data-tip={props.label}
                    title={props.label}
                >{props.label}</span>
                }
                {props.count !== void 0 && (
                    <span style={{ fontSize : '0.8em' }}> ({props.count + ' ' + locale.getLabel('L2020')})</span>
                )}
            </div>
        );
    }
};