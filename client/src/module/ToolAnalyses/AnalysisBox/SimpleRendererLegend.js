import React from "react";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import {legendDecorators} from "./decorator/LegendDecorator";

class SimpleRendererLegend extends ModuleBase {

    constructor(props, context) {
        super(props, context);

        this._richLayer = this.$viewerStore.getRichLayerByAnalysisId(props.analysis._id);
    }

    shouldComponentUpdate(nextProps) {
        let update = false;
        let props = this.props;

        if (!nextProps) {
            return update;
        }

        if (props.legend !== nextProps.legend) {
            update = true;
        }

        return update;
    }

    render() {
        let Type = legendDecorators[this._richLayer.layer.geometryType];
        let props = {
            color: this.props.legend.fill,
            appClass : this.appClass,
            label : this.props.legend.text
        };
        return (
            <section>
                {
                    this.props.legend &&
                    <div style={{display: 'flex'}}
                         className={this.appClass + '-thematic-color-legend ' + this.appClass + '-legend-container'}>
                        { this.props.legend.fill &&
                        <Type {...props}/>
                        }
                    </div>
                }
            </section>
        )
    }
}

export default SimpleRendererLegend;
