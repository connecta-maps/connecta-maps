import React from 'react';
import ModuleBase from "../../../_base/abstract/ModuleBase";
import MaskHelper from "../../../helper/MaskHelper";
import {locale} from "../../../i18n/Locale";
import {MapsDataLoader} from "../../../MapsDataLoader";
import {legendDecorators} from "./decorator/LegendDecorator";

class ThematicLegend extends ModuleBase {

    /**
     * @type {IRichLayer}
     */
    _richLayer;

    constructor(props, context) {
        super(props, context);

        this._richLayer = this.$viewerStore.getRichLayerByAnalysisId(props.analysis._id);

        this.state = {
            /**
             * @type {IAnalysis}
             */
            analysis : props.analysis
        };
    }

    shouldComponentUpdate(nextProps) {
        let update = false;
        let props = this.props;

        if (!nextProps) return update;
        if (props.legend !== nextProps.legend) update = true;

        return update;
    }

    /**
     *
     * @param {String} typeRenderer fill|size
     */
    getMaskConfig = (typeRenderer) => {
        let analysis = this.state.analysis;
        let businessData = MapsDataLoader.get(this._richLayer.resultSetId);
        let mask = "";

        if (analysis.configRenderer.thematic[typeRenderer]) {
            mask = (analysis.outFieldsConfig || businessData[0])[
                analysis.configRenderer.thematic[typeRenderer].classificationField];
        }

        return mask;
    };

    render() {
        let renderLabel = (typeRenderer, index) => {
            let configRenderer = this.props.legend[typeRenderer];

            let breakConfig = configRenderer[index];

            let valueMasked = MaskHelper.getValueMasked(
                breakConfig.value.start,
                this.getMaskConfig(typeRenderer)
            );
            let greaterValueMasked = MaskHelper.getValueMasked(
                breakConfig.value.ends,
                this.getMaskConfig(typeRenderer)
            );

            if (breakConfig.value.ends !== undefined) {
                return '>= ' + valueMasked + ' < ' + greaterValueMasked;
            } else {
                return '>= ' + valueMasked;
            }
        };

        let sizesLength,
            sizeLegendContainerSize,
            legend = this.props.legend;

        if (legend && legend.size) {
            sizesLength = legend.size.length;
            if (sizesLength) {
                sizeLegendContainerSize = (legend.size[sizesLength - 1].symbology.radius * 2) + 25;
            }
        }

        return (
            <section>
                {
                    this.props.legend && this.props.legend.fill &&
                    <div style={{display: 'block'}}
                         className={this.appClass + '-thematic-color-legend ' + this.appClass + '-legend-container'}>
                        { this.props.legend.fill &&
                        this.props.legend.fill.map(({symbology, count}, index) => {
                            let Type = legendDecorators[this._richLayer.layer.geometryType];
                            let color = symbology.fillColor;

                            let props = {
                                color,
                                appClass : this.appClass,
                                label : renderLabel('fill', index),
                                count : count
                            };

                            return (
                                <Type key={index}
                                      {...props}/>
                            );
                        })
                        }
                    </div>
                }

                {
                    this.props.legend && this.props.legend.uniqueValues && (
                        <div style={{
                            maxHeight: 200,
                            overflowY : 'scroll',
                            flexDirection : 'column',
                            display : 'flex'
                        }}
                             className={this.appClass + '-thematic-color-legend ' + this.appClass + '-legend-container'}>
                            {this.props.legend.uniqueValues.map((uniqueValue, index) => {
                                let Type = legendDecorators[this._richLayer.layer.geometryType];
                                let props = {
                                    color : uniqueValue.symbology.fillColor,
                                    appClass : this.appClass,
                                    label : uniqueValue.value,
                                    count: uniqueValue.count
                                };

                                return (
                                    <Type key={index}
                                          {...props}/>
                                );
                            })}
                        </div>
                    )
                }

                {
                    this.props.legend && this.props.legend.size && sizeLegendContainerSize &&
                    ([
                        <br key={0}/>,
                        <div key={1} className={this.appClass + '-thematic-size-legend ' + this.appClass + '-legend-container'}
                             style={{display: 'flex', height: sizeLegendContainerSize}}>
                            {
                                this.props.legend.size.map((size, index) => {
                                    let circleSize = size.symbology.radius * 2;

                                    let lineStyle = (index + 1) % 2 === 0 ? {left: '50%'} : {right: '50%'};
                                    let lineContainerStyle = (index + 1) % 2 === 0 ? {textAlign: 'right'} : {textAlign: 'left'};

                                    return (
                                        <div key={index} className={this.appClass + '-thematic-size-legend-circle'} style={{width: circleSize, height: circleSize}}>
                                            <div className={this.appClass + '-thematic-size-legend-line-container'} style={lineContainerStyle}>
                                                <div className={this.appClass + '-thematic-size-legend-line'} style={lineStyle}>{renderLabel('size', index)}</div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    ])

                }
            </section>
        )
    }
}

export default ThematicLegend;
