import React from "react";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import chroma from "chroma-js";
import {locale} from '../../../i18n/Locale';

class ClusterLegend extends ModuleBase {

    constructor (props, context) {
        super(props, context);

        this.state = {
            legend : props.legend
        };
    }

    onChangeLegendConfig = ({analysisId, type, legend}) => {
        this.setState({
            legend
        });
    };

    shouldComponentUpdate(a, nextState) {
        let update = false;
        let state = this.state;

        if (!nextState) {
            return update;
        }

        if (state.legend !== nextState.legend) {
            update = true;
        }

        return update;
    }

    render() {

        let renderText = (legend, index) => {
            if (index === 0) {
                return <span >{locale.getLabel('L1009')}</span>
            } else {
                return <span>{'> ' + legend.init + (legend.end ? ' ≤ ' + legend.end : '') }</span>
            }
        };

        let renderLegend = (legend, index) => {

            let legendSize = 14 + (index * 2);
            let borderSize = legendSize + 6;

            if (index === 0) {
                return (
                    <div className={this.appClass + '-cluster-legend-color-border'}
                         style={{boxSizing: 'content-box', border: `solid 2px rgba(${chroma(this.state.legend.outlineColor).rgb()}, 0.6)`, height: legendSize, width: legendSize}}>
                        <div className={this.appClass + '-cluster-legend-color'}
                             style={{background: legend.color, width: legendSize, height: legendSize}}/>
                    </div>
                )
            } else {
                return (
                    <div className={this.appClass + '-cluster-legend-color-border'}
                         style={{border: `solid 3px rgba(${chroma(legend.color).rgb()}, 0.4)`, height: borderSize, width: borderSize}}>
                        <div className={this.appClass + '-cluster-legend-color'}
                             style={{background: legend.color, width: legendSize, height: legendSize}}/>
                    </div>
                )
            }
        };

        return (
            this.state.legend &&
            <div style={{display: 'flex'}}
                 className={this.appClass + '-cluster-legend ' + this.appClass + '-legend-container'}>
                {
                    this.state.legend.map((legend, index) => {

                        return (
                            <div key={index} className={this.appClass + '-cluster-legend-container'}>
                                <div style={{width: '14%'}} >
                                    {renderLegend(legend, index)}
                                </div>
                                <div>
                                    {renderText(legend, index)}
                                </div>
                            </div>

                        )
                    })
                }
            </div>
        )
    }

}

export default ClusterLegend;
