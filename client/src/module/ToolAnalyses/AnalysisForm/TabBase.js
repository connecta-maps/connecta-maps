import React from "react";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import Converter from "../../../util/Converter";
import {MapsDataLoader} from "../../../MapsDataLoader";

export default class TabBase extends ModuleBase{

    componentWillReceiveProps({analysis}) {
        if (!analysis || (analysis && !analysis.richLayerId)) {
            return;
        }

        let {richLayerId} = analysis;
        let richLayer = this.$viewerStore.getRichLayerMetaData(richLayerId);
        let state = Converter.mixin(this.state, {
            outFields : this.$viewerStore.getOutfieldsByRichLayerId(richLayerId),
            currentGeometryType : richLayer.layer.geometryType,
            resultSet : MapsDataLoader.get(richLayer.resultSetId)
        });

        state.analysis = analysis;

        if(state.outFields.length){
            let outFieldsConfig = state.outFields.map((outField) => Object.assign({}, {
                [outField.name]: Object.assign(outField, { checked: true })
            }));
            state.analysis.outFieldsConfig = Object.assign({}, ...outFieldsConfig, analysis.outFieldsConfig);
        }


        this.setState(state);
    }

}