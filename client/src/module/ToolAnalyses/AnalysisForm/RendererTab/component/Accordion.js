import React from 'react';
import PropTypes from 'prop-types';
import ModuleBase from "../../../../../_base/abstract/ModuleBase";
import {CircularProgress} from "material-ui";

export default class Accordion extends ModuleBase {

    static propTypes = {
        label : PropTypes.string,
        onLoad : PropTypes.func,
        onCollapse : PropTypes.func,
    };

    constructor(props, context) {
        super(props, context);
        this.setClassName('accordion-component');

        this.state = {
            collapsed : true,
            children : props.children
        };
    }

    componentDidMount() {
        if (this.props.onLoad) {
            this.props.onLoad(this);
        }
    }

    render() {
        let className = this.className() + ' ' + (
            this.state.collapsed ? 'collapsed' : ''
        );
        return (
            <div className={className}>

                <div className={this.className('label')}>
                    <div>{this.props.label}</div>
                    <div
                        className={this.className('icon-collapsed')}
                        onClick={() => {
                            let collapsed = !this.state.collapsed;
                            this.setState({ collapsed });

                            if (this.props.onCollapse) {
                                this.props.onCollapse(this, collapsed);
                            }
                        }}
                    />
                </div>

                <div className={this.className('container')}>
                    {this.state.children ? this.state.children :
                        <CircularProgress />
                    }
                </div>

            </div>
        );
    }

}