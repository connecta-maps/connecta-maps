import React from 'react';
import update from 'react-addons-update';
import ModuleBase from "../../../../../_base/abstract/ModuleBase";
import PropTypes from 'prop-types';
import {
    Checkbox,
    DropDownMenu,
    MenuItem,
    RaisedButton,
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from "material-ui";
import {locale} from "../../../../../i18n/Locale";
import Aggregator from "../../../../../helper/ThematicHelper";
import {scope} from "../../../../../scope";
import MaskHelper from "../../../../../helper/MaskHelper";
import Symbology from "./Symbology";
import * as chroma from "chroma-js";
import Converter from "../../../../../util/Converter";
import {DEFAULT_ANALYSIS_CONFIG} from "../../../../../constant/analysis";
import ColorPicker from "../../../../../component/ColorPicker";
import InputRange from "../../../../../component/InputRange";
import AppErrorI18n from "../../../../../native/AppErrorI18n";

export const palette = (index) => {
    let ramps = [
        { ramp : DEFAULT_ANALYSIS_CONFIG.configRenderer.thematic.fill.colorRamp },
        { ramp : ['#005c06', '#d9c345', '#be0600'] },
        { ramp : ['#d93c00', '#91842f', '#7fbe69'] },
        { ramp : ['#d90056', '#069', '#00bebc'] },
        { ramp : ['#7B2A3B', '#005c06', '#86DDB2'] },
        { ramp : ['#092140', '#3c7dcb', '#BF2A2A'] },
        { ramp : ['#5C4B51', '#2a4966', '#F06060'] }
    ];
    return index === void 0 ? ramps : ramps[index];
};

export default class ThematicListBreaksComponent extends ModuleBase {

    static propTypes = {
        breaks : PropTypes.array,
        analysis : PropTypes.any,
        onChangeAnalysis : PropTypes.func
    };

    /**
     *
     * @param {IAnalysis} analysis
     * @returns {Promise}
     */
    static getConfigBreaks(analysis, $parent) {
        return new Promise((resolve, reject) => {
            let $viewerStore = $parent.inject(scope.ViewerStore);
            let $geoService = $parent.inject(scope.GeoService);
            let layer = $viewerStore.getLayerMetaDataByAnalysisId(analysis._id);

            if (analysis.configRenderer.thematic.fill) {
                //query and get-breaks
                return Promise.all([
                    $geoService.query(layer._id, { }, analysis),
                    $geoService.getBreaks(layer._id, analysis.configRenderer.thematic.fill, analysis.filterConfig)
                ])
                    .then(([queryResponse, getBreaksResponse]) => {
                        resolve(
                            Aggregator.getConfigThematicRenderer(
                                'fill',
                                queryResponse,
                                getBreaksResponse,
                                analysis.configRenderer.thematic
                            ).fill
                        );
                    })
                    .catch(reject);

            } else if (analysis.configRenderer.thematic.uniqueValues) {
                // generate-renderer
                return $geoService.generateRenderer(layer._id, analysis.configRenderer.thematic.uniqueValues, analysis.filterConfig)
                    .then(result => resolve(result.uniqueValues)).catch(reject);

            } else {
                reject(new Error('No Data'));

            }
        });

    }

    currentGeometrySymbologyNode;
    currentGeometrySymbologyIndex;

    constructor(props, context) {
        super(props, context);
        this.setClassName('tool-analysis-analysis-form-thematic-list-breaks');

        this.state = {
            breaks : props.breaks,
            flagVisibleDropDown : false,
            openPopover : false,
            form : this.getDefaultForm()
        };

        this._layer = this.$viewerStore.getLayerByAnalysisId(this.props.analysis._id);
    }

    getDefaultForm() {
        let customBreaks;
        if (Converter.verifyNestedValue(this.props.analysis, 'configRenderer.thematic.customBreaks')) {
            customBreaks = this.props.analysis.configRenderer.thematic.customBreaks;
        }

        let rampIndex = 0;

        if (Converter.verifyNestedValue(this.props.analysis, 'configRenderer.thematic.fill')) {
            let paletteStr = palette().map(({ramp}) => JSON.stringify(ramp));
            let index = paletteStr.indexOf(JSON.stringify(this.props.analysis.configRenderer.thematic.fill.colorRamp));

            if (index >= 0) {
                rampIndex = index;
            }
        }

        return {
            rampIndex,
            customBreaks : customBreaks || []
        }
    }

    getCustomBreakOnForm = (breakIdentifier) => {
        let customBreak;
        for (let iterCustomBreak of this.state.form.customBreaks) {
            if (iterCustomBreak.breakIdentifier === breakIdentifier) {
                customBreak = iterCustomBreak;
                break;
            }
        }
        return customBreak;
    };

    onSave = () => {
        try {
            let customBreaks = [];
            let countInvisibles = 0;

            for (let customBreak of this.state.form.customBreaks) {
                if (customBreak.symbology || customBreak.visible === false) {
                    customBreaks.push(customBreak);
                }

                if (customBreak.visible === false) {
                    countInvisibles += 1;
                }
            }

            if (countInvisibles === this.state.breaks.length) {
                throw new AppErrorI18n('APP_INFO_08', null);
            }

            if (this.state.form.rampIndex) {

            }

            let type = 'fill';

            for (let iterBreak of this.state.breaks) {
                if (typeof iterBreak.value !== 'object') {
                    type = 'uniqueValues';
                    break;
                }
            }

            this.props.onChangeAnalysis(update(this.props.analysis, {
                configRenderer : {
                    thematic : {
                        [type] : {
                            colorRamp : {
                                $set : palette(this.state.form.rampIndex).ramp
                            }
                        },
                        customBreaks : {
                            $set : customBreaks
                        }
                    }
                }
            }));

            this.$appAction.alertInfo('APP_SUCCESS_07');

        } catch (error) {
            if (error instanceof AppErrorI18n) {
                this.$appAction.alertWarn(error.code, error.params);

            } else {
                this.$appAction.alertError('APP_ERROR_05', null, error);
            }
        }
    };

    onChangeForm = (form) => {
        this.setState({
            form : update(this.state.form, form)
        });
    };

    createCustomBreak = (breakValue, indexOnBreaks, params) => {
        let value = breakValue.value;
        let breakIdentifier = typeof value === 'string' ? value : indexOnBreaks;
        let mappingByBreakIdentifier = {};
        let {visible, symbology} = params;

        if (symbology) {
            if (!symbology.borderColor) {
                symbology.borderColor = '#000000';
            }
            if (symbology.borderSize === void 0) {
                symbology.borderSize = 1;
            }
        }

        for (let current of this.state.form.customBreaks) {
            mappingByBreakIdentifier[current.breakIdentifier] = current;
        }

        if (!mappingByBreakIdentifier[breakIdentifier]) {
            let $set = {
                breakIdentifier,
                visible : visible !== void 0 ? visible : false
            };

            if (symbology) {
                $set.symbology = symbology;
            }

            mappingByBreakIdentifier = update(mappingByBreakIdentifier, {
                [breakIdentifier] : { $set }
            });
        } else {
            let $updates = {};

            for (let key in params) {
                if (key === 'visible') {
                    params[key] = visible !== void 0 ? visible : false;
                }
                $updates[key] = {
                    $set : params[key]
                };
            }

            mappingByBreakIdentifier = update(mappingByBreakIdentifier, {
                [breakIdentifier] : $updates
            });
        }

        return {
            customBreaks : Object.keys(mappingByBreakIdentifier).map((breakIdentifier) => {
                return mappingByBreakIdentifier[breakIdentifier];
            }),
            customBreak : mappingByBreakIdentifier[breakIdentifier]
        };
    };

    render() {
        return (
            <div className={this.className()}>

                <div className="cmaps-row">
                    <div className="cmaps-col-12">
                        {this.renderComboPaletteColors()}
                    </div>
                    <div className="cmaps-col-12">
                        { this.state.openPopover && this.renderPopover() }
                        {this.renderTableSymbology()}
                    </div>
                </div>

                <div className="cmaps-row">
                    <div className="cmaps-col-12" style={{
                        textAlign : 'right',
                        marginTop : '10px'
                    }}>
                        <RaisedButton
                            label={locale.getLabel('L2003')}
                            onTouchTap={this.onSave}
                        />
                    </div>
                </div>

            </div>
        );
    }

    renderComboPaletteColors = () => {
        let rampIndex = String(this.state.form.rampIndex);

        let createDivColors = (config) => {
            return (
                <div style={{
                    width : 120,
                    height : 20,
                    display: 'flex',
                    flexDirection: 'row'
                }}>
                    {config.ramp.map((color, index) => (
                        <div key={index}
                             style={{
                                 width: '100%',
                                 background: color
                             }}/>
                    ))}
                </div>
            );
        };

        let Component = (
            <div onClick={() => {
                this.setState({
                    flagVisibleDropDown : !this.state.flagVisibleDropDown
                })
            }}>
                <div className={this.className('current-ramp-color')}>
                    { createDivColors(palette(Number(rampIndex))) }
                </div>
            </div>
        );

        if (this.state.flagVisibleDropDown) {
            Component = (
                <DropDownMenu
                    openImmediately={true}
                    value={this.state.form.rampIndex}
                    autoWidth={true}
                    style={{width: 200}}
                    onClose={() => {
                        this.setState({
                            flagVisibleDropDown: false
                        });
                    }}
                    onChange={(event, key, index) => {
                        let formUpdate = {
                            rampIndex: {
                                $set: index
                            }
                        };

                        if (index !== this.state.form.rampIndex) {
                            formUpdate.customBreaks = {
                                $set : []
                            };
                        }

                        this.onChangeForm(formUpdate);

                        if (index !== null) {
                            let ramp = palette(index).ramp;
                            let arr = chroma
                                .scale(ramp)
                                .colors(this.state.breaks.length);

                            let breaks = [];

                            for (let i in this.state.breaks) {
                                let breakConfig = this.state.breaks[i];
                                breaks.push(Object.assign({}, breakConfig, {
                                    symbology : {
                                        fillColor : arr[i]
                                    }
                                }));
                            }

                            setTimeout(() => {
                                this.setState({ breaks })
                            }, 0);
                        }
                    }}
                >
                    {palette().map((config, index) => {
                        return (
                            <MenuItem
                                primaryText={<span style={{display: 'none'}}>{config.label}</span>}
                                key={index}
                                value={index}
                                leftIcon={
                                    <div style={{
                                        width: 'calc(100% - 50px)',
                                        height: 27,
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'space-around',
                                        alignContent: 'center'
                                    }}>
                                        {createDivColors(config)}
                                    </div>
                                }/>
                        );

                    })}
                </DropDownMenu>
            );
        }

        return Component;
    };

    renderPopover = () => {
        if (!this.currentGeometrySymbologyNode) {
            return;
        }

        let tableSymbology = this.refs.tableSymbology;
        let position = {
            y : this.currentGeometrySymbologyNode.parentNode.offsetTop - tableSymbology.refs.tableDiv.scrollTop,
            x : this.currentGeometrySymbologyNode.parentNode.offsetLeft
        };
        let currentBreakValue = this.state.breaks[this.currentGeometrySymbologyIndex];
        let value = currentBreakValue.value;
        let breakIdentifier = typeof value === 'string' ? value : this.currentGeometrySymbologyIndex;
        let customBreak = this.getCustomBreakOnForm(breakIdentifier) || {};
        let symbology = customBreak.symbology || currentBreakValue.symbology;

        if (!symbology.borderColor) {
            symbology.borderColor = '#000000';
        }
        if (symbology.borderSize === void 0) {
            symbology.borderSize = 1;
        }

        let changeSymbology = (symbology) => {
            let customBreaks = this.createCustomBreak(
                currentBreakValue,
                this.currentGeometrySymbologyIndex,
                {
                    symbology,
                    visible : true
                }
            ).customBreaks;
            this.onChangeForm({
                customBreaks : {
                    $set : customBreaks
                }
            });
        };

        return (
            <div className={this.className('popover')}>

                <div
                    className={this.className('tip')}
                    style={{
                        top : position.y - 60
                    }}
                />

                <div className={'cmaps-button-primary'}
                     style={{
                         position : 'absolute',
                         top : 5,
                         right : 5
                     }}
                     onClick={() => {
                         this.currentGeometrySymbologyNode = null;

                         this.setState({
                             openPopover : false
                         });
                     }}
                > X </div>

                <div className="cmaps-row">
                    <div className="cmaps-col-12">
                        <ColorPicker
                            style={{ maxWidth : '90%', width : '90%' }}
                            label={locale.getLabel('L3005')}
                            value={symbology.fillColor}
                            onChange={({hex}) =>
                                changeSymbology(update(symbology, {
                                    fillColor : {
                                        $set : hex
                                    }
                                }))
                            }
                        />
                    </div>
                    <div className="cmaps-col-12">
                        <ColorPicker
                            style={{ maxWidth : '90%', width : '90%' }}
                            label={locale.getLabel('L3004')}
                            value={symbology.borderColor}
                            onChange={({hex}) =>
                                changeSymbology(update(symbology, {
                                    borderColor : {
                                        $set : hex
                                    }
                                }))
                            }
                        />
                    </div>
                    <div className="cmaps-col-12">
                        <InputRange label={locale.getLabel('L3003')}
                                    style={{ width : '100%' }}
                                    value={symbology.borderSize}
                                    min={0}
                                    max={10}
                                    step={1}
                                    onDragStop={(opacity) =>
                                        changeSymbology(update(symbology, {
                                            borderSize : {
                                                $set : opacity
                                            }
                                        }))
                                    }/>
                    </div>
                </div>

            </div>
        );
    };

    renderTableSymbology = () => {
        let checkedAll = true;

        for (let customBreak of this.state.form.customBreaks) {
            if (!customBreak.visible) {
                checkedAll = false;
                break;
            }
        }

        return (
            <Table
                ref={'tableSymbology'}
                multiSelectable={true}
                height={'150px'}
                bodyStyle={{
                    overflowY : this.state.openPopover ? 'hidden' : 'auto'
                }}
            >
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn
                            style={{ textAlign : 'center', width: 40, padding : 0 }}
                        >
                            <Checkbox
                                checked={checkedAll}
                                onCheck={(event, checked) => {
                                    let customBreaks = [];

                                    if (checked) {
                                        for (let customBreak of this.state.form.customBreaks) {
                                            customBreaks.push(update(customBreak, {
                                                visible : {
                                                    $set : true
                                                }
                                            }));
                                        }

                                    } else {
                                        for (let index in this.state.breaks) {
                                            let breakValue = this.state.breaks[index];
                                            customBreaks.push(
                                                this.createCustomBreak(breakValue, Number(index), { visible : false }).customBreak
                                            );
                                        }
                                    }

                                    this.onChangeForm({
                                        customBreaks : {
                                            $set : customBreaks
                                        }
                                    });
                                }}
                            />
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            style={{ textAlign : 'center', width: 40 }}
                        >
                            {locale.getLabel('L3001')}
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            style={{ textAlign : 'center' }}
                        >
                            {locale.getLabel('L0035')}
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            style={{ textAlign : 'center' }}
                        >
                            {locale.getLabel('L3002')}
                        </TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    displayRowCheckbox={false}>
                    { this.state.breaks.map((breakValue, index) => {
                        let value = breakValue.value;
                        let count = breakValue.count;
                        let breakIdentifier = typeof value === 'string' ? value : index;
                        let customBreak = this.getCustomBreakOnForm(breakIdentifier) || {};
                        let symbology = customBreak.symbology || breakValue.symbology;

                        if (typeof breakValue.value === 'object') {
                            let config = {
                                typeMask : 'number',
                                config : {
                                    decimalSeparator: ',',
                                    millerSeparator: '.',
                                }
                            };
                            let start = MaskHelper.getValueMasked(breakValue.value.start, config);
                            let ends = MaskHelper.getValueMasked(breakValue.value.ends, config);

                            value = start + ' - ' + ends;
                        }

                        return (
                            <TableRow
                                key={index}
                                selectable={false}
                            >
                                <TableRowColumn
                                    style={{ textAlign : 'center', width: 40, padding : 0 }}
                                >
                                    <Checkbox
                                        checked={typeof customBreak.visible === 'boolean' ? customBreak.visible : true}
                                        onCheck={(event, checked) => {
                                            let customBreaks = this.createCustomBreak(
                                                breakValue,
                                                Number(index),
                                                { visible : checked }
                                            ).customBreaks;

                                            this.onChangeForm({
                                                customBreaks : {
                                                    $set : customBreaks
                                                }
                                            });
                                        }}
                                    />
                                </TableRowColumn>

                                <TableRowColumn style={{
                                    textAlign : 'center',
                                    width: 40,
                                    boxSizing: "content-box"
                                }}>
                                    <Symbology geometryType={this._layer.geometryType}
                                               onClickGeometrySymbology={(el) => {
                                                   this.setState({
                                                       openPopover : false
                                                   }, () => {
                                                       let openPopover = true;

                                                       if (this.currentGeometrySymbologyNode !== el) {
                                                           this.currentGeometrySymbologyNode = el;
                                                           this.currentGeometrySymbologyIndex = index;
                                                       } else {
                                                           this.currentGeometrySymbologyNode = null;
                                                           openPopover = false;
                                                       }
                                                       this.setState({ openPopover });
                                                   });
                                               }}
                                               symbology={symbology} />
                                </TableRowColumn>
                                <TableRowColumn style={{ textAlign : 'center' }}>
                                    <span title={value}>{value}</span>
                                </TableRowColumn>
                                <TableRowColumn style={{ textAlign : 'center' }}>
                                    { count }
                                </TableRowColumn>
                            </TableRow>
                        );
                    }) }
                </TableBody>
            </Table>
        );
    }

}