import React from 'react';
import ColorPicker from '../../../../../component/ColorPicker';
import {locale} from '../../../../../i18n/Locale';
import InputRange from '../../../../../component/InputRange';
import update from 'react-addons-update';
import {ANALYSIS_TYPE} from "../../../../../constant/analysis";
import IconButton from 'material-ui/IconButton';
import CloudUpload from 'material-ui/svg-icons/file/cloud-upload';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Card, CardText} from 'material-ui/Card';
import Visibility from 'material-ui/svg-icons/action/visibility';
import VisibilityOff from 'material-ui/svg-icons/action/visibility-off';
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import {GEOMETRY_TYPE} from '../../../../../constant/analysis';
import BreakCount from "../../../../../component/BreakCount";
import ModuleBase from "../../../../../_base/abstract/ModuleBase";

class TypeStatic extends ModuleBase {

    constructor (props, context) {
        super(props, context);

        this.state = {
            customSymbologyIsVisible : !!props.currentRenderer.customSymbology.base64 || !!props.currentRenderer.customSymbology.iconText
        };
    }

    handleFillChanged = (color) => {
        let state = update(this.props, {
            currentRenderer : {
                fill: {
                    $set: color.hex
                }
            }
        });

        this.emitSimpleRenderer(state);
    };

    handleOutlineColorChanged = (color) => {
        let state = update(this.props, {
            currentRenderer : {
                outlineColor: {
                    $set: color.hex
                }
            }
        });

        this.emitSimpleRenderer(state);
    };

    handleSizeChanged = (value) => {
        let state = update(this.props, {
            currentRenderer : {
                size: {
                    $set: value
                }
            }
        });

        this.emitSimpleRenderer(state);
    };

    handleCustomSymbologyVisibilityChange = (event, isInputChecked) => {
        this.setState({
            customSymbologyIsVisible : isInputChecked
        });
    };

    handleCLickUploadImageCustomSymbology = () => {
        let imageUploadDOMNode = this.refs.customSymbologyInput;
        imageUploadDOMNode.click();
    };

    handleChangeImageCustomSymbology = (event) => {
        let file = event.target.files[0];
        let fileReader = new FileReader();
        let image;
        let state;

        fileReader.addEventListener("load", () => {
            image = fileReader.result;
            state = update(this.props, {
                currentRenderer : {
                    customSymbology : {
                        fileName : {
                            $set : file.name
                        },
                        base64 : {
                            $set : image
                        }
                    }
                }
            });

            this.emitSimpleRenderer(state);
        }, false);

        if (!file) return;

        if (file.type.match(new RegExp("^image/*"))) {
            fileReader.readAsDataURL(file);
        } else {
            this.$appAction.alertWarn("APP_WARN_03");
        }
    };

    handleClickDeleteCustomSymbology = () => {
        let state = update(this.props, {
            currentRenderer : {
                customSymbology : {
                    $set : {}
                }
            }
        });

        delete state.currentRenderer.customSymbology;

        this.emitSimpleRenderer(state);
    };

    emitSimpleRenderer = (state) => {
        this.props.onChange({
            currentRenderer : ANALYSIS_TYPE.STATIC,
            state : state.currentRenderer
        });
    };

    handleIconTextChange = (event, index, value) => {
        let state = update(this.props, {
            currentRenderer : {
                customSymbology : {
                    iconText : {
                        $set: value
                    }
                }
            }
        });

        this.emitSimpleRenderer(state);
    };

    handleChangeCustomSymbologyFontColor = (color) => {
        let newState = update(this.props, {
            currentRenderer : {
                customSymbology : {
                    fontColor : {
                        $set: color.hex
                    }
                }
            }
        });

        this.emitSimpleRenderer(newState);
    };

    handleChangeCustomSymbologyFontSize = (size) => {
        let newState = update(this.props, {
            currentRenderer : {
                customSymbology : {
                    fontSize : {
                        $set: size
                    }
                }
            }
        });

        this.emitSimpleRenderer(newState);
    };

    renderSelectIconText(){
        return this.props.outFields.map((field) => {
            return <MenuItem key={field.name} value={field.name} label={field.alias} primaryText={field.alias}/>;
        });
    }

    render () {
        return (
            <section className="cmaps-type-static-container">
                <div className="cmaps-static-color-picker-container">
                    <ColorPicker value={this.props.currentRenderer.fill}
                                 onChange={this.handleFillChanged}
                                 label={locale.getLabel('L0012')} />

                    <ColorPicker value={this.props.currentRenderer.outlineColor}
                                 onChange={this.handleOutlineColorChanged}
                                 label={locale.getLabel('L0013')} />

                    <div style={{ width : '30%' }}>
                        {this.props.currentGeometryType === GEOMETRY_TYPE.POINT && (
                            <InputRange value={Number(this.props.currentRenderer.size)}
                                        step={1}
                                        min={1}
                                        max={100}
                                        onDragStop={this.handleSizeChanged}
                                        label={locale.getLabel('L0019')} />
                        )}
                    </div>
                </div>


                <Checkbox label={locale.getLabel('L0062')}
                          checked={this.state.customSymbologyIsVisible}
                          onCheck={this.handleCustomSymbologyVisibilityChange}
                          checkedIcon={<Visibility />}
                          uncheckedIcon={<VisibilityOff />}/>

                {this.state.customSymbologyIsVisible && (
                    <Card style={{width: '60%'}}>
                        <CardText>
                            <div className="cmaps-type-static-custom-symbology">
                                <Avatar src={this.props.currentRenderer.customSymbology.base64}
                                        onClick={this.handleCLickUploadImageCustomSymbology}
                                        style={{cursor : "pointer"}}
                                        backgroundColor={"transparent"}/>

                                <div className="cmaps-type-static-custom-symbology-text-box">
                                    <div className="cmaps-title">{locale.getLabel("L0063")}</div>
                                    <div className="cmaps-subtitle">{this.props.currentRenderer.customSymbology.fileName}</div>
                                </div>

                                <IconButton onClick={this.handleCLickUploadImageCustomSymbology}
                                            style={{width: 48, height: 48}}
                                            iconStyle={{width: 40, height: 40}}>
                                    <CloudUpload/>
                                </IconButton>

                                <input type="file"
                                       accept="image/*"
                                       onChange={this.handleChangeImageCustomSymbology}
                                       ref="customSymbologyInput"
                                       style={{display : "none"}}/>

                                <IconButton onClick={this.handleClickDeleteCustomSymbology}
                                            style={{width: 24, height: 24, position: 'absolute', right: 0, transform: 'translate3d(10px, -10px, 0)'}}
                                            iconStyle={{position: 'absolute', top: 0, right: 0}}>
                                    <DeleteForever />
                                </IconButton>
                            </div>

                            <div style={{display : 'flex'}}>
                                <div style={{width : '60%'}}>
                                    <SelectField floatingLabelText={locale.getLabel('L1010')}
                                                 floatingLabelFixed={true} style={{width: '100%'}}
                                                 value={this.props.currentRenderer.customSymbology.iconText}
                                                 onChange={this.handleIconTextChange}>
                                        <MenuItem  primaryText={locale.getLabel('L0067')}
                                                   value={""}
                                                   label={locale.getLabel('L0067')}/>
                                        {this.renderSelectIconText()}
                                    </SelectField>
                                </div>


                                <div style={{width : '40%'}}>
                                    <ColorPicker style={{minWidth : 0}}
                                                 label={locale.getLabel("L0086")}
                                                 value={this.props.currentRenderer.customSymbology.fontColor}
                                                 onChange={this.handleChangeCustomSymbologyFontColor}/>
                                </div>

                                <div>
                                    <BreakCount label={locale.getLabel("L0087")}
                                                min={8}
                                                max={20}
                                                count={this.props.currentRenderer.customSymbology.fontSize}
                                                onChange={this.handleChangeCustomSymbologyFontSize}/>
                                </div>
                            </div>
                        </CardText>
                    </Card>
                )}
            </section>
        );
    }
}

export default TypeStatic;
