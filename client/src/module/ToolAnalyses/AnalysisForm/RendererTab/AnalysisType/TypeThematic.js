import React from 'react';
import PropTypes from "prop-types";
import {MenuItem, SelectField, TextField, Toggle} from "material-ui";
import {locale} from "../../../../../i18n/Locale";
import ModuleBase from "../../../../../_base/abstract/ModuleBase";
import update from "react-addons-update";
import {
    ANALYSIS_TYPE,
    CLASSIFICATION_METHODS,
    DEFAULT_ANALYSIS_CONFIG,
    GEOMETRY_TYPE
} from '../../../../../constant/analysis';
import Accordion from "../component/Accordion";
import ThematicListBreaksComponent from "../component/ThematicListBreaksComponent";
import Converter from "../../../../../util/Converter";

const style = () => {
    return {

        SelectField : {
            marginTop : '-15px',
            padding : '0'
        },

        TextField : {
            marginTop : '-15px',
            padding : '0'
        }

    };
};

class TypeThematic extends ModuleBase {

    static propTypes = {
        currentGeometryType: PropTypes.string,
        onChange : PropTypes.func,
        onChangeAnalysis : PropTypes.func,
        currentRenderer : PropTypes.object
    };

    _accordionRef;

    constructor(props, context) {
        super(props, context);

        let uniqueValuesConfig = props.currentRenderer.uniqueValues;
        let fillInput = props.currentRenderer.fill;
        let sizeInput = props.currentRenderer.size;

        let form = this.getDefaultForm();

        if (props.analysis._id) {
            form.fill.enable = false;
            form.size.enable = false;

            if (fillInput &&
                fillInput.classificationMethod &&
                fillInput.classificationField) {
                form.fill.field = fillInput.classificationField;
                form.fill.method = fillInput.classificationMethod;
                form.fill.breakCount = fillInput.breakCount;
                form.fill.enable = true;
            }

            if (uniqueValuesConfig &&
                uniqueValuesConfig.field) {
                form.fill.field = uniqueValuesConfig.field;
                form.fill.enable = true;
                form.fill.method = 'UniqueValues';
            }

            if (sizeInput &&
                sizeInput.classificationMethod &&
                sizeInput.classificationField) {
                form.size.field = sizeInput.classificationField;
                form.size.method = sizeInput.classificationMethod;
                form.size.breakCount = sizeInput.breakCount;
                form.size.enable = true;
            }
        }

        this.state = {
            isFormChangePendent : true,

            /**
             * @type {IAnalysis}
             */
            analysis : props.analysis,

            form,

            currentRenderer : props.currentRenderer || {},

            /**
             * @type {IOutField[]}
             */
            outFields : props.outFields || []
        };
    }

    getDefaultForm(type = '') {
        let all = {
            fill : {
                enable : true,
                method : '',
                field : '',
                breakCount : 4
            },
            size : {
                enable : false,
                breakCount : 4,
                method : '',
                field : ''
            }
        };

        return type ? all[type] : all;
    }

    loadThematicListBreaksComponent = () => {
        return new Promise((resolve, reject) => {
            ThematicListBreaksComponent.getConfigBreaks(this.state.analysis, this.context.$parent)
                .then((breaks) => {
                    resolve(
                        <ThematicListBreaksComponent
                            onChangeAnalysis={this.props.onChangeAnalysis}
                            analysis={this.state.analysis}
                            breaks={breaks}/>
                    );
                })
                .catch(error => {
                    console.error(error);
                    resolve(<div style={{ color : '#900' }}>
                        Não foi possível carregar as quebras.
                    </div>);
                });
        });
    };

    onChangeForm = (typeOrParamsToUpdate, formField, value) => {
        let form = this.state.form;

        if (typeof typeOrParamsToUpdate === 'string') {
            form = update(this.state.form, {
                [typeOrParamsToUpdate] : {
                    [formField] : {
                        $set : value
                    }
                }
            });
        } else {
            form = update(form, typeOrParamsToUpdate);
        }


        let analysis = this.getNormalizedAnalysis(form);

        this.setState({ form, analysis, isFormChangePendent : true });

        if (this._accordionRef) {
            this._accordionRef.setState({
                children : null,
                collapsed : true
            });
        }

        this.props.onChange({
            currentRenderer : ANALYSIS_TYPE.THEMATIC,
            state : analysis.configRenderer.thematic
        });
    };

    /**
     *
     * @param form
     * @returns {IAnalysis}
     */
    getNormalizedAnalysis(form = this.state.form) {
        let sizeForm = form.size;
        let fillForm = form.fill;
        /** @type {IAnalysis} */
        let analysis = update(this.state.analysis, {
            configRenderer : {
                thematic : {
                    $set : {}
                }
            }
        });

        if (sizeForm.field && sizeForm.method) {
            analysis.configRenderer.thematic.size = {
                maxSize: DEFAULT_ANALYSIS_CONFIG.configRenderer.thematic.size.maxSize,
                minSize: DEFAULT_ANALYSIS_CONFIG.configRenderer.thematic.size.minSize,
                breakCount : sizeForm.breakCount,
                classificationMethod : sizeForm.method,
                classificationField : sizeForm.field
            };
        }

        if (fillForm.field &&
            fillForm.method &&
            fillForm.method !== 'UniqueValues'
        ) {
            analysis.configRenderer.thematic.fill = {
                colorRamp : DEFAULT_ANALYSIS_CONFIG.configRenderer.thematic.fill.colorRamp,
                breakCount : fillForm.breakCount,
                classificationMethod : fillForm.method,
                classificationField : fillForm.field
            };

        } else if (fillForm.method === 'UniqueValues') {
            analysis.configRenderer.thematic.uniqueValues = {
                field : fillForm.field,
                colorRamp : DEFAULT_ANALYSIS_CONFIG.configRenderer.thematic.uniqueValues.colorRamp
            };
        }

        return analysis;
    }

    renderClassificationMethods = (type) => {
        let classificationMethods = [];
        let currentField = this.state.outFields.filter(outfield => (
            this.state.form[type].field === outfield.name
        ));
        currentField = (currentField && currentField.length) && currentField.pop();

        let form = this.state.form;

        if (!form[type].field) {
            return;
        }

        for (let classificationMethod of CLASSIFICATION_METHODS) {
            if (classificationMethod.name === 'UniqueValues') {
                if (type === 'fill') {
                    classificationMethods.push(classificationMethod);
                }

            } else if (currentField.valueType === 'raw' || currentField.valueType === 'number') {
                classificationMethods.push(classificationMethod);
            }
        }

        return (
            <SelectField
                style={style().SelectField}
                autoWidth={true}
                floatingLabelText={locale.getLabel('L0092')}
                floatingLabelFixed={true}
                value={form[type].method}
                onChange={(event, key, payload) => {
                    let form = Converter.deepClone(this.state.form);

                    if (payload === 'UniqueValues') {
                        delete form[type].breakCount;
                    } else {
                        form[type].breakCount = form[type].breakCount || 4;
                    }

                    this.setState({ form }, () => {
                        this.onChangeForm(type, 'method', payload);
                    });
                }}
            >
                { classificationMethods.map((classificationMethod, index) => {
                    return (
                        <MenuItem
                            key={index}
                            value={classificationMethod.name}
                            primaryText={classificationMethod.alias}
                        />
                    );
                }) }
            </SelectField>
        );
    };

    renderClassificationFields = (type) => {
        let form = this.state.form;
        let outFields = [];

        this.state.outFields.forEach((outField) => {
            if (type === 'size') {

                if (outField.valueType === 'number') {
                    outFields.push(outField);
                }

            } else {
                outFields.push(outField);
            }
        });

        return (
            <SelectField
                style={style().SelectField}
                autoWidth={true}
                floatingLabelText={locale.getLabel('L1023')}
                floatingLabelFixed={true}
                value={form[type].field}
                onChange={(event, key, payload) => {
                    let config = this.getDefaultForm(type);

                    if (type === 'size') {
                        config = update(config, {
                            enable : {
                                $set : true
                            }
                        });
                    }

                    this.setState({
                        form : update(this.state.form, {
                            [type] : {
                                $set : config
                            }
                        })
                    }, () => {
                        this.onChangeForm(type, 'field', payload);
                    });
                }}
            >
                { outFields.map((outField, index) => {
                    return (
                        <MenuItem
                            key={index}
                            value={outField.name}
                            primaryText={outField.alias}
                        />
                    );
                }) }
            </SelectField>
        );
    };

    renderBreakCount = (type) => {
        if (this.state.form[type] &&
            this.state.form[type].method &&
            this.state.form[type].method !== 'UniqueValues') {

            return (
                <div className="cmaps-row">
                    <div className="cmaps-col-6">
                        <TextField
                            style={style().TextField}
                            floatingLabelText={locale.getLabel('L0017')}
                            floatingLabelFixed={true}
                            value={this.state.form[type].breakCount || 4}
                            type={'number'}
                            onChange={(event, newValue) => {
                                newValue = Number(newValue);
                                if (newValue >= 12) {
                                    newValue = 12;
                                } else if (newValue <= 4) {
                                    newValue = 4;
                                }
                                this.onChangeForm(type, 'breakCount', newValue);
                            }}
                        />
                    </div>
                </div>
            );
        }
    };

    onToggleType = (type, isInputChecked) => {
        let form = this.state.form;

        if (!isInputChecked) {
            form = {
                [type] : {
                    $set : Object.assign({}, this.getDefaultForm(type), {
                        enable : isInputChecked
                    })
                }
            };
        } else {
            form = {
                [type] : {
                    enable : {
                        $set : isInputChecked
                    }
                }
            };
        }

        this.onChangeForm(form);
    };

    render() {
        return (
            <div className={this.className()}>

                {/*FILL*/}
                <div className="cmaps-row">

                    {this.props.currentGeometryType === GEOMETRY_TYPE.POINT &&
                    <div className="cmaps-row">

                        <div className="cmaps-col-5">
                            <Toggle
                                label={locale.getLabel('L0018')}
                                toggled={this.state.form.fill.enable}
                                onToggle={(event, isInputChecked) => {
                                    this.onToggleType('fill', isInputChecked);
                                }}
                                style={{width: '100%'}}
                            />
                        </div>
                    </div>
                    }

                    { this.state.form.fill.enable &&
                    <div className="cmaps-row">

                        <div className="cmaps-row">
                            <div className="cmaps-col-6">
                                { this.renderClassificationFields('fill') }
                            </div>

                            <div className="cmaps-col-6">
                                {this.renderClassificationMethods('fill')}
                            </div>
                        </div>

                        { this.renderBreakCount('fill') }

                        {this.state.form.fill.method && this.state.form.fill.field &&
                        <div className="cmaps-row">
                            <div className="cmaps-col-12">
                                <Accordion
                                    ref={el => this._accordionRef = el}
                                    label={locale.getLabel('L0093')}
                                    onCollapse={(instance, collapsed) => {
                                        if (collapsed === false && this.state.isFormChangePendent) {
                                            this.loadThematicListBreaksComponent()
                                                .then(component => {
                                                    instance.setState({
                                                        children: component
                                                    });
                                                    this.setState({
                                                        isFormChangePendent : false
                                                    });
                                                });
                                        }
                                    }}
                                />
                            </div>
                        </div>
                        }

                    </div>
                    }

                </div>

                {/*SIZE*/}
                {this.props.currentGeometryType === GEOMETRY_TYPE.POINT &&
                <div className="cmaps-row"
                     style={{
                         borderTop : '1px solid #ccc',
                         paddingTop : 10
                     }}
                >

                    <div className="cmaps-row">

                        <div className="cmaps-col-5">
                            <Toggle
                                label={locale.getLabel('L0014')}
                                toggled={this.state.form.size.enable}
                                onToggle={(event, isInputChecked) => {
                                    this.onToggleType('size', isInputChecked);
                                }}
                                style={{width: '100%'}}
                                disabled={this.props.currentGeometryType !== GEOMETRY_TYPE.POINT}
                            />
                        </div>
                    </div>

                    {this.state.form.size.enable &&
                    <div className="cmaps-row">

                        <div className="cmaps-row">
                            <div className="cmaps-col-6">
                                {this.renderClassificationFields('size')}
                            </div>

                            <div className="cmaps-col-6">
                                {this.renderClassificationMethods('size')}
                            </div>
                        </div>

                        { this.renderBreakCount('size') }

                    </div>
                    }

                </div>
                }

            </div>
        );
    }

}

export default TypeThematic;