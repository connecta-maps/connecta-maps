import React from "react";
import {DEFAULT_ANALYSIS_CONFIG} from "../../../../constant/analysis";
import Converter from "../../../../util/Converter";
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import TabBase from "../TabBase";
import update from "react-addons-update";
import {MenuItem} from "material-ui";
import {locale} from "../../../../i18n/Locale";

export default class FieldsTab extends TabBase {

    /**
     * @type {{ onChangeAnalysis:Function }}
     */
    props;

    constructor(props, context) {
        super(props, context);
        this.setClassName('analysis-form-fields-tab');

        this._maskDefinitions = locale.getMaskDefinitions();

        this.state = {
            outFields : props.outFields || [],
            /**
             * @type {IAnalysis}
             */
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis)
        };
    }

    onChangeCheckedField = (rowIndex) => {
        let outField = this.state.outFields[rowIndex];
        let outFieldsConfig = Object.assign({}, this.state.analysis.outFieldsConfig);

        let checked = !this.state.analysis.outFieldsConfig[outField.name].checked;

        if (!checked) {
            outFieldsConfig[outField.name] =
                Object.assign({}, outField, { checked });

            this.props.onChangeAnalysis(Object.assign({}, this.state.analysis, { outFieldsConfig }));

        } else {
            this.changeOutFieldConfig(outField.name, {  checked });
        }
    };

    onChangeTypeMask = (outField, typeMask) => {
        let config = {typeMask};
        config.config = this._maskDefinitions[typeMask];
        this.changeOutFieldConfig(outField.name, config);
    };

    changeOutFieldConfig(outFieldName, configToMixin) {
        let outFieldsConfigCloned = Object.assign({}, this.state.analysis.outFieldsConfig || {});
        outFieldsConfigCloned = Object.assign(outFieldsConfigCloned, {
            [outFieldName] : Object.assign({}, outFieldsConfigCloned[outFieldName] || {}, configToMixin)
        });
        let analysis = update(this.state.analysis, {
            outFieldsConfig : {
                $set : outFieldsConfigCloned
            }
        });
        this.props.onChangeAnalysis(analysis);
    }

    render() {
        let masks = Object.keys(this._maskDefinitions);

        let outFields = this.state.outFields.sort((a, b) =>  a.name.localeCompare(b.name) );
        return (
            <div className={this.className()}>
                <div className={this.className('row')}>
                    <div className={this.className('column col12')} />
                    <div className={this.className('column col3')}>
                        <span style={{fontWeight : 'bold'}}>{locale.getLabel('L1026')}</span>
                    </div>
                    <div className={this.className('column col3')}>
                        <span style={{fontWeight : 'bold'}}>{locale.getLabel('L1027')}</span>
                    </div>
                    <div className={this.className('column col3')}>
                        <span style={{fontWeight : 'bold'}}>{locale.getLabel('L1025')}</span>
                    </div>
                </div>
                { outFields.map((outField, index) => {
                    let config = this.state.analysis.outFieldsConfig[outField.name];
                    let checked = Boolean(config ? config.checked : true);
                    return (
                        <div key={index} className={this.className('row')}>
                            <div className={this.className("column col12")}>
                                <Checkbox
                                    onCheck={() => this.onChangeCheckedField(index)}
                                    checked={checked} />
                            </div>
                            <div className={this.className("column col3")}>
                                {outField.name}
                            </div>
                            <div className={this.className("column col3")}>
                                <TextField
                                    name={'label'}
                                    disabled={!checked}
                                    onChange={(event, label) => this.changeOutFieldConfig(outField.name, {label})}
                                    value={config ? config.label : outField.name}
                                />
                            </div>
                            <div className={this.className("column col3")}>
                                <SelectField
                                    style={{
                                        position : 'absolute',
                                        top : '4px',
                                        width : 'auto',
                                        left: 0,
                                        right : 10
                                    }}
                                    autoWidth={true}
                                    disabled={!checked}
                                    value={config && config.typeMask || 'none'}
                                    onChange={(event, b, type) => {
                                        this.onChangeTypeMask(outField, type);
                                    }}>
                                    <MenuItem value={'none'} primaryText={locale.getLabel('L1028')} />

                                    { masks.map((maskName, index) => {
                                        return (
                                            <MenuItem
                                                key={index}
                                                value={maskName}
                                                primaryText={locale.getLabel(maskName)} />
                                        );
                                    }) }
                                </SelectField>
                            </div>
                        </div>
                    );
                }) }
            </div>
        );
    }

}