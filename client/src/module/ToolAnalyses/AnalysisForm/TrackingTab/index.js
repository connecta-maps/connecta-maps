import React from "react";
import {MenuItem, SelectField} from "material-ui";
import InputGradient from "../../../../component/InputGradient";
import {locale} from "../../../../i18n/Locale";
import update from "react-addons-update";
import Converter from "../../../../util/Converter";
import {DEFAULT_ANALYSIS_CONFIG} from "../../../../constant/analysis";
import TabBase from "../TabBase";

export default class TrackingTab extends TabBase {

    /**
     * @type { { onChangeAnalysis : Function } }
     */
    props;

    constructor(props, context) {
        super(props, context);
        this.setClassName('analysis-form-tracking-tab');

        this.state = {
            outFields : props.outFields || [],
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis)
        };
    }

    handleFilterFieldFieldChanged = (event, index, value) => {
        let analysis = update(this.state.analysis, {
            trackingConfig : {
                filterField : {
                    $set : value
                }
            }
        });
        this.props.onChangeAnalysis(analysis);
    };

    handleTemporalDimensionFieldChanged = (event, index, value) => {
        let analysis = update(this.state.analysis, {
            trackingConfig : {
                temporalDimensionField : {
                    $set : value
                }
            }
        });
        this.props.onChangeAnalysis(analysis);
    };

    handleInitialColorRampChange = (color) => {
        let analysis = update(this.state.analysis, {
            trackingConfig : {
                colors : {
                    $splice : [[0, 1, color]]
                }
            }
        });

        this.props.onChangeAnalysis(analysis);
    };

    handleFinalColorRampChange = (color) => {
        let analysis = update(this.state.analysis, {
            trackingConfig : {
                colors : {
                    $splice : [[1, 1, color]]
                }
            }
        });
        this.props.onChangeAnalysis(analysis);
    };

    render() {
        return (
            <div className={this.className()}>

                <div style={{
                    display : 'flex',
                    flexDirection : 'row'
                }}>
                    <div style={{ marginRight : '15px' }}>
                        <SelectField
                            value={this.state.analysis.trackingConfig.filterField}
                            onChange={this.handleFilterFieldFieldChanged}
                            floatingLabelText={locale.getLabel('L0083')}>

                            {this.state.outFields.map((item, index) => {
                                return <MenuItem key={index} value={item.name} primaryText={item.alias} />
                            })}
                        </SelectField>
                    </div>

                    <div>
                        <SelectField
                            value={this.state.analysis.trackingConfig.temporalDimensionField}
                            onChange={this.handleTemporalDimensionFieldChanged}
                            floatingLabelText={locale.getLabel('L0084')}>

                            {this.state.outFields.map((item, index) => {
                                return <MenuItem key={index} value={item.name} primaryText={item.alias} />
                            })}
                        </SelectField>
                    </div>
                </div>

                <InputGradient label={locale.getLabel('L0028')}
                               onInitialColorChange={this.handleInitialColorRampChange}
                               onFinalColorChange={this.handleFinalColorRampChange}
                               initialColor={this.state.analysis.trackingConfig.colors[0]}
                               finalColor={this.state.analysis.trackingConfig.colors[1]}/>
            </div>
        );
    }

}