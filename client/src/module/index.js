import React from "react";
import PropTypes from "prop-types";
import {scope} from "../scope";
import {toolsConfig} from "../tools.config";
import {MAP_CONSTANT} from "../constant/map";
import {VIEWER} from "../constant/viewer";
import ModuleBase from "../_base/abstract/ModuleBase";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import theme from "../theme.config";
import WidgetPopup from "./WidgetPopup";
import MessageComponent from "../component/MessageComponent";
import WidgetTimeline from "./WidgetTimeline";
import WidgetSwipe from "./WidgetSwipe";
import WidgetLegend from "./WidgetLegend";
import ToolbarComponent from "../component/Toolbar";
import ReactTooltip from 'react-tooltip';

export default class AppModule extends React.Component {

    mapsTheme = getMuiTheme(theme);

    /**
     * @type { { $parent : ConnectaMaps, viewerId:String } }
     */
    props;

    static propTypes = {
        $parent:PropTypes.any,
        viewer:PropTypes.any,
        user:PropTypes.any
    };

    static childContextTypes = {
        $parent : PropTypes.any
    };

    getChildContext() {
        return {
            $parent : this.props.$parent
        };
    }

    handlers = [];

    _iconsToolsNode = [];

    constructor(props) {
        super(props);
        this.$viewerStore = this.props.$parent.inject(scope.ViewerStore);
        this.$appStore = this.props.$parent.inject(scope.AppStore);
        this.$appAction = this.props.$parent.inject(scope.AppAction);
        this.$viewerAction = this.props.$parent.inject(scope.ViewerAction);
        this.$mapAction = this.props.$parent.inject(scope.MapAction);
        this.$mapStore = this.props.$parent.inject(scope.MapStore);

        this.state = {
            openTool : false,
            currentTool : toolsConfig.currentTool,
            toolsConfig : null,
            viewerId : {}
        };
    }

    destroy () {
        this.setState({
            viewer : null
        });
        setTimeout(() => {
            this.$mapAction.destroy();
        }, 10);
    }

    componentDidMount() {
        this.handlers = ModuleBase.own(
            this.$viewerStore.listen(VIEWER.LOAD, this.onGetViewer),
            this.$viewerStore.listen(VIEWER.SAVE_VIEWER, this.onSaveViewer),
            this.$viewerStore.listen(VIEWER.UPDATE, this.onUpdateViewer),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_MAP, this.onMapLoad)
        );
        this.$viewerAction.getViewer(this.props.viewerId, this.props.user);
    }

    componentWillUnmount() {
        for (let handler of this.handlers) {
            handler.remove();
        }
    }

    onSaveViewer = () => {
        this.$appAction.alertSuccess('APP_SUCCESS_03');
    };

    onMapLoad = () => {
        this.setState({
            toolsConfig : toolsConfig
        });
    };

    onGetViewer = (viewer) => {
        this.$mapAction.buildMap(this.refs.mapNode, viewer.project.mapConfig);
        this.setState({
            viewer : viewer
        });
    };

    onUpdateViewer = (viewer) => {
        this.setState({
            viewer : viewer
        });
    };

    updateViewer(viewerId){
        this.$viewerAction.updateViewer(viewerId);
    };

    buildIconTools() {
        if (!this.state.toolsConfig) {
            return;
        }

        this._iconsToolsNode = [];

        let nodes = [];
        let currentTool = toolsConfig.tools.filter((tool) => this.state.currentTool === tool.name)[0];
        let Classes = [];
        let width = this.state.openTool ? 360 : 0;

        nodes[0] = (
            <div key="t0" className="cmaps-tool-icons">
                {
                    toolsConfig.tools.map((tool, index) => {
                        let active = (
                            tool.name === this.state.currentTool &&
                            this.state.openTool === true
                        )
                            ? 'cmaps-active' :
                            '';
                        let Tool = tool.Class;

                        Classes.push(<div key={index} className="cmaps-tool-container"
                                          style={{ display : tool.name === this.state.currentTool ? 'block' : 'none'}}>
                            <Tool />
                        </div>);

                        return (
                            <div key={index}
                                 onClick={ () => {
                                     if (this.state.currentTool === tool.name) {
                                         this.setState({
                                             openTool : !this.state.openTool
                                         });
                                     } else {
                                         this.setState({
                                             currentTool : tool.name,
                                             openTool : true
                                         });
                                     }
                                 } }
                                 ref={(el) => this._iconsToolsNode.push(el)}
                                 data-tip={ tool.label || tool.name }
                                 className={'cmaps-tool-icon ' + active}>{toolsConfig.tools[index].icon}</div>
                        );
                    })
                }
            </div>
        );

        let ButtonLabel = currentTool.ButtonLabel;

        nodes[1] = (
            <div key="t1"
                 className="cmaps-tool-current"
                 style={{
                     width : width
                 }}>
                <div className="cmaps-tool-title">
                    { currentTool ? currentTool.label : '' }
                    {
                        currentTool.ButtonLabel &&
                        <div style={{ position: 'absolute', right: 0, top : 16 }}>
                            <ButtonLabel $parent={this.props.$parent} />
                        </div>
                    }
                </div>
                {Classes}
            </div>
        );

        return nodes;
    }

    toggleTools = () => {
        setTimeout(() => {
            this.$appAction.toggleTools(this.state.openTool);
        }, 500);

        this.setState({
            openTool : !this.state.openTool
        });
    };

    render() {
        let width = this.state.openTool ? 400 : 0;
        let viewerIsBuilt = Boolean(this.state.viewer);

        let hasTools = Boolean(
            this.state.toolsConfig && this.state.toolsConfig.tools.length &&
            this.state.viewer && (this.state.openTool === false)
        );

        return (
            <MuiThemeProvider muiTheme={this.mapsTheme}>
                <div style={{
                    height: '100%',
                    width: '100%',
                    display : 'flex',
                    flexDirection:'column'
                }}>
                    {
                        this.state.viewer &&
                        <ToolbarComponent/>
                    }

                    <section className="Maps">

                        <div className="cmaps-tools" style={{
                            left: 0
                        }}>
                            {this.buildIconTools()}
                        </div>

                        <div className="cmaps-map-container">
                            <div ref="mapNode" className="cmaps-map" />
                            {viewerIsBuilt && (<WidgetPopup />)}
                            {viewerIsBuilt && (<WidgetTimeline />)}
                            {viewerIsBuilt && (<WidgetSwipe />)}
                            {viewerIsBuilt && (<WidgetLegend />)}
                        </div>

                        <MessageComponent $parent={this.props.$parent}/>
                        {hasTools &&
                        <ReactTooltip/>
                        }
                    </section>
                </div>
            </MuiThemeProvider>
        );
    }

}