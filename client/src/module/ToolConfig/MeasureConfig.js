import React from "react";
import ModuleBase from "../../_base/abstract/ModuleBase";
import {MenuItem, SelectField} from "material-ui";
import {locale} from "../../i18n/Locale";
import update from 'react-addons-update';

export default class MeasureConfig extends ModuleBase {

    _lengthUnity = [
        {
            label: locale.getLabel('L4002'),
            value: 'meters'
        },
        {
            label: locale.getLabel('L4004'),
            value: 'kilometers'
        }
    ];
    _areaUnity = [
        {
            label: locale.getLabel('L4006'),
            value: 'hectares'
        },
        {
            label: locale.getLabel('L4008'),
            value: 'sqmeters'
        },
        {
            label: locale.getLabel('L40010'),
            value: 'squaredKilometers'
        }
    ];

    constructor(props, context) {
        super(props, context);

        let viewer = this.$viewerStore.viewer;

        this.state = {
            measureConfig :{
                lengthUnity: viewer.measureConfig ? this.$viewerStore.viewer.measureConfig.lengthUnity : 'kilometers',
                areaUnity: viewer.measureConfig ? this.$viewerStore.viewer.measureConfig.areaUnity : 'squaredKilometers'
            },
        }
    }

    handleLengthUnityChanged = (event, index, lengthUnity) => {
        let measureConfig = update(this.state.measureConfig, {
            lengthUnity: {
                $set: lengthUnity
            }
        });

        this.setState({ measureConfig });

        this.$viewerAction.changeLengthUnit(measureConfig);
    };

    handleAreaUnityChanged = (event, index, areaUnity) => {
        let measureConfig = update(this.state.measureConfig, {
            areaUnity: {
                $set: areaUnity
            }
        });

        this.setState({ measureConfig });

        this.$viewerAction.changeLengthUnit(measureConfig);
    };

    render() {
        return (
            <section>
                <div className={this.className('row')}>
                    <SelectField
                        value={this.state.measureConfig.lengthUnity}
                        autoWidth={true}
                        onChange={this.handleLengthUnityChanged}
                        floatingLabelText={locale.getLabel('L0100')}>

                        {this._lengthUnity.map((unity, index) => {
                            return <MenuItem key={index} value={unity.value} primaryText={unity.label} />
                        })}
                    </SelectField>
                </div>
                <div className={this.className('row')}>
                    <SelectField
                        value={this.state.measureConfig.areaUnity}
                        autoWidth={true}
                        onChange={this.handleAreaUnityChanged}
                        floatingLabelText={locale.getLabel('L0101')}>

                        {this._areaUnity.map((unity, index) => {
                            return <MenuItem key={index} value={unity.value} primaryText={unity.label} />
                        })}
                    </SelectField>
                </div>
            </section>
        );
    }

}