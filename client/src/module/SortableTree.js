import React from 'react';
import PropTypes from "prop-types";
import ModuleBase from '../_base/abstract/ModuleBase';
import {arrayMove, SortableContainer, SortableElement, SortableHandle} from 'react-sortable-hoc';
import update from 'react-addons-update';
import AnalysisBox from './ToolAnalyses/AnalysisBox';
import Checkbox from 'material-ui/Checkbox';
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import IconButton from "material-ui/IconButton";
import MoreVert from "material-ui/svg-icons/navigation/more-vert";
import {locale} from "../i18n/Locale";
import PlayArrow from "material-ui/svg-icons/av/play-arrow";
import EditableInput from "../component/EditableInput";
import CheckboxIndeterminate from "react-mdc-web/lib/Checkbox";
import {VIEWER} from "../constant/viewer";
import LayerBox from './ToolLayers/LayerBox';
import {scope} from "../scope";
import {MAP_CONSTANT} from "../constant/map";

let toolAnalysisGroupReferences = {};
let analysisBoxReferences = {};

//Handle do SortableElement..
const DragLabel = SortableHandle((props) => <span style={{lineHeight: '26px', cursor: 'move', width: '200px', textOverflow: 'ellipsis', overflow: 'hidden', display: 'block', whiteSpace : 'nowrap'}}>{props.title}</span>);

const newGroup = (title) => {
    return {
        _id : new Date().getTime(),
        type : TYPES.GROUP,
        title : title,
        collapsed : false,
        children : []
    }
};

class SortableTree extends ModuleBase {

    static propTypes = {
        items : PropTypes.array,
        isLayers: PropTypes.bool,
        onAddItemClick: PropTypes.func,
        onRemoveItem: PropTypes.func
    };

    static addGroupAtRoot($parent, isLayers = false) {
        /** @type {ViewerStore} */
        let $viewerStore = $parent.inject(scope.ViewerStore);
        /** @type {ViewerAction} */
        let $viewerAction = $parent.inject(scope.ViewerAction);
        let config = isLayers ? $viewerStore.layersConfig : $viewerStore.analysisConfig;
        let items = [].concat(config);
        items.push({
            _id : new Date().getTime(),
            type : 'GROUP',
            title : locale.getLabel("L1014") + ' ' + (config.length + 1),
            collapsed : false,
            children : []
        });
        $viewerAction.updateGroupRoot(items, isLayers);
    }

    _subTreeReference = {};

    constructor (props, context) {
        super(props, context);

        this.state = {
            items : props.items
        };
    }

    componentDidMount () {
        this.own(
            this.$viewerStore.listen(VIEWER.CHECK_GROUP, this.checkIndeterminateStatusGroup),
            this.$mapStore.listen(MAP_CONSTANT.MAP_ZOOM_END, this.handleMapZoomEnd)
        );
    }

    handleMapZoomEnd = ({analysisId}) => {
        if (analysisBoxReferences[analysisId]) {
            analysisBoxReferences[analysisId].handleMapZoomEnd();
        }
    };

    checkIndeterminateStatusGroup = () => {
        this.forceUpdate();
    };

    handleClickAddGroup = () => {
        SortableTree.addGroupAtRoot(this.context.$parent);
    };

    handleReorderTree = (reorderedTree) => {
        this.$viewerAction.updateGroupRoot(reorderedTree, this.props.isLayers);
    };

    rebuildChildren = (groupId, children) => {
        if (toolAnalysisGroupReferences[groupId]) {
            toolAnalysisGroupReferences[groupId].rebuildSubTreeChildren(groupId, children);
        }
    };

    render (){
        return (
            <ToolAnalysisGroupContainer
                {...this.props}
                ref={(element) => {
                    this._subTreeReference = element;
                }}
                useDragHandle={true}
                parent={true}
                onToggleSwipeAnalysis={this.props.onToggleSwipeAnalysis}
                onAddGroupClick={this.handleClickAddGroup}
                onSortEnd={({oldIndex, newIndex}) => {
                    let reorderedTree = arrayMove(this.props.items, oldIndex, newIndex);
                    this.handleReorderTree(reorderedTree);
                }}/>
        );

    };
}

class ToolAnalysisGroup extends ModuleBase {

    static propTypes = {
        onAddGroupClick : PropTypes.func,
        onAddItemClick: PropTypes.func,
        onRemoveItem : PropTypes.func,
        onRemoveGroup : PropTypes.func,
        onToggleSwipeAnalysis : PropTypes.func,
        isLayers: PropTypes.bool
    };

    incrementPadding = 20;

    _subGroupInstances = {};

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props._id,
            title: this.props.title,
            collapsed : !this.props.parent ? !!this.props.shown : true,
            startPadding: this.props.startPadding || 0,
            editGroup: false,
            items : props.items
        };
    }

    handleCollapseGroup = (event, collapsed) => {
        this.setState({
            collapsed : collapsed
        }, () => {
            this.$viewerStore.toggleGroupShown(this.state.id, collapsed, this.props.isLayers);
        });
    };

    handleForm = () => {
        if(this.props.onAddItemClick){
            this.props.onAddItemClick(this.state.id)
        }
    };

    buildDragLabel = (title) => {
        return <DragLabel title={title} />
    };

    handleReorderSubGroups = (parentId, items) => {
        this.$viewerAction.updateGroupChildren(parentId, items, this.props.isLayers);
    };

    handleCheckGroup = (checked, groupId) => {
        this.$viewerAction.handleCheckGroup(groupId, checked, this.props.isLayers);
    };

    rebuildSubTreeChildren = (groupId, children) => {
        if (groupId === this.props._id) {
            this.setState({
                items : []
            }, () => {
                this.setState({
                    items : children
                });
            });
        } else {
            for (let id in this._subGroupInstances) {
                this._subGroupInstances[id].rebuildSubTreeChildren(groupId, children);
            }
        }
    };

    buildContent = (items) => {
        this._subGroupInstances = {};

        return items.map((item, index) => {
            if (item.type === TYPES.GROUP) {
                return (
                    <WrapItem index={index} key={index}>
                        <ToolAnalysisGroupContainer items={item.children}
                                                    groupId={item._id}
                                                    index={index}
                                                    ref={(element) => {this._subGroupInstances[item._id] = element}}
                                                    isLayers={this.props.isLayers}
                                                    startPadding={this.state.startPadding + this.incrementPadding}
                                                    onAddItemClick={this.props.onAddItemClick}
                                                    onRemoveItem={this.props.onRemoveItem}
                                                    onToggleSwipeAnalysis={this.props.onToggleSwipeAnalysis}
                                                    onRemoveGroup={this.props.onRemoveGroup}
                                                    {...item}
                                                    useDragHandle={true}
                                                    pressDelay={100}
                                                    lockAxis={"y"}
                                                    onSortEnd={({oldIndex, newIndex}) => {
                                                        item.children = arrayMove(item.children, oldIndex, newIndex);
                                                        this.handleReorderSubGroups(item._id, item.children);
                                                    }} />
                    </WrapItem>
                )
            } else {
                return (
                    <WrapItem index={index}  key={index}>
                        <div className="cmaps-sortable-item-container"
                             style={{zIndex : 11}}>
                            <div className="tree-padding" style={{width: this.state.startPadding}}>&nbsp;</div>
                            {this.renderBox(item)}
                        </div>
                    </WrapItem>
                );
            }
        });
    };

    handleAddSubGroup = () => {
        let lastIndex = (this.state.items.length + 1);
        let state = update(this.props, {
            items: {
                $push: [newGroup(locale.getLabel("L1015") + lastIndex)]
            }
        });

        this.$viewerAction.updateGroupChildren(this.state.id, state.items, this.props.isLayers);
    };

    handleEditGroup = () => {
        this.setState({ editGroup: true });
    };

    handleDeleteGroup = () => {
        if(this.props.onRemoveGroup){
            this.props.onRemoveGroup(this.state.id, this.props.isLayers);
        }
    };

    handleFinishEditGroup = (value) => {
        this.$viewerAction.updateGroupLabel(this.state.id, value, this.props.isLayers);
        this.setState({ editGroup: false, title: value});
    };

    buildLabelGroup(){
        return (
            <div className="cmaps-label-group-container" style={{}}>
                {!this.props.parent && !this.props.isLayers && (
                    <CheckboxIndeterminate onChange={({target: {checked}}) => {this.handleCheckGroup(checked, this.props.groupId)}}
                                           indeterminate={this.props.indeterminate}
                                           checked={this.props.enable}
                    />
                )}

                <EditableInput onFinishEdit={this.handleFinishEditGroup}
                               isEditing={this.state.editGroup}
                               value={this.props.title}>
                    <DragLabel title={this.state.title} />
                </EditableInput>
            </div>
        );
    }

    renderBox(item){
        if(this.props.isLayers){
            return (
                <LayerBox
                    id={item._id}
                    {...item}
                    title={item.title}
                    dragHandle={this.buildDragLabel}
                    onRemoveItem={this.props.onRemoveItem}/>
            );
        }

        return (
            <AnalysisBox ref={element => analysisBoxReferences[item._id] = element}
                         id={item._id}
                         groupId={this.props.groupId}
                         onToggleSwipeAnalysis={this.props.onToggleSwipeAnalysis}
                         dragHandle={this.buildDragLabel}
                         onRemove={this.props.onRemoveItem} />
        );
    }

    canExpandGroup(items){
        let canExpand = false;

        for(let item of items) {
            if(item.enable){
                canExpand = true;
                break;
            }
        }
        return canExpand;
    }

    verifyChildTypes = (items) => {
        let arr = [];

        arr = items.filter((item) => {
            return item.type !== TYPES.GROUP;
        });

        return !arr.length ? " not-have-child-items" : "";
    };

    render () {
        let classNameHeader = "cmaps-sortable-group-header" + (this.verifyChildTypes(this.state.items));
        return  (
            <div className="cmaps-sortable-tree-component">
                <div className={classNameHeader}
                     style={{
                         paddingLeft: this.state.startPadding,
                         display : this.props.parent ? 'none' : 'flex'
                     }}>

                    {!this.props.parent && (
                        <Checkbox label={this.buildLabelGroup()}
                                  checked={this.state.collapsed}
                                  onCheck={this.handleCollapseGroup}
                                  checkedIcon={<PlayArrow style={{transform: 'rotate(90deg)', margin : '8px 0 0 -1px'}} />}
                                  uncheckedIcon={<PlayArrow style={{margin : '8px 0 0 -1px'}}/>}
                                  style={{width: '10%', display: 'inline-block', marginRight : 0}}
                                  iconStyle={{marginRight : 0}} />
                    )}

                    {!this.props.parent && (
                        <div className="cmaps-sortable-group-header-menu-container">
                            <IconMenu
                                iconButtonElement={<IconButton style={{transition: 'none'}}><MoreVert /></IconButton>}
                                anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                style={{cursor: 'point', zIndex : 3, height : 40, transform: 'translateY(-4px)'}}>
                                <MenuItem primaryText={locale.getLabel('L1005')} onTouchTap ={this.handleEditGroup}/>
                                <MenuItem primaryText={locale.getLabel('L1006')} onTouchTap={this.handleDeleteGroup}/>
                                <MenuItem primaryText={locale.getLabel('L1016')}
                                          onTouchTap={this.handleAddSubGroup}/>
                                <MenuItem primaryText={locale.getLabel(this.props.isLayers ? "L1019" : "L1017")}
                                          onTouchTap={this.handleForm}/>
                            </IconMenu>
                        </div>
                    )}

                </div>

                <div className="cmaps-sortable-group-content"
                     style={{display : this.state.collapsed ? "block" : "none"}}>

                    {this.props.isLayers ? (this.state.collapsed || this.canExpandGroup(this.state.items)) && this.buildContent(this.state.items)
                        : this.buildContent(this.state.items)}
                </div>
            </div>
        );
    }
}

class MeuPe1 extends React.Component{
    render() {
        return (
            <ToolAnalysisGroup ref={(element) => {
                this.props._id && (toolAnalysisGroupReferences[this.props._id] = element)
            }} {...this.props} />
        );
    }
}

class MeuPe2 extends React.Component{
    render() {
        return this.props.children;
    }
}

const ToolAnalysisGroupContainer = SortableContainer(MeuPe1, {withRef: true});

const WrapItem = SortableElement(MeuPe2, {withRef: true});

const TYPES = {
    GROUP : "GROUP",
    ANALYSIS : "ITEM"
};

export default SortableTree;