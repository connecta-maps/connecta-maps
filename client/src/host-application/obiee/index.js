import HostApplicationBase from "../../_base/abstract/HostApplicationBase";

export default class OBIEE extends HostApplicationBase{

    $getResultSetsMetadata() {

    }

    build() {
        removeStyles(document.querySelectorAll('style'));
        removeStyles(document.querySelectorAll('link'));

        document.body.appendChild(this.$parent.node);
        // document.body.appendChild(document.getElementById('DashboardPageContentDiv'));

        let style = document.createElement('style');
        style.innerHTML = `
        body{
                overflow: hidden !important;
            }
            #root{
                width:100%;
                height : 100%;
                /*position:fixed;*/
                /*top:0;*/
                /*left:0;*/
                z-index:100;
            }
        `;

        if (!(/EditDashboard/.test(document.location.search))) {
            document.head.appendChild(style);
        }

        function removeStyles(elements) {
            let childrenToRemove = [];
            let i = 0;

            while (i < elements.length) {
                let html = elements[i].innerHTML || '';
                let keep = elements[i].dataset.keep;

                if (!(/\.Maps/.test(html)) && !keep) {
                    childrenToRemove.push(elements[i]);
                }

                i += 1;
            }

            i = 0;
            while (i < childrenToRemove.length) {
                childrenToRemove[i].remove();
                i += 1;
            }
        }
    }

}