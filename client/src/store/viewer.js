import {VIEWER} from "../constant/viewer";
import StoreBase from "../_base/abstract/StoreBase";
import ViewerController from "./controller/viewer";
import {MapsDataLoader} from "../MapsDataLoader";
import Converter from "../util/Converter";

export default class ViewerStore extends StoreBase {

    _richLayersMap = {};

    _layersMap = {};

    _legends = {};

    /**
     * @type {IViewer}
     */
    _viewer;

    _analyses = {};

    _layers = {};

    _timelineValue;

    _swipedAnalysisId;

    _swipedValue;

    listOfAnalysisIds;

    _trackingLayer = {};

    events = {
        [VIEWER.CHANGE_ANALYSIS_TYPE]: {},
        [VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY]: {},
        [VIEWER.ENABLE_ANALYSIS]: {},
        [VIEWER.DISABLE_ANALYSIS]: {},
        [VIEWER.ENABLE_STATIC_LAYER]: {},
        [VIEWER.DISABLE_STATIC_LAYER]: {},
        [VIEWER.CHECK_GROUP] : {},
        [VIEWER.TOGGLE_SWIPE]: {},
        [VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS] : {},
        [VIEWER.CLICK_ON_SAVE_NEW_LAYER] : {},
        [VIEWER.SAVE_NEW_LAYER]: {
            path: 'viewer.toolsConfig.layersConfig'
        },
        [VIEWER.SAVE_ANALYSIS] : {},
        [VIEWER.REORDER_ANALYSES]: {
            path : 'viewer.toolsConfig.analysisConfig'
        },
        [VIEWER.REORDER_STATIC_LAYERS]: {
            path: 'viewer.toolsConfig.layersConfig'
        },
        [VIEWER.GET_ANALYSIS_CONFIG]: {
            path : 'viewer.toolsConfig.analysisConfig'
        },
        [VIEWER.LOAD] : {
            path : 'viewer'
        },
        [VIEWER.UPDATE] : {
            path : 'viewer'
        },
        [VIEWER.EDIT_ANALYSIS] : {},
        [VIEWER.DELETE_ANALYSIS] : {},
        [VIEWER.SAVE_VIEWER] : {},
        [VIEWER.SAVE_PROJECT] : {},
        [VIEWER.DELETE_STATIC_LAYER] : {
            path: 'viewer.toolsConfig.layersConfig'
        },
        [VIEWER.GET_LAYER_CONFIG] : {
            path : 'viewer.toolsConfig.layersConfig'
        },
        [VIEWER.ADD_ANALYSIS_BOX] : {},
        [VIEWER.ADD_LAYER_BOX] : {},
        [VIEWER.REMOVE_ANALYSIS_BOX] : {},
        [VIEWER.REMOVE_LAYER_BOX] : {},
        [VIEWER.SHOW_ANALYSIS_FORM] : {},
        [VIEWER.DELETE_GROUP] : {
            path : 'viewer.toolsConfig.analysisConfig'
        },
        [VIEWER.DISABLE_SWIPE] : {},
        [VIEWER.ACTIVATE_TRACKING] : {},
        [VIEWER.DEACTIVATE_TRACKING] : {},
        [VIEWER.TRACKING_LAYER_BUILDING] : {},
        [VIEWER.TRACKING_LAYER_BUILD_SUCCESS] : {},
        [VIEWER.TRACKING_LAYER_BUILD_ERROR] : {},
        [VIEWER.CHANGE_LENGTH_UNIT] : {}
    };

    /**
     *
     * @return {ViewerController}
     * @override
     */
    getControllerClass() {
        return ViewerController;
    }

    set swipedAnalysisId(value){
        this._swipedAnalysisId = value;
    }

    get swipedAnalysisId(){
        return this._swipedAnalysisId;
    }

    set swipedValue(value){
        this._swipedValue = value;
    }

    get swipedValue(){
        return this._swipedValue;
    }

    set timelineValue(value){
        this._timelineValue = value;
    }

    get timelineValue(){
        return this._timelineValue;
    }

    /**
     *
     * @returns {IViewer}
     */
    get viewer() {
        return this._viewer;
    }

    /**
     *
     * @param {IViewer} viewer
     */
    set viewer(viewer) {
        this._viewer = viewer;

        if (!viewer.toolsConfig) {
            viewer.toolsConfig = {};
        }

        if (!viewer.toolsConfig.analysisConfig) {
            viewer.toolsConfig = {analysisConfig : []};
        }

        if (!viewer.toolsConfig.layersConfig) {
            viewer.toolsConfig.layersConfig = [];
        }

        if (viewer.toolsConfig && viewer.toolsConfig.analysisConfig){
            let allAnalyses = [];
            this.getAllAnalyses(viewer.toolsConfig.analysisConfig, allAnalyses);

            this.listOfAnalysisIds = allAnalyses.map((item) => item._id);

            this._checkIfAnalysesIsOnZoomLevel(viewer.toolsConfig.analysisConfig);

            allAnalyses.forEach((item) => {
                this._analyses[item._id] = item;
            });
        }

        if (viewer.toolsConfig && viewer.toolsConfig.layersConfig){
            let allLayers = [];
            this.getAllAnalyses(viewer.toolsConfig.layersConfig, allLayers);
            allLayers.forEach((item) => {
                this._layers[item._id] = item;
            });
        }

        //Estrutura os dados negociais
        for (let richLayer of viewer.project.richLayers) {
            this.richLayersMap[richLayer._id] = richLayer;

            MapsDataLoader.set(richLayer.layer._id, {
                outFields : richLayer.layer.layerFields.map(({alias, name}) => ({
                        name,
                        alias,
                        valueType : 'raw'
                    }
                ))
            });

            let businessDataMetada = MapsDataLoader.get(String(richLayer.dataSourceIdentifier));

            if (businessDataMetada && businessDataMetada.outFields) {
                let firstBusinessData = {};

                businessDataMetada.outFields
                    .forEach(outField => firstBusinessData[outField.name] = null);

                MapsDataLoader.set(richLayer.resultSetId, [firstBusinessData]);
            }
        }

        for (let key in this._richLayersMap) {
            this.layersMap[this.richLayersMap[key].layer._id] = this.richLayersMap[key].layer;
        }

        MapsDataLoader.set(viewer._id, viewer);
    }

    /**
     *
     * @returns {{name:String : IAnalysis}}
     */
    get analyses() {
        return this._analyses;
    }

    get layers() {
        return this._layers;
    }

    set legends(value){
        this._legends = value;
    }

    get legends(){
        return this._legends;
    }

    /**
     *
     * @returns {Object}
     */
    get richLayersMap() {
        return this._richLayersMap;
    }

    /**
     *
     * @param {Object} value
     */
    set richLayersMap(value) {
        this._richLayersMap = value;
    }

    /**
     *
     * @returns {Object}
     */
    get layersMap() {
        return this._layersMap;
    }

    /**
     *
     * @param {Object} value
     */
    set layersMap(value) {
        this._layersMap = value;
    }

    /**
     * @return {IAnalysis[]}
     */
    get analysisConfig() {
        return this._viewer.toolsConfig.analysisConfig;
    }

    get layersConfig(){
        return this._viewer.toolsConfig.layersConfig;
    }

    get trackingLayer() {
        return this._trackingLayer;
    }

    set trackingLayer(value) {
        this._trackingLayer = value;
    }

    _addLegend(id, legend, type){
        if (!type) {
            this.legends[id] = legend;

        } else {
            if (!this.legends[id]){
                this.legends[id] = {};
            }

            this.legends[id][type] = legend;
        }
    }

    addStaticLegend(id, legend) {
        this._addLegend(id, legend);
    }

    addAnalysisLegend(id, legend, type) {
        this._addLegend(id, legend, type);
    }

    getAllAnalyses(groups, result){
        return groups.filter((item) => {
            let isGroup = item.type === 'GROUP';
            if (isGroup) {
                this.getAllAnalyses(item.children, result);
            } else {
                result.push(item);
            }
            return !isGroup;

        });
    }

    _findGroup(groupId, groups){
        let result = null;

        groups.map((item) => {
            if(item.type === 'GROUP' && item._id === groupId){
                result = item;
            }else if(item.type === 'GROUP' && !result){
                result = this._findGroup(groupId, item.children);
            }
        });

        return result;
    }

    deleteGroup(groupId, isLayers){
        if(isLayers){
            this._viewer.toolsConfig.layersConfig = this._deleteGroup(groupId, this._viewer.toolsConfig.layersConfig, isLayers);
        }else{
            this._viewer.toolsConfig.analysisConfig = this._deleteGroup(groupId, this._viewer.toolsConfig.analysisConfig, isLayers);
        }
    }

    _deleteGroup(groupId, groups, isLayers){
        return groups.filter((item) => {
            if(item.type === 'GROUP'){
                let canDelete = item._id === groupId;

                if(canDelete){
                    this._deleteAllGroupItens(item.children, isLayers);
                }else{
                    item.children = this._deleteGroup(groupId, item.children, isLayers);
                }
                return !canDelete;
            }
            return true;
        });
    }

    _deleteAllGroupItens(groups, isLayers){
        groups.map((item) => {
            if(item.type === 'GROUP'){
                this._deleteAllGroupItens(item.children, isLayers);
            }else{
                if(isLayers){
                    delete this._layers[item._id];
                }else {
                    delete this._analyses[item._id];
                }
            }
        });
    }

    saveAnalysis(groupId, analysis, add){
        let group = this._findGroup(groupId, this._viewer.toolsConfig.analysisConfig);
        this._saveItem(analysis, group, add);

        this._analyses[analysis._id] = analysis;
    }

    saveLayer(groupId, layer, add){
        let group = this._findGroup(groupId, this._viewer.toolsConfig.layersConfig);
        this._saveItem(layer, group, add);

        this._layers[layer._id] = layer;
    }

    _saveItem(groupItem, group, add){
        if (add) {
            group.children.push(groupItem);
        } else {
            let index = -1;

            for(let i in group.children){
                let item = group.children[i];
                if(groupItem._id === item._id){
                    index = i;
                    break;
                }
            }

            group.children.splice(index, 1, groupItem);
        }
    }

    updateGroupLabel(groupId, label, isLayers){
        let group;
        group = this._findGroup(groupId, isLayers ? this._viewer.toolsConfig.layersConfig : this._viewer.toolsConfig.analysisConfig);
        group.title = label;
    };

    replaceGroupRoot(rootGroup, isLayers){
        if (isLayers) {
            this._viewer.toolsConfig.layersConfig = rootGroup;
        } else {
            this._viewer.toolsConfig.analysisConfig = rootGroup;
        }
    }

    replaceGroupChildren(groupId, children, isLayers){
        let group = this._findGroup(groupId, isLayers ? this._viewer.toolsConfig.layersConfig : this._viewer.toolsConfig.analysisConfig);
        group.children = children;
    }

    deleteAnalysis(analysisId){
        this._deleteItem(analysisId, this._viewer.toolsConfig.analysisConfig);
    }

    deleteLayer(layerId){
        this._deleteItem(layerId, this._viewer.toolsConfig.layersConfig, true);
    }

    _deleteItem(id, groups, isLayers){
        let removed = false;

        return groups.filter((item) => {
            if(item.type !== 'GROUP'){
                let remove = item._id === id;

                if(remove){
                    removed = true;

                    if(isLayers){
                        delete this._layers[id];
                        delete this._legends[id];
                    }else {
                        delete this._analyses[id];
                    }
                }

                return !remove;
            } else if(!removed) {
                item.children = this._deleteItem(id, item.children);
            }

            return true;
        });
    }

    /**
     * Método retorna meta dados da rich layer parametrizada
     * @param {String} richLayerId
     * @return {*}
     */
    getRichLayerMetaData(richLayerId) {
        return this._richLayersMap[richLayerId];
    }

    /**
     * Método retorna meta dados da layer parametrizada
     * @param {String} layerId
     * @return {*}
     */
    getLayerMetaData(layerId) {
        return this._layersMap[layerId];
    }

    /**
     * @type {String} layerId
     * @returns {IRichLayer}
     */
    getRichLayerByLayerId(layerId) {
        let richLayers = this.richLayersMap;
        let richLayer;

        for (let id in richLayers) {
            let cRichLayer = richLayers[id];

            if (cRichLayer.layer._id === layerId) {
                richLayer = cRichLayer;
                break;
            }
        }

        return richLayer;
    }

    /**
     *
     * @param {String} analysisId
     * @returns {ILayer}
     */
    getLayerByAnalysisId = (analysisId) => {
        return this.richLayersMap[this.analyses[analysisId].richLayerId].layer;
    };

    /**
     * Método retorna meta dados da Richlayer a partir do ID da análise parametrizada
     * @param {Number} analysisId
     * @return {*}
     */
    getRichLayerMetaDataByAnalysisIndex(analysisId) {
        let richLayer;
        let analysis = this.analyses[analysisId];

        if (analysis) {
            richLayer = this._richLayersMap[analysis.richLayerId];
        }

        return richLayer;
    }

    /**
     *
     * @param analysisId
     * @return {IRichLayer}
     */
    getRichLayerByAnalysisId(analysisId) {
        let analysis = this.analyses[analysisId];
        return  this.viewer.project.richLayers.filter(
            rich => rich._id === analysis.richLayerId
        ).pop();
    }

    /**
     * Método retorna meta dados da Layer a partir do ID da análise parametrizada
     * @param {Number} index
     */
    getLayerMetaDataByAnalysisId(id) {
        let richLayer = this.getRichLayerMetaDataByAnalysisIndex(id);
        if (richLayer) {
            return richLayer.layer;
        }
    }

    /**
     * Método que retorna os outFields de uma richLayer a partir do ID da richLayer
     * @param richLayerId
     * @returns {Array}
     */
    getOutfieldsByRichLayerId(richLayerId) {
        let richLayer = this.richLayersMap[richLayerId];
        let outFields = [];
        let resultSet = MapsDataLoader.get(richLayer.resultSetId);
        let featureSet = resultSet ? resultSet[0] : {};

        let metadata = MapsDataLoader
            .get(String(richLayer.dataSourceIdentifier));

        if (metadata) {
            return metadata.outFields;
        }

        for (let name in featureSet) {
            outFields.push({
                alias : name,
                name,
                valueType : typeof featureSet[name]
            });
        }

        return outFields;
    }

    toggleAnalysisShown(analysisId, shown) {
        this._analyses[analysisId].shown = shown;
    }

    toggleGroupShown(groupId, shown, isLayers) {
        let group = this._findGroup(groupId, isLayers ? this._viewer.toolsConfig.layersConfig : this._viewer.toolsConfig.analysisConfig);
        group.shown = shown;
    }

    toggleCheckGroup(groupId, checked, isLayers) {
        let self = this;
        let group, method;

        if(isLayers){
            group = this._findGroup(groupId, this._viewer.toolsConfig.layersConfig);
            method = checked ? "enableStaticLayer" : "disableStaticLayer";
        }else{
            group = this._findGroup(groupId, this._viewer.toolsConfig.analysisConfig);
            method = checked ? "enableAnalysis" : "disableAnalysis";
        }
        group.enable = checked;

        checkOrUncheckAllAnalysesGroup(group.children);

        function checkOrUncheckAllAnalysesGroup (group) {
            group.forEach((item) => {
                item.enable = checked;

                if (item.type === "GROUP") {
                    checkOrUncheckAllAnalysesGroup(item.children);

                } else {
                    setTimeout(() => {
                        self.$viewerAction[method](item._id);
                    }, 0);
                }
            });
        }
    }

    findGroupByAnalysisId (analysisId) {
        let groups = this.viewer.toolsConfig.analysisConfig;

        return (function find (groups) {
            let groupToReturn = {};

            for (let item of groups) {
                if (item.type === "GROUP") {
                    let childMapIds = item.children.map(child => child._id);

                    if (childMapIds.includes(analysisId)) {
                        groupToReturn = item;
                        break;
                    } else {
                        return find(item.children);
                    }
                }
            }

            return groupToReturn;
        })(groups);
    }

    findParentOfAGroupByGroupId (groupId) {
        let groups = this.viewer.toolsConfig.analysisConfig;

        return (function find (groups) {
            let groupToReturn = {};

            for (let item of groups) {
                if (item.type === "GROUP") {
                    let childGroupsMapIds = item.children.filter(child => child.type === "GROUP").map(item => item._id);

                    if (childGroupsMapIds.includes(groupId)) {
                        groupToReturn = item;
                        break;
                    } else {
                        return find(item.children);
                    }
                }
            }

            return groupToReturn;
        })(groups);
    }

    verifyIfPointIsOnTracking = (coordinate) => {
        let trackingPoints = this.trackingLayer.points || [];
        let containedPoints = trackingPoints.filter((point) => {return point.lat === coordinate.lat && point.lng === coordinate.lng});

        return containedPoints.length !== 0;
    };

    _checkIfAnalysesIsOnZoomLevel = (analysisConfig) => {
        let zoomLevel = this.viewer.project.mapConfig.zoom;

        check(analysisConfig);

        function check (group) {
            group.forEach((item) => {
                if (item.type === "ITEM") {
                    if (item.visibleScaleRange) {
                        if (zoomLevel < item.visibleScaleRange.min || zoomLevel > item.visibleScaleRange.max) {
                            item.outOfVisibleScale = true;
                        }
                    }
                } else {
                    check(item.children);
                }
            });
        }
    };

    checkIfAnalysisIsOnZoomLevel (analysis) {
        let zoomLevel = this.$parent.mapAPI.map.getZoom();
        analysis.outOfVisibleScale = analysis.visibleScaleRange ? zoomLevel < analysis.visibleScaleRange.min || zoomLevel > analysis.visibleScaleRange.max : false;
    }

    checkIndeterminateStatusGroup = (isLayer) => {
        //     let config = isLayer ? "layersConfig" : "analysisConfig";
        //     let groups = this.viewer.toolsConfig[config];
        //
        //     (function defineGroupStatus (items) {
        //         let checked = [];
        //
        //         for (let item of items) {
        //
        //             if (item.enable) {
        //                 checked.push(item);
        //             }
        //
        //             if (item.children) {
        //                 let ref = defineGroupStatus(item.children);
        //                 item.enable = ref.enable;
        //                 item.indeterminate = ref.indeterminate;
        //             }
        //         }
        //
        //         return {
        //             enable : (items.length && checked.length) && (items.length === checked.length),
        //             indeterminate : checked.length > 0 && checked.length < items.length
        //         };
        //     })(groups);
    }
}