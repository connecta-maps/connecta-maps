import StoreBase from "../_base/abstract/StoreBase";
import {APP_CONSTANT} from "../constant/index";
import AppController from "./controller/index";

export default class AppStore extends StoreBase {

    events = {
        [APP_CONSTANT.ALERT_ERROR] : {},
        [APP_CONSTANT.ALERT_SUCCESS] : {},
        [APP_CONSTANT.ALERT_WARN] : {},
        [APP_CONSTANT.ALERT_INFO] : {},
        [APP_CONSTANT.TOGGLE_TOOLS] : {},
        [APP_CONSTANT.DISABLE_POPUP] : {}
    };

    getControllerClass() {
        return AppController;
    }

}