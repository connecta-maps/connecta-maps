import StoreBase from "../_base/abstract/StoreBase";
import BasemapController from "./controller/basemap";
import {BASEMAP} from "../constant/basemap";

export default class BasemapStore extends StoreBase{

    events = {
        [BASEMAP.LOAD_DEFAULT_BASEMAPS] : {
            path : 'basemaps'
        },
        [BASEMAP.CHANGE_CURRENT_BASEMAP] : {}
    };

    getControllerClass() {
        return BasemapController;
    }

}