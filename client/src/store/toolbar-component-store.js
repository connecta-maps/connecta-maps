import StoreBase from "../_base/abstract/StoreBase";
import ToolbarComponentController from "./controller/toolbar-component-controller";
import TOOLBAR_COMPONENT_CONSTANT from "../constant/toolbar-component-constant";

export default class ToolbarComponentStore extends StoreBase {

    events = {
        [TOOLBAR_COMPONENT_CONSTANT.GET_SPATIAL_DATA_SOURCES] : {},
        [TOOLBAR_COMPONENT_CONSTANT.GET_LAYERS] : {},
        [TOOLBAR_COMPONENT_CONSTANT.SAVE_LAYER] : {},
        [TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE] : {},
        [TOOLBAR_COMPONENT_CONSTANT.REMOVE_SPATIAL_DATA_SOURCE] : {},
        [TOOLBAR_COMPONENT_CONSTANT.GET_RESULT_SETS_METADATA] : {}
    };

    getControllerClass() {
        return ToolbarComponentController;
    }

}