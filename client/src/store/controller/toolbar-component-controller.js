import ControllerBase from "../../_base/abstract/ControllerBase";
import TOOLBAR_COMPONENT_CONSTANT from "../../constant/toolbar-component-constant";
import {scope} from "../../scope";
import update from "react-addons-update";
import {MapsDataLoader} from "../../MapsDataLoader";

export default class ToolbarComponentController extends ControllerBase {

    prepare() {
        this.$appService = this.$parent.inject(scope.AppService);
        this.$appAction = this.$parent.inject(scope.AppAction);
        this.$parent.dispatcher.register(({type, data}) => {

            switch (type) {
                case TOOLBAR_COMPONENT_CONSTANT.GET_SPATIAL_DATA_SOURCES:
                    this.getSpatialDatasources();
                    break;
                case TOOLBAR_COMPONENT_CONSTANT.GET_LAYERS:
                    this.getLayers(data);
                    break;
                case TOOLBAR_COMPONENT_CONSTANT.GET_RESULT_SETS_METADATA:
                    this.getResultSetsMetadata();
                    break;
                case TOOLBAR_COMPONENT_CONSTANT.SAVE_LAYER:
                    this.saveLayer(data);
                    break;
                case TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE:
                    this.saveSpatialDataSource(data);
                    break;
                case TOOLBAR_COMPONENT_CONSTANT.REMOVE_SPATIAL_DATA_SOURCE:
                    this.removeSpatialDataSource(data);
                    break;
            }
        });
    }

    removeSpatialDataSource(spatialDataSourceId) {
        this.$appService.removeSpatialDataSource(spatialDataSourceId)
            .then(({data}) => {
                let viewer = this.$viewerStore.viewer;

                for (let index in viewer.project.richLayers) {
                    let richLayer = viewer.project.richLayers[index];
                    if (richLayer.layer.spatialDataSourceId === spatialDataSourceId) {
                        viewer = update(viewer, {
                            project : {
                                richLayers : {
                                    $splice : [[index, 1]]
                                }
                            }
                        });
                    }
                }

                return this.$appService.saveProject(viewer.project)
                    .then(() => {
                        this.$viewerStore.viewer = viewer;
                        return data;
                    });
            })
            .then(({data}) => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.REMOVE_SPATIAL_DATA_SOURCE, data);
                this.$appAction.alertSuccess('APP_SUCCESS_06');
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
    }

    saveLayer (layer) {
        this.$appService.saveLayer(layer)
            .then(({data}) => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.SAVE_LAYER, data);
                this.$appAction.alertSuccess('APP_SUCCESS_05');
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
    }

    saveSpatialDataSource (spatialDataSource) {
        this.$appService.saveSpatialDataSource(spatialDataSource)
            .then(({data}) => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE, [null, data]);
                this.$appAction.alertSuccess('APP_SUCCESS_05');
            })
            .catch(error => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE, [error]);
                this.$appAction.alertError('SERVER_ERROR_01', null, error.response);
            });
    }

    getResultSetsMetadata() {
        this.$parent.hostApplication.$getResultSetsMetadata()
            .then(({data}) => {
                for (let resultSetMetada of data) {
                    MapsDataLoader.set(resultSetMetada.id, resultSetMetada);
                }
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.GET_RESULT_SETS_METADATA, data);
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
    }

    /**
     *
     * @param {String} spatialDataSourceId
     */
    getLayers(spatialDataSourceId) {
        this.$appService.getLayersByDataSourceId(spatialDataSourceId)
            .then(({data}) => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.GET_LAYERS, data);
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
    }

    getSpatialDatasources() {
        this.$appService.getSpatialDatasources()
            .then(({data}) => {
                this.$emitter(TOOLBAR_COMPONENT_CONSTANT.GET_SPATIAL_DATA_SOURCES, data);
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
    }

}