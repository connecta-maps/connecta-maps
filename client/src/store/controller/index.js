import {APP_CONSTANT} from "../../constant/index";
import {scope} from "../../scope";
import ViewerService from "../../service/AppService";
import ControllerBase from "../../_base/abstract/ControllerBase";
import {locale} from "../../i18n/Locale";
import AppError from "../../native/AppError";
import AppErrorI18n from "../../native/AppErrorI18n";

export default class AppController extends ControllerBase {

    prepare() {
        this.appAction = this.$parent.inject(scope.AppAction);

        this.$parent.dispatcher.register((action) => {
            let dataEmitted = action.data;

            switch(action.type) {
                case APP_CONSTANT.ALERT_ERROR :
                    let type = APP_CONSTANT.ALERT_ERROR;
                    let msg = '';

                    if (dataEmitted.error instanceof AppErrorI18n) {
                        dataEmitted.codeMessage = dataEmitted.error.code;
                        dataEmitted.config = dataEmitted.error.params;
                        type = dataEmitted.error.type || type;

                        msg = locale.getMessage(dataEmitted.codeMessage, dataEmitted.config);

                    } else if (dataEmitted.error instanceof AppError) {
                        msg = dataEmitted.error.message;

                    } else if (dataEmitted.error && dataEmitted.error.data && dataEmitted.error.data.message) {
                        msg = dataEmitted.error.data.message;

                    } else if (dataEmitted.codeMessage) {
                        msg = locale.getMessage(dataEmitted.codeMessage, dataEmitted.config);

                    } else if (dataEmitted.error) {
                        msg = dataEmitted.error.message;
                    }

                    this.$emitter(type, msg);
                    break;

                case APP_CONSTANT.ALERT_SUCCESS :
                    this.$emitter(APP_CONSTANT.ALERT_SUCCESS, locale.getMessage(dataEmitted.codeMessage, dataEmitted.config));
                    break;

                case APP_CONSTANT.ALERT_WARN :
                    this.$emitter(APP_CONSTANT.ALERT_WARN, locale.getMessage(dataEmitted.codeMessage, dataEmitted.config));
                    break;

                case APP_CONSTANT.DISABLE_POPUP :
                    this.$emitter(APP_CONSTANT.DISABLE_POPUP, true);
                    break;

                case APP_CONSTANT.ALERT_INFO :
                    this.$emitter(APP_CONSTANT.ALERT_INFO, locale.getMessage(dataEmitted.codeMessage, dataEmitted.config));
                    break;

                case APP_CONSTANT.TOGGLE_TOOLS :
                    this.$parent.mapAPI.resizeMap();
                    this.$emitter(APP_CONSTANT.TOGGLE_TOOLS, dataEmitted);
                    break;
            }
        });
    }

    /**
     *
     * @param {String} viewerId
     */
    getViewer(viewerId) {
        let promise = ViewerService.getViewer(viewerId);
        promise.then(({data}) => {
            this.store.setViewer(data);
        });
        promise.catch(error => this.appAction.alertError('SERVER_ERROR_01', null, error));
    }

}