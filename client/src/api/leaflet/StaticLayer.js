import LayerBase from "./_base/LayerBase";
import Q from 'q';
import axios from "axios";
import WorkerHelper from "../../helper/WorkerHelper";
import L from "leaflet";
import {appConfig} from "../../app.config";

class StaticLayer extends LayerBase {

    _layer;

    _layerConfig;

    constructor (context, layerConfig) {
        super(context);
        this._layerConfig = layerConfig;
    }

    getLayer() {
        return this._layer;
    }

    build() {
        return Q.promise((resolve, reject) => {
            let layerId = String(this._layerConfig.layerId);
            this[this._layerConfig.operationName](layerId)
                .then((layer) => {
                    this._layer = layer;
                    resolve(appConfig.serviceDomain + '/geo/' + layerId + '/getLegendGraphic' + '?width=50&height=50');
                })
                .catch(reject);
        });
    }

    getMap(layerId) {
        return Q.promise((resolve, reject) => {
            let options = {
                transparent : true
            };
            resolve(L.tileLayer.wms(appConfig.serviceDomain + '/geo/' + layerId + '/getMap', options));
        });
    }

    query(layerId) {
        let options = {
            pane : 'tilePane'
        };
        return this.$geoService.query(layerId, {
            outSR : 'EPSG:4674'
        }).then((geoJSON) => L.geoJson(geoJSON, {
                style: () => options,
                pointToLayer : (feature, latLng) => L.circleMarker(latLng, options)
            }))
            .catch(error => {
                console.error(error);
            });
    }
}

export default StaticLayer;