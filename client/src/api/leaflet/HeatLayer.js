import LayerBase from "./_base/LayerBase";
import Q from 'q';
import WorkerHelper from "../../helper/WorkerHelper";
import L from "leaflet";
import chroma from "chroma-js";
import "../../../public/assets/js/leaflet-heat";

export default class HeatLayer extends LayerBase {

    _layer;

    getLayer() {
        return this._layer;
    }

    build() {
        return Q.Promise((resolve, reject) => {
            try {
                let layer = this.$viewerStore.getLayerMetaDataByAnalysisId(this.analysisIndex);

                let promise = this.$geoService.query(layer._id, {
                    outSR : 'EPSG:4674'
                }, this.analysis);

                this.config = this.analysis.configRenderer.heatmap;

                promise.catch(reject);
                promise.then((res) => {
                    this._worker = new WorkerHelper('geojson-to-heatmap.js');
                    let promise = this._worker.execute({config: this.config, geoJSON: res});
                    promise.then(({heatmapOptions, data, legendInfo}) => {
                        heatmapOptions.pane = this.analysis._id;

                        if (heatmapOptions._intensityValues) {
                            /*let gradient = {};
                            let values = Object.keys(heatmapOptions._intensityValues);
                            chroma
                                .scale([this.config.colors[0], this.config.colors[1]])
                                .colors(values.length)
                                .forEach((color, index) => {
                                    gradient[values[index]] = color;
                                });
                            heatmapOptions.gradient = gradient;*/
                        }

                        this._layer = L.heatLayer(data, heatmapOptions);
                        resolve(legendInfo);
                    });
                });

            } catch (error) {
                reject(error);
            }
        });
    }

}