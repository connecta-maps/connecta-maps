import Q from "q";

let workers = {};

export default class WorkerHelper {

    /**
     * @type {Worker}
     */
    _worker;

    /**
     *
     * @param {String} workerName
     */
    constructor(workerName) {
        if (!workers[workerName]) {
            let file = require('../worker/' + workerName);
            file = file[Object.keys(file)[0]];
            workers[workerName] = file;
        }

        this._worker = new Worker(URL.createObjectURL(
            new Blob(
                ['(' + workers[workerName].toString() + '())'],
                {type : 'text/javascript'}
            )
        ));
    }

    /**
     *
     * @param {*} data
     */
    postMessage(data) {
        this._worker.postMessage(data);
    }

    execute(data) {
        return new Promise((resolve, reject) => {
            try {
                this._worker.onerror = (error) => {
                    reject(error);
                    this._worker.terminate();
                };

                this._worker.onmessage = ({data}) => {
                    resolve(data);
                    this._worker.terminate();
                };

                this.postMessage(data);
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     *
     * @param {Promise} promise
     * @param {String} deepPath
     * @param {*} [config]
     */
    executeWithPromise(promise, deepPath, config) {
        let deferred = Q.defer();
        //TODO: possibilitar parametrizar o deepPath com o intuito de navegar em níveis mais aninhados do resultado final

        try {

            this._worker.onerror = (error) => {
                deferred.reject(error);
                this._worker.terminate();
            };

            this._worker.onmessage = ({data}) => {
                deferred.resolve(data);
                this._worker.terminate();
            };

            promise
                .then((data)=> {
                    try {
                        let dataToPost = {
                            data : deepPath ? data[deepPath] : data,
                            config : config
                        };
                        this.postMessage(dataToPost);
                    } catch (error) {
                        deferred.reject(error);
                    }
                })
                .catch(deferred.reject);

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

}