import Converter from "../util/Converter";
import {GEOMETRY_TYPE} from "../constant/geometry";

export class AnalysisValidator {

    static execute(analysis, geometryType){
        let newAnalysis = Converter.deepClone(analysis);
        delete newAnalysis.timeLineIsActive;

        for(let key in newAnalysis.configRenderer) {
            this[key](newAnalysis.configRenderer, geometryType);
        }

        return newAnalysis;
    }

    static chart(configRenderer){
        let chart = configRenderer.chart;
        if(!chart.metric || !chart.dimension) {
            delete configRenderer.chart;
        }
    }

    static thematic(configRenderer, geometryType){
        let thematic = configRenderer.thematic;

        if (thematic.fill) {
            if(!thematic.fill.classificationField || !thematic.fill.classificationMethod){
                delete configRenderer.thematic.fill;
            }
        }

        if (thematic.size) {
            if(geometryType !== GEOMETRY_TYPE.POINT || !thematic.size.classificationField || !thematic.size.classificationMethod) {
                delete configRenderer.thematic.size;
            }
        }

        if (thematic.uniqueValues && !thematic.uniqueValues.field) {
            delete configRenderer.thematic.uniqueValues;
        }

        if(!thematic.size && !thematic.fill && !thematic.uniqueValues){
            delete configRenderer.thematic;
        }

    }

    static heatmap(configRenderer, geometryType){

        if(geometryType !== GEOMETRY_TYPE.POINT) {
            delete configRenderer.heatmap;
        }
    }

    static cluster(configRenderer, geometryType){
        if(geometryType !== GEOMETRY_TYPE.POINT) {
            delete configRenderer.cluster;
        }
    }

    static simpleRenderer(simpleRenderer){
        //
    }

}