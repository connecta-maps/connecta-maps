import React from 'react';
import Dialog from 'material-ui/Dialog';
import SketchPicker from 'react-color';

class InputGradient extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            displayInitialColorPicker : false,
            displayFinalColorPicker : false
        };
    }

    handleClickInitialColorPicker = () => {
        this.setState({
            displayInitialColorPicker : !this.state.displayInitialColorPicker
        });
    };

    handleClickFinalColorPicker = () => {
        this.setState({
            displayFinalColorPicker : !this.state.displayFinalColorPicker
        });
    };

    handleInitialColorChange = (color) => {
        this.props.onInitialColorChange(color.hex);
    };

    handleFinalColorChange = (color) => {
        this.props.onFinalColorChange(color.hex);
    };

    render () {
        return (
            <section className="cmaps-input-gradient-container">
                <span className="cmaps-input-gradient-custom-label">{this.props.label}</span>
                <div className="cmaps-input-gradient-color-ramp-container">
                    <div className="cmaps-input-gradient-left-squared"
                         style={{backgroundColor : this.props.initialColor}}
                         onClick={this.handleClickInitialColorPicker}>
                    </div>

                    <div className="cmaps-input-gradient-bar"
                         style={{background: 'linear-gradient(to right, ' + this.props.initialColor + ', ' + this.props.finalColor + ')'}} />

                    <div className="cmaps-input-gradient-right-squared"
                         style={{backgroundColor : this.props.finalColor}}
                         onClick={this.handleClickFinalColorPicker}>
                    </div>
                </div>

                <Dialog open={this.state.displayInitialColorPicker}
                        modal={false}
                        onRequestClose={this.handleClickInitialColorPicker}
                        bodyStyle={{padding : 0}}
                        contentStyle={{width : 225}}>

                    <SketchPicker color={this.props.initialColor}
                                  onChangeComplete={this.handleInitialColorChange}/>
                </Dialog>

                <Dialog open={this.state.displayFinalColorPicker}
                        modal={false}
                        onRequestClose={this.handleClickFinalColorPicker}
                        bodyStyle={{padding : 0}}
                        contentStyle={{width : 225}}>

                    <SketchPicker color={this.props.finalColor}
                                  onChangeComplete={this.handleFinalColorChange}/>
                </Dialog>

            </section>
        );
    }
}

export default InputGradient;