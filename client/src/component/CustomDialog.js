import React from 'react';
import PropTypes from 'prop-types';
import ModuleBase from "../_base/abstract/ModuleBase";
import CloseButton from 'material-ui/svg-icons/navigation/close';

class CustomDialog extends ModuleBase {

    static propTypes = {
        autoWidth : PropTypes.bool,
        height : PropTypes.number,
        width : PropTypes.number,
        title : PropTypes.string,
        actions : PropTypes.arrayOf(PropTypes.element),
        children : PropTypes.element,
        open : PropTypes.bool,
        onCloseClick : PropTypes.func
    };

    static RefTypes = {
        dialogNode : PropTypes.element
    };

    constructor(props, context) {
        super(props, context);

        let size = this.getSize();

        this.setClassName('custom-dialog-component');

        this.state = {
            height : size.height,
            width : size.width,
        };
    }

    getSize() {
        let windowHeight = window.innerHeight;
        let windowWidth = window.innerWidth;
        let autoWidth = this.props.autoWidth !== undefined ? this.props.autoWidth : false;

        if (!this.props.width) {
            autoWidth = true;
        }

        return {
            height : this.props.height || (windowHeight - 70),
            width : autoWidth ? (windowWidth - 70) : this.props.width
        };
    }

    componentDidMount () {
        let body = document.body;
        body.style.margin = 0;
        body.insertBefore(this.refs.dialogNode, body.childNodes[body.childNodes.length - 1]);

        window.onresize = () => {
            let size = this.getSize();
            this.setState({
                height : size.height,
                width : size.width
            });
        };
    }

    handleCloseClick = () => {
        if (this.props.onCloseClick) {
            this.props.onCloseClick();
        }
    };

    render () {
        let contentStyle ={
            overflow : 'hidden',
            height : this.props.title ? '80%' : '90%'
        };

        return (
            <div ref="dialogNode">
                {this.props.open && (
                    <div className={this.className()}
                         style={{height : window.innerHeight}}>

                        <div className={this.className("dialog")}
                             style={{
                                 height : this.state.height,
                                 width : this.state.width
                             }}>

                            {this.props.title && (
                                <div className={this.className("dialog-header")}>
                                    <h4 className={this.className("dialog-header-title")}>
                                        {this.props.title}
                                    </h4>
                                    <div onClick={this.handleCloseClick}
                                         style={{cursor : 'pointer'}}>
                                        <CloseButton />
                                    </div>
                                </div>
                            )}

                            <div className={this.className("dialog-content")}
                                 style={contentStyle}>
                                {this.props.children}
                            </div>

                            <div className={this.className("dialog-footer")}>
                                {this.props.actions}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default CustomDialog;