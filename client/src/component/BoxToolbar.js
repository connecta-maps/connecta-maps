import React from "react";
import Details from "material-ui/svg-icons/image/details";
import Compare from "material-ui/svg-icons/image/compare";
import Opacity from "material-ui/svg-icons/action/opacity";
import MoreHoriz from "material-ui/svg-icons/navigation/more-horiz";
import Delete from "material-ui/svg-icons/action/delete-forever";
import CenterFocus from "material-ui/svg-icons/image/filter-center-focus";
import FileDownload from "material-ui/svg-icons/file/file-download";
import ModuleBase from "../_base/abstract/ModuleBase";
import {locale} from '../i18n/Locale';
import {IconButton} from "material-ui";

class BoxToolbar extends ModuleBase {

    static propTypes = {
        analysisId : PropTypes.number,
        analysisLayerIsSwiped : PropTypes.bool,
        showDetails : PropTypes.bool,
        onCLickShowDetails : PropTypes.func
    };

    _baseClass = "cmaps-box-toolbar-component";

    _tooltipPosition = "left-center";

    handleCenterLayerClick = () => {
        this.$mapAction.centerAtLayer(this.props.analysisId);
    };

    handleCompareClick = () => {
        let swiped = this.props.analysisLayerIsSwiped;
        this.props.onToggleSwipeAnalysis(!swiped, this.props.analysisId);
    };

    render () {
        return (
            <div className={this._baseClass}>
                <div className={this._baseClass + "-tools-content"}>
                    <div className={this._baseClass + "-tool" + (this.props.showDetails ? " enabled" : "")}
                         onClick={this.props.onCLickShowDetails}>
                        <IconButton tooltip={locale.getLabel("L1007")} tooltipPosition={this._tooltipPosition}>
                            <Details />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool"} >
                        <IconButton tooltip={locale.getLabel("L0020")} tooltipPosition={this._tooltipPosition}>
                            <Opacity />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool" + (this.props.analysisLayerIsSwiped ? " enabled" : "")}
                         onClick={this.handleCompareClick}>

                        <IconButton tooltip={locale.getLabel("L1003")} tooltipPosition={this._tooltipPosition}>
                            <Compare />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool"}
                         onClick={this.handleCenterLayerClick}>

                        <IconButton tooltip={locale.getLabel("L1004")} tooltipPosition={this._tooltipPosition}>
                            <CenterFocus />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool"} >
                        <IconButton tooltip={locale.getLabel("L0080")} tooltipPosition={this._tooltipPosition}>

                            <FileDownload />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool"} >
                        <IconButton tooltip={locale.getLabel("L1006")} tooltipPosition={this._tooltipPosition}>
                            <Delete />
                        </IconButton>
                    </div>
                    <div className={this._baseClass + "-tool"} >
                        <IconButton tooltip={locale.getLabel("L0081")} tooltipPosition={this._tooltipPosition}>
                            <MoreHoriz />
                        </IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default BoxToolbar;