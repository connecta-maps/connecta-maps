import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import {Dialog, FlatButton, IconButton, IconMenu, MenuItem} from "material-ui";
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import AddLocation from 'material-ui/svg-icons/maps/add-location';
import Copy from 'material-ui/svg-icons/content/content-copy';
import IconNavigationArrowBack from "material-ui/svg-icons/navigation/arrow-back";
import {locale} from "../i18n/Locale";
import MaintainSpatialDataSource from "./Toolbar/MantainSpatialDataSource/index";
import CopyLink from "./Toolbar/CopyLink/index";
import update from "react-addons-update";

export default class ToolbarComponent extends ModuleBase {

    _currentForm;

    _lastForm;

    _menus = [
        {
            label : locale.getLabel('L2003'),
            onClick : () => {
                this.$viewerAction.saveViewer(this.context.$parent.user);
            },
            Icon : (
                <div className="maps-icon maps-icon-floppy-disk"
                     style={{top : 3, left : 8}}
                />
            )
        },
        {
            label : locale.getLabel('L2001'),
            onClick : () => {
                this.setState({
                    modal : {
                        open : true,
                        currentForm : {
                            Class : MaintainSpatialDataSource,
                            title : locale.getLabel('L2002')
                        }
                    }
                });
            },
            Icon : <AddLocation />
        },
        {
            label : locale.getLabel('L2021'),
            onClick : () => {
                this.setState({
                    modal : {
                        open : true,
                        currentForm : {
                            Class : CopyLink,
                            title : locale.getLabel('L2021')
                        }
                    }
                });
            },
            Icon : <Copy />
        }
    ];

    constructor(props, context) {
        super(props, context);
        this.setClassName('toolbar-component');

        this.state = {
            modal : {
                open : false,
                openSubPage : false,
                currentForm : {},
                msgError : ''
            },
            extensions : this.context.$parent.extensions
        };
    }

    handleCloseModal = (actionStr) => {
        let msgError = '';
        let open = false;
        let currentForm = {};

        try {
            if (this._currentForm && this._currentForm[
                    actionStr === 'save' ? '$onSave' : '$onCancel'
                    ]) {
                this._currentForm[
                    actionStr === 'save' ? '$onSave' : '$onCancel'
                    ]();
            }
        } catch (error) {
            msgError = error.message;
            open = true;
            currentForm = this.state.modal.currentForm;
        }

        let modal = update(this.state.modal, {
            currentForm : { $set : currentForm },
            open : { $set : open },
            msgError : { $set : msgError }
        });

        this.setState({ modal });
    };

    openSubPage = (title, Class, props = {}) => {
        this._lastForm = this.state.modal.currentForm;
        let modal = update(this.state.modal, {
            openSubPage : {
                $set : true
            },
            currentForm : {
                $set : {
                    Class,
                    props,
                    title
                }
            }
        });
        this.setState({ modal });
    };

    back = () => {
        let modal = update(this.state.modal, {
            openSubPage : {
                $set : false
            },
            currentForm : {
                $set : this._lastForm
            }
        });
        this.setState({ modal });
    };

    render() {
        let ComponentForContentModal = this.state.modal.currentForm.Class;
        let actions = [
            <FlatButton
                label={locale.getLabel('L2010')}
                primary={true}
                onClick={this.handleCloseModal.bind(this, 'cancel')}
            />
        ];

        return (
            <section className={this.className()}>

                <div className={this.className("extensions")}>
                    <div className={this.className('icon-plus')} />
                    { this.state.extensions.map(({Icon, onClick, title}, index) => {

                        if (!(Icon instanceof React.Component)) {
                            if (typeof Icon === 'string') {
                                Icon = <div className={Icon} />;
                            }
                        }

                        return (
                            <div className={this.className("extension")}
                                 key={index}
                                 data-tip={ title || '' }
                                 onClick={() => onClick(this.context.$parent)}>
                                {Icon}
                            </div>
                        );
                    }) }
                </div>

                <div className={this.className('logo')} />
                <div className={this.className('title')}>
                    { this.$viewerStore.viewer.title }
                </div>
                <div className={this.className('menu')}>
                    <IconMenu
                        iconStyle={{
                            color : '#fff'
                        }}
                        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    >
                        { this._menus.map((menuConfig, index) => {
                            return (
                                <MenuItem
                                    key={index}
                                    leftIcon={menuConfig.Icon}
                                    onTouchTap={menuConfig.onClick}
                                    primaryText={menuConfig.label}/>
                            );
                        }) }
                    </IconMenu>
                </div>

                <Dialog
                    modal={true}
                    title={ this.state.modal.currentForm.title }
                    actions={actions}

                    titleStyle={{
                        paddingLeft : this.state.modal.openSubPage ? 55 : 20
                    }}
                    contentStyle={{
                        maxWidth : 'none',
                        width : this.state.modal.currentForm.Class === CopyLink ? '40%' : '90%',
                        padding : 0,
                        transform : 'translate(0, 40px)'
                    }}
                    bodyStyle={{ padding : 0 }}
                    autoScrollBodyContent={true}
                    open={this.state.modal.open || false}
                >
                    { this.state.modal.msgError &&
                    <div style={{ color : '#900' }}>{this.state.modal.msgError}</div>
                    }
                    <div
                        style={{
                            height: this.state.modal.currentForm.Class === CopyLink ? 'auto' : 370,
                            padding: '10px 20px',
                            overflow: 'auto',
                            overflowX: 'hidden'
                        }}>
                        {
                            ComponentForContentModal &&
                            <ComponentForContentModal
                                {...this.state.modal.currentForm.props || {}}
                                openSubPage={this.openSubPage}
                                back={this.back}
                                ref={(el) => this._currentForm = el}
                            />
                        }
                    </div>

                    {this.state.modal.openSubPage &&
                    <div style={{
                        position: 'absolute',
                        top: 15,
                        left: 7
                    }}>
                        <IconButton
                            onTouchTap={this.back}
                        >
                            <IconNavigationArrowBack />
                        </IconButton>
                    </div>
                    }
                </Dialog>
            </section>
        );
    }
}