import React from 'react';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconButton from 'material-ui/IconButton';
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {locale} from '../i18n/Locale';
import {Card, CardHeader, CardActions} from 'material-ui/Card';
import update from 'react-addons-update';
import OPERATORS, {OPERATORS_NAMES} from '../constant/operators';
import ChipInput from 'material-ui-chip-input';

class FilterFields extends React.Component {

    constructor (props) {
        super(props);
        this.state = {};
    }

    handleClickAddButton = () => {
        let newProps = update(this.props, {
            filters : {
                $push : [
                    {
                        valueType : this.state.selectedField.valueType,
                        field : this.state.selectedField.name,
                        value : ''
                    }
                ]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleClickRemoveButton = (index) => {
        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleChangeSelectedField = (event, index, value) => {
        this.setState({selectedField : value});
    };

    handleOperatorChanged = (index, event, key, value) => {
        let oldFilter = this.props.filters[index];
        let newFilter = { operator : value };

        if (value === OPERATORS_NAMES.IN)  newFilter.value = [];
        if (value === OPERATORS_NAMES.DISTINCT) oldFilter.value = "";

        newFilter = Object.assign({}, oldFilter, newFilter);

        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1, newFilter]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleValueChanged = (index, event, value) => {
        let oldFilter = this.props.filters[index];
        let newFilter = Object.assign(oldFilter, {value : value});
        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1, newFilter]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleRenderMenuItemsFilterFields = (filter) => {
        return OPERATORS.map((operator, index) => {
            if (operator.exclusion) {
                for (let exclude of operator.exclusion) {
                    if (exclude === filter.valueType)
                        return;
                }
            }

            return <MenuItem key={index} value={operator.name} primaryText={operator.alias}/>
        });
    };

    checkTagIncludeField = (optionsField, suggestion) => {
        if (optionsField.length > 0) {
            return optionsField.map((option) => {
                return option.toLowerCase().includes(suggestion.toLowerCase());
            }).includes(true);
        }
        return false;
    };

    filterSuggestions = (suggestions, value, index) => {
        return suggestions.filter((suggestion) => {
            return suggestion.toLowerCase().includes(value.toLowerCase()) && !this.checkTagIncludeField(this.props.filters[index].value, suggestion);
        });
    };

    handleOnRequestAddValue = (value, index, filter) => {
        let dataSourceSuggestions = this.props.resultSet.map((field)=> {return String(field[filter.field])});
        let tagForInclusion = this.filterSuggestions(dataSourceSuggestions, value, index)[0];

        if (tagForInclusion) {
            let oldFilter = this.props.filters[index];
            let newFilter = update(oldFilter, {
                value : {
                    $push : [tagForInclusion]
                }
            });
            let newProps = update(this.props, {
                filters : {
                    $splice : [[index, 1, newFilter]]
                }
            });

            this.props.onRequestChange(newProps.filters);
        }
    };

    handleOnRequestDeleteValue = (value, index) => {
        let oldFilter = this.props.filters[index];
        let newFilter = update(oldFilter, {
            value : {
                $splice : [[index, 1]]
            }
        });
        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1, newFilter]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleInitialValueChanged = (index, event, value) => {
        let oldFilter = this.props.filters[index];
        let newFilter = Object.assign(oldFilter, {initialValue : value});

        if (newFilter.hasOwnProperty("value")) delete oldFilter.value;

        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1, newFilter]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    handleFinalValueChanged = (index, event, value) => {
        let oldFilter = this.props.filters[index];
        let newFilter = Object.assign(oldFilter, {finalValue : value});

        if (newFilter.hasOwnProperty("value")) delete oldFilter.value;

        let newProps = update(this.props, {
            filters : {
                $splice : [[index, 1, newFilter]]
            }
        });

        this.props.onRequestChange(newProps.filters);
    };

    render () {
        return (
            <section className="cmaps-filter-fields-container">

                <div className={'cmaps-filter-fields-container-combo-field'}>
                    <SelectField floatingLabelText={locale.getLabel('L0025')}
                                 value={this.state.selectedField}
                                 style={{ width : 'calc(100% - 55px)' }}
                                 onChange={this.handleChangeSelectedField}>

                        {this.props.fields.map((item, index) => {
                            return <MenuItem key={index} value={item} primaryText={item.alias}/>
                        })}

                    </SelectField>

                    <FloatingActionButton disabled={!this.state.selectedField}
                                          mini={true}
                                          onClick={this.handleClickAddButton}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>

                <div className="cmaps-filter-fields-cards-container">
                    {this.props.filters.map((filter, index) => {
                        return (
                            <Card className="cmaps-filter-fields-card" key={index}>

                                <CardHeader style={{paddingBottom : 0}}
                                            textStyle={{paddingRight : 0}}
                                            titleStyle={{whiteSpace : 'nowrap', maxWidth : 165, overflow: 'hidden', textOverflow: 'ellipsis', fontWeight : 'bold'}}
                                            title={filter.field}>

                                    <IconButton onMouseUp={() => this.handleClickRemoveButton(index)}
                                                tooltip={locale.getLabel('L0032')}
                                                tooltipPosition="top-center"
                                                style={{position : 'absolute', right : 0, top : 0}}>
                                        <DeleteForever />
                                    </IconButton>
                                </CardHeader>

                                <CardActions style={{padding : '15px', paddingTop : 0}}>
                                    <SelectField autoWidth={true}
                                                 style={{width : '100%'}}
                                                 value={filter.operator}
                                                 onChange={(event, key, value) => this.handleOperatorChanged(index, event, key, value)}
                                                 floatingLabelText={locale.getLabel('L0034')}>

                                        {this.handleRenderMenuItemsFilterFields(filter)}
                                    </SelectField>

                                    {(filter.operator !== OPERATORS_NAMES.IN &&
                                    filter.operator !== OPERATORS_NAMES.BETWEEN &&
                                    filter.operator !== OPERATORS_NAMES.NOT_BETWEEN) && (
                                        <TextField style={{width : '100%', marginTop : '14px'}}
                                                   value={filter.value}
                                                   disabled={filter.operator === OPERATORS_NAMES.DISTINCT}
                                                   onChange={(event, value) => this.handleValueChanged (index, event, value)}
                                                   floatingLabelText={locale.getLabel('L0035')}/>
                                    )}

                                    {filter.operator === OPERATORS_NAMES.IN && (
                                        <ChipInput
                                            fullWidth={true}
                                            dataSource={this.props.resultSet.map((field)=> String(field[filter.field]))}
                                            style={{marginRight : 0}}
                                            value={filter.value}
                                            onRequestAdd={(value) => this.handleOnRequestAddValue(value, index, filter)}
                                            onRequestDelete={(value) => this.handleOnRequestDeleteValue(value, index)}
                                            floatingLabelText={locale.getLabel('L0035')} />
                                    )}

                                    {(filter.operator === OPERATORS_NAMES.BETWEEN ||
                                    filter.operator === OPERATORS_NAMES.NOT_BETWEEN) && (
                                        <div style={{width : '100%', display : 'flex', justifyContent: 'space-between'}}>
                                            <TextField style={{width : '45%'}}
                                                       value={filter.initialValue}
                                                       onChange={(event, value) => this.handleInitialValueChanged (index, event, value)}
                                                       floatingLabelText={locale.getLabel("L0065")}/>

                                            <TextField style={{width : '45%'}}
                                                       value={filter.finalValue}
                                                       onChange={(event, value) => this.handleFinalValueChanged (index, event, value)}
                                                       floatingLabelText={locale.getLabel("L0066")}/>
                                        </div>
                                    )}
                                </CardActions>
                            </Card>
                        );
                    })}
                </div>
            </section>
        );
    }
}

export default FilterFields;