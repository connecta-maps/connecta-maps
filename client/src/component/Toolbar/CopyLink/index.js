import React from "react";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import {RaisedButton, TextField} from "material-ui";
import {locale} from '../../../i18n/Locale';

export default class CopyLink extends ModuleBase {

    constructor(props, context) {
        super(props, context);
        this.setClassName('toolbar-copy-link-form');

        let link = context.$parent.link;

        this.state = {
            link
        };
    }

    onCopy = () => {
        this.linkref.select();
        document.execCommand('copy');

    };

    render() {
        return (
            <div className={this.className()}>
                <div className={'cmaps-row'}>
                <TextField
                    ref={el => this.linkref = el}
                    style={{ width : '100%' }}
                    className={'link-value'}
                    value={this.state.link}
                    floatingLabelText={locale.getLabel('L0094')}
                />
                </div>
                <div className={'cmaps-row'}>
                    <RaisedButton
                        primary={true}
                        label={locale.getLabel('L0095')}
                        onTouchTap={this.onCopy}
                    />
                </div>
            </div>
        );
    }
}