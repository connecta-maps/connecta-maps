import React from "react";
import PropTypes from 'prop-types';
import update from "react-addons-update";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import TOOLBAR_COMPONENT_CONSTANT from "../../../constant/toolbar-component-constant";
import {locale} from "../../../i18n/Locale";
import {CircularProgress, MenuItem, RaisedButton, SelectField, TextField, Toggle} from "material-ui";
import IconActionGroupWork from "material-ui/svg-icons/action/group-work";
import IconEditorShowChart from "material-ui/svg-icons/editor/show-chart";
import IconActionHighLightOff from "material-ui/svg-icons/action/highlight-off";
import "./_MaintainLayers.scss";
import IRichLayer from "../../../_base/interface/IRichLayer";
import {MapsDataLoader} from "../../../MapsDataLoader";

export default class MaintainLayers extends ModuleBase {

    static propTypes = {
        currentSpatialDataSource : PropTypes.any
    };

    _tempLayers = [];
    _tempLayersMapping = {};

    constructor(props, context) {
        super(props, context);
        this.setClassName('toolbar-maintain-layer');

        this.state = {
            layers : [],
            resultSetsMetadata : [],
            loading : true,
            isEditingLayer : false,
            current : {
                richLayer : null,
                layer : null
            }
        };
    }

    componentDidMount() {
        this.own(
            this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.GET_LAYERS, this.onGetLayers),
            this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.GET_RESULT_SETS_METADATA, this.onGetResultSetMetadata)
        );
        this.$toolbarComponentAction.getLayers(this.props.currentSpatialDataSource._id);
        this.$toolbarComponentAction.getResultSetsMetadata();
    }

    onGetResultSetMetadata = (resultSetsMetadata) => {
        this.setState({
            resultSetsMetadata
        });
    };

    onGetLayers = (layers) => {
        this._tempLayers = layers;
        this._tempLayersMapping = {};

        for (let layer of layers) {
            //mapeia o title e o layerIdentifier para que o consumo seja mais rápido
            this._tempLayersMapping[
            layer.title +
            layer.layerIdentifier
                ] = layer;
        }

        this.setState({
            layers,
            loading : false
        });
    };

    onCloseEditing = () => {
        this.setState({
            isEditingLayer : false,
            current : {}
        });
    };

    onChangeSearchLayer = (event, newValue) => {
        let layers = [];

        newValue = newValue.replace(/\s\s+/g, ' ').trim();
        newValue = newValue ? new RegExp(newValue, 'i') : '';

        if (newValue) {
            for (let fieldsConcatStr in this._tempLayersMapping) {
                if (newValue.test(fieldsConcatStr)) {
                    layers.push(this._tempLayersMapping[fieldsConcatStr]);
                }
            }
        } else {
            layers = this._tempLayers;
        }

        this.setState({
            layers
        });
    };

    onEditLayer = (richLayer, layer) => {
        this.setState({
            isEditingLayer : true,
            current : {
                richLayer, layer
            }
        });
    };

    render() {
        return (
            <div className={this.className()}>

                {this.state.loading &&
                <div style={{ padding: 25 }}>
                    <CircularProgress />
                </div>}

                {!this.state.loading &&
                <div className={this.className('container')}>
                    <div className={this.className('search-layers')}>
                        <TextField
                            onChange={this.onChangeSearchLayer}
                            hintText={locale.getLabel('L2012')}
                        />
                    </div>

                    <div style={{
                        display : 'flex',
                        flexDirection : 'row',
                        flexWrap : 'nowrap',
                        width : '100%',
                        height: '100%'
                    }}>
                        <LayersList
                            style={{ width : this.state.isEditingLayer ? '40%' : '100%' }}
                            loading={this.state.loading}
                            layers={this.state.layers}
                            current={this.state.current}
                            appClass={this.appClass}
                            isEditingLayer={this.state.isEditingLayer}
                            onEditLayer={this.onEditLayer}
                        />

                        {this.state.isEditingLayer && this.state.current.layer &&
                        <EditLayer
                            appClass={this.appClass}
                            current={this.state.current}
                            resultSetsMetadata={this.state.resultSetsMetadata}
                            onCloseEditing={this.onCloseEditing}
                        />
                        }
                    </div>
                </div>
                }
            </div>
        );
    }
}

class EditLayer extends ModuleBase {

    static propTypes = {
        appClass : PropTypes.string,
        onCloseEditing : PropTypes.func,
        current : PropTypes.object,
        resultSetsMetadata : PropTypes.array
    };

    constructor(props, context) {
        super(props, context);
        this.appClass = props.appClass;
        this.state = this.getInitialState(props);
    }

    getInitialState(props) {
        return {
            current : props.current,
            resultSetsMetadata : props.resultSetsMetadata
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState(
            this.getInitialState(nextProps)
        );
    }

    onChangeDataSourceIdentifier = (event, key, dataSourceIdentifier) => {
        //todo: criar uma richLayer quando editar uma layer que não tem richLayer
        let current = update(this.state.current, {
            richLayer : {
                dataSourceIdentifier : {
                    $set : dataSourceIdentifier
                }
            }
        });
        this.setState({ current });
    };

    onChangeResultSetKey = (event, key, resultSetKey) => {
        let current = update(this.state.current, {
            richLayer : {
                crossingKeys : {
                    resultSetKey : {
                        $set : resultSetKey
                    }
                }
            }
        });
        this.setState({ current });
    };

    onChangeLayerCache = (operationName, toggle) => {
        let current = update(this.state.current, {
            layer : {
                geoCache : {
                    [operationName] : {
                        $set : toggle
                    }
                }
            }
        });
        this.setState({ current });
    };

    onChangeGeoKey = (event, key, geoKey) => {
        let current = update(this.state.current, {
            richLayer : {
                crossingKeys : {
                    geoKey : {
                        $set : geoKey
                    }
                }
            }
        });
        this.setState({ current });
    };

    onChangeTitle = (event, title) => {
        let current = update(this.state.current, {
            richLayer : {
                title : {
                    $set : title
                }
            }
        });
        this.setState({ current });
    };

    get businessOutFields() {
        let currentResultSetsMetadata = this.state.resultSetsMetadata.length && this.state.current.richLayer ?
            this.state.resultSetsMetadata.filter(r => (
                String(r.id) === String(this.state.current.richLayer.dataSourceIdentifier)
            ))[0] : null;

        return currentResultSetsMetadata ? currentResultSetsMetadata.outFields : [];
    }

    onChangeLayer = (fieldName, value) => {
        let current = update(this.state.current, {
            layer : {
                [fieldName] : {
                    $set : value
                }
            }
        });
        this.setState({ current });
    };

    onSave = () => {
        try {
            let {current} = this.state;
            let {richLayer, layer} = current;

            if (this.state.current.layer.serviceType === 'geo') {
                richLayer.crossingKeys = { geoKey : '_id', resultSetKey : '_id' };
                richLayer.dataSourceIdentifier = layer._id;
            }

            if (!current) {
                throw new Error('Não possui uma camada na edição!');
            }

            if (!layer) {
                throw new Error('Não possui uma camada na edição!');
            }

            if (!richLayer) {
                throw new Error('Não existe configuração de cruzamento para esta camada!');
            }

            if (!richLayer.dataSourceIdentifier ||
                !richLayer.crossingKeys ||
                (richLayer.crossingKeys && !richLayer.crossingKeys.resultSetKey) ||
                (richLayer.crossingKeys && !richLayer.crossingKeys.geoKey)
            ) {
                throw new Error('Não existe configuração de cruzamento para esta camada!');
            }

            if (!richLayer.title) {
                richLayer = update(richLayer, {
                    title : { $set : layer.title }
                });
            }

            let handler = this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.SAVE_LAYER, (layer) => {
                handler.remove();

                richLayer.layer = layer;
                let viewer = this.$viewerStore.viewer;
                let richLayersMap = this.$viewerStore.richLayersMap;

                if (!(richLayer._id in richLayersMap)) {
                    viewer = update(viewer, {
                        project : {
                            richLayers : {
                                $push : [richLayer]
                            }
                        }
                    });
                } else {
                    for (let index in viewer.project.richLayers) {
                        let currentRichLayer = viewer.project.richLayers[index];
                        if (currentRichLayer._id === richLayer._id) {
                            viewer = update(viewer, {
                                project : {
                                    richLayers : {
                                        [index] : {
                                            $set : richLayer
                                        }
                                    }
                                }
                            });
                            break;
                        }
                    }
                }

                this.$viewerStore.richLayersMap = {};
                this.$viewerStore.viewer = viewer;
                this.$viewerAction.saveProject(viewer.project);
            });
            this.$toolbarComponentAction.saveLayer(layer);
        } catch (error) {
            this.$appAction.alertError(null, null, error);
        }
    };

    render() {
        return (
            <div className={this.className('form-edit-layer')}>
                <div className={this.className("header")}>
                    <div className={this.className('close')}
                         onClick={this.props.onCloseEditing}>
                        <IconActionHighLightOff style={{ width: 30, height : 30 }} />
                    </div>
                    <div className={this.className('title')}>
                        <TextField
                            floatingLabelText={locale.getLabel('L2004')}
                            onChange={this.onChangeTitle}
                            value={ this.state.current.richLayer.title }
                        />
                    </div>
                    <div style={{ color: '#777', margin: '15px 0 0 25px' }}>
                        <div style={{ fontSize : '0.7em' }}> {locale.getLabel('L2015')} </div>
                        <div style={{ fontSize : '0.9em' }}>
                            { this.state.current.layer.layerIdentifier }
                        </div>
                    </div>
                </div>
                <div className={this.className("body")}>
                    <div className="cmaps-row">
                        <div className="cmaps-col-3"
                             style={{
                                 width: 155,
                                 marginRight: 45
                             }}>
                            <Toggle
                                onToggle={(event, isInputChecked) => {
                                    this.onChangeLayerCache('queryCache', isInputChecked);
                                }}
                                toggled={this.state.current.layer.geoCache.queryCache}
                                label="Cache: Query"
                            />
                        </div>
                        <div className="cmaps-col-3"
                             style={{
                                 width: 185,
                                 marginRight: 45
                             }}>
                            <Toggle
                                onToggle={(event, isInputChecked) => {
                                    this.onChangeLayerCache('getBreaksCache', isInputChecked);
                                }}
                                toggled={this.state.current.layer.geoCache.getBreaksCache}
                                label="Cache: GetBreaks"
                            />
                        </div>
                        <div className="cmaps-col-3">
                            <RaisedButton
                                label={locale.getLabel('L2007')}
                            />
                        </div>
                    </div>

                    <br/>

                    <div className="cmaps-row">
                        <div className="cmaps-col-5">
                            <Toggle
                                onToggle={(event, isInputChecked) => {
                                    let serviceType = isInputChecked ? 'geo' : '';
                                    this.onChangeLayer('serviceType', serviceType);
                                }}
                                toggled={Boolean(this.state.current.layer.serviceType === 'geo')}
                                label={locale.getLabel('L2018')}
                            />
                        </div>
                    </div>

                    <br/>

                    { Boolean(this.state.current.layer.serviceType !== 'geo') &&
                    <div>
                        <div className="cmaps-row">
                            <b>{locale.getLabel('L2014')}</b>
                        </div>
                        <div className="cmaps-row">
                            <div className="cmaps-col-3">
                                <SelectField
                                    autoWidth={true}
                                    style={{
                                        maxWidth : 180
                                    }}
                                    value={String(this.state.current.richLayer.crossingKeys.geoKey)}
                                    floatingLabelText={"Campo Geográfico"}
                                    onChange={this.onChangeGeoKey}>
                                    {this.state.current.layer.layerFields.map((layerField, index) => {
                                        return (
                                            <MenuItem value={String(layerField.name)}
                                                      key={index}
                                                      primaryText={layerField.alias} />
                                        );
                                    })}
                                </SelectField>
                            </div>
                            <div className="cmaps-col-3">
                                <SelectField
                                    autoWidth={true}
                                    style={{
                                        maxWidth : 180
                                    }}
                                    value={String(this.state.current.richLayer.dataSourceIdentifier)}
                                    floatingLabelText={"Análise"}
                                    onChange={this.onChangeDataSourceIdentifier}>
                                    {this.state.resultSetsMetadata.map((resultSetMetadata, index) => {
                                        return (
                                            <MenuItem value={String(resultSetMetadata.id)}
                                                      key={index}
                                                      primaryText={resultSetMetadata.name} />
                                        );
                                    })}
                                </SelectField>
                            </div>
                            <div className="cmaps-col-3">
                                <SelectField
                                    autoWidth={true}
                                    style={{
                                        maxWidth : 180
                                    }}
                                    onChange={this.onChangeResultSetKey}
                                    value={String(this.state.current.richLayer.crossingKeys.resultSetKey)}
                                    floatingLabelText={"Campo negocial"}>
                                    {this.businessOutFields.map((outField, index) => {
                                        return (
                                            <MenuItem
                                                key={index}
                                                value={outField.name}
                                                primaryText={outField.alias}
                                            />
                                        );
                                    })}
                                </SelectField>
                            </div>
                        </div>
                    </div>
                    }
                    <div className="cmaps-row">
                        <RaisedButton
                            primary={true}
                            onTouchTap={this.onSave}
                            label={locale.getLabel('L0036')}
                        />
                    </div>
                </div>
            </div>
        );
    }

}

class LayersList extends ModuleBase {

    static propTypes = {
        loading : PropTypes.bool,
        layers : PropTypes.array,
        appClass : PropTypes.string,
        current : PropTypes.object,
        isEditingLayer : PropTypes.bool,
        onEditLayer : PropTypes.func
    };

    constructor(props, context) {
        super(props, context);
        this.appClass = props.appClass;
        this.state = this.getInitialState(props);
    }

    getInitialState(props) {
        return {
            loading : props.loading,
            layers : props.layers,
            current : props.current,
            isEditingLayer : props.isEditingLayer,
            style : props.style
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState(
            this.getInitialState(nextProps)
        );
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.isEditingLayer && this._layersNode) {
            this._layersNode.scrollTop = 0;
        }
        return true;
    }

    render() {
        return (
            <div className={this.className("layers")}
                 ref={el => this._layersNode = el}
                 style={this.state.style}>
                {this.state.layers.map((layer, index) => {
                    let geometryTypeClass = (
                        (/point/i.test(layer.geometryType)) ? 'point' :
                            (/polygon/i.test(layer.geometryType)) ? 'polygon' :
                                (/line/i.test(layer.geometryType)) ? 'line' : ''
                    );

                    let PolygonIcon = () => {
                        return (
                            <div style={{
                                margin: '0 12px',
                                width: '100%',
                                height: '55%'
                            }} />
                        );
                    };

                    let mappingIcon = {
                        'point' : IconActionGroupWork,
                        'polygon' : PolygonIcon,//IconImageCropLandscape
                        'line' : IconEditorShowChart
                    };
                    let IconGeometry = mappingIcon[geometryTypeClass];
                    let richLayer = this.$viewerStore.getRichLayerByLayerId(layer._id);
                    let isCurrent = Boolean(
                        this.state.current.layer &&
                        this.state.current.layer._id === layer._id
                    );

                    let crossingLabel = '';

                    if (richLayer) {
                        crossingLabel = richLayer.layer.serviceType === 'geo' ? 'Usada sem cruzamento' : richLayer.layer.serviceType;
                    }

                    return (
                        <div key={index}
                             className={
                                 this.className('card-layer') + ' ' +
                                 (richLayer ? this.className('rich') : '') + ' ' +
                                 (isCurrent ? this.className('current') : '')
                             }>

                            <div
                                style={{
                                    display : isCurrent ? 'none' : 'flex'
                                }}
                                className={this.className('card-hover-buttons')}>

                                <RaisedButton
                                    secondary={true}
                                    label={locale.getLabel('L1005')}
                                    onTouchTap={() => {
                                        richLayer = richLayer || IRichLayer.getDefault(layer);
                                        this.props.onEditLayer(richLayer, layer);
                                    }}
                                />
                            </div>

                            <div className={this.className('card-geometry-type') + ' ' + geometryTypeClass}>
                                {IconGeometry &&
                                <IconGeometry/>
                                }
                            </div>
                            <div className={this.className('card-info')}>
                                <div style={{color: '#444', maxWidth : 140}}>
                                    <div className={this.className('card-header')}>
                                        { richLayer ? richLayer.title : layer.title }
                                    </div>
                                    <div className={this.className('card-subtitle')}>{layer.layerIdentifier}</div>
                                    <div className={this.className('card-content')}>
                                        {richLayer &&
                                        <div>{richLayer.crossingKeys.geoKey}</div>
                                        }
                                    </div>
                                </div>

                                {richLayer &&
                                <div style={{color: '#069'}}
                                     className="maps-icon maps-icon-contrast"/>
                                }

                                {richLayer && !this.state.isEditingLayer &&
                                <div style={{color: '#069'}}>
                                    {richLayer.layer.serviceType !== 'geo' &&
                                    <div className={this.className('card-header')}>{locale.getLabel('L2014')}</div>
                                    }
                                    <div className={this.className('card-content')}>
                                        <div> {crossingLabel} </div>
                                    </div>
                                </div>
                                }
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }

}