import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'material-ui/Slider';

class InputRange extends React.Component {

    static propTypes = {
        onDragStart : PropTypes.func,
        onDragStop : PropTypes.func,
        label : PropTypes.string,
        value : PropTypes.number,

        defaultValue:PropTypes.any,
        disableFocusRipple:PropTypes.any,
        disabled:PropTypes.any,
        max:PropTypes.any,
        min:PropTypes.any,
        name:PropTypes.any,
        onBlur:PropTypes.any,
        onChange:PropTypes.any,
        onFocus:PropTypes.any,
        required:PropTypes.any,
        sliderStyle:PropTypes.any,
        step:PropTypes.any,
        style:PropTypes.any
    };

    constructor (props) {
        super(props);
        this.state = {
            value : props.value
        };
    }

    handlerOnDragStart = (event) => {
        if (this.props.onDragStart) {
            this.props.onDragStart(event);
        }
    };

    handlerOnDragStop = () => {
        if (this.props.onDragStop) {
            this.props.onDragStop(this.state.value);
        }
    };

    render () {
        let propsOverride = Object.assign({}, this.props, {
            style : { width : '100%', marginTop: 0 },
            sliderStyle : { marginBottom : 10, marginTop: 0 }
        });
        let style = Object.assign({
            marginTop : 22
        }, this.props.style || {});
        return (
            <div className="cmaps-input-range" style={style}>
                <div style={{
                    display : 'flex',
                    flexDirection : 'row',
                    justifyContent : 'flex-start'
                }}>
                    <span className="cmaps-input-range-custom-label">{this.props.label}</span>
                    <span style={{ marginLeft : '15px' }}>{this.state.value}</span>
                </div>
                <Slider {...propsOverride}
                        onChange={(event, value) => this.setState({value})}
                        onDragStart={this.handlerOnDragStart}
                        onDragStop={this.handlerOnDragStop}/>
            </div>
        );
    }

}

export default InputRange;