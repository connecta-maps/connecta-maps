import ServiceBase from "../_base/abstract/ServiceBase";
import {appConfig} from "../app.config";
import {MapsDataLoader} from "../MapsDataLoader";
import GeoCacheHelper from "../helper/GeoCacheHelper";
import Q from "q";
import {OPERATORS_NAMES} from "../constant/operators";
import {scope} from "../scope";
import queryString from "query-string";
import AppError from "../native/AppError";
import {APP_CONSTANT} from "../constant/index";
import AppErrorI18n from "../native/AppErrorI18n";

export default class GeoService extends ServiceBase {

    get helper() {
        return this.$parent.inject(scope.RequestHelper);
    }

    /**
     * @param {String} layerId
     * @param {{
     * breakCount : Number,
     * classificationMethod : String,
     * classificationField : String
     * }} getBreaksParams
     * @param {IFilterConfig[]} filterConfig
     * @return {Promise}
     */
    getBreaks(layerId, getBreaksParams, filterConfig) {
        return new Promise((resolve, reject) => {
            let url = appConfig.serviceDomain + '/geo/' + layerId + '/get-breaks';
            getBreaksParams.project = MapsDataLoader.get(this.$parent.viewerId).project;
            getBreaksParams.filterConfig = filterConfig;

            this.helper.post(url, getBreaksParams)
                .then(result => resolve(result.data) )
                .catch(error => {
                    try {
                        if (error &&
                            error.response &&
                            error.response.data &&
                            error.response.data.message) {
                            throw new Error(error.response.data.message);
                        }

                        reject(error);
                    } catch (error) {
                        reject(error);
                    }
                });
        });
    }

    getQueryUrl(layerId, queryParams = {}, analysis = {}, useInQueryString = false) {
        let timeLineFilter = [];

        if (analysis.timelineColumn && this.$viewerStore.timelineValue) {
            timeLineFilter.push({
                field: analysis.timelineColumn,
                operator: OPERATORS_NAMES.EQUAL,
                value: this.$viewerStore.timelineValue
            });
        }

        queryParams = Object.assign({}, queryParams, {
            project : this.$viewerStore.viewer.project,
            filterConfig : [].concat(analysis.filterConfig, timeLineFilter)
        });

        if (useInQueryString) {
            for (let key in queryParams) {
                if (typeof queryParams[key] === 'object') {
                    queryParams[key] = JSON.stringify(queryParams[key]);
                }
            }
        }

        return {
            url : appConfig.serviceDomain + '/geo/' + layerId + '/query',
            queryParams,
            urlStr : appConfig.serviceDomain + '/geo/' + layerId + '/query?' + queryString.stringify(queryParams),
        };
    }

    /**
     *
     * @param {String} layerId
     * @param {Object} [queryParamsArgument]
     * @param {IAnalysis} [analysis]
     * @returns {Promise}  Resolve GeoJSON, onde cada feature possui os atributos:
     * $spatialData : contendo os dados originais da camada geográfica
     * properties : dados negociais cruzados
     * geometry : geometria da feature
     */
    query(layerId, queryParamsArgument, analysis = {}) {
        return new Promise((resolve, reject) => {
            let {url, queryParams} = this.getQueryUrl(layerId, queryParamsArgument, analysis);
            let geoCacheHelper = new GeoCacheHelper(analysis._id, url, arguments);
            let cache = geoCacheHelper.getDataFromCache();

            if (cache && !this.$viewerStore.timelineValue) {
                resolve(cache);

            } else {
                return this.helper.post(url, queryParams)
                    .then(({data}) => {
                        try {
                            if (!data.features.length) {
                                throw new AppErrorI18n('APP_WARN_04', null, APP_CONSTANT.ALERT_WARN);
                            }
                            geoCacheHelper.setData(data);
                            resolve(data);
                        } catch (error) {
                            reject(error);
                        }

                    })
                    .catch(error => {
                        console.error(error);
                        try {
                            if (error &&
                                error.response &&
                                error.response.data &&
                                error.response.data.message) {
                                throw new AppError(error.response.data.message);
                            }

                            reject(error);

                        } catch (error) {
                            reject(error);
                        }
                    });
            }
        });
    }

    getMap (layerId) {
        let deferred = Q.defer();

        try {
            let url = appConfig.serviceDomain + '/geo/' + layerId + '/getMap';
            this.helper.get(url)
                .then((result) => {
                    deferred.resolve(result);
                })
                .catch(error => {
                    deferred.reject(error);
                });
        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

    generateRenderer (layerId, config, filterConfig = []) {
        let url = appConfig.serviceDomain + '/geo/' + layerId + '/generate-renderer';
        let project = this.$viewerStore.viewer.project;

        return this.helper.post(url, {
            field : config.field,
            project: project,
            colorRamp: config.colorRamp,
            filterConfig
        }).then(({data}) => data);
    }

}