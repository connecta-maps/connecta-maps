import {appConfig} from "../app.config";
import ServiceBase from "../_base/abstract/ServiceBase";
import {scope} from "../scope";

export default class AppService extends ServiceBase {

    get helper() {
        return this.$parent.inject(scope.RequestHelper);
    }

    getViewer(viewerId, user) {
        let params = {};

        if (user) {
            params.userId = user.id;
            params.application = user.application;
        }

        return this.helper.get(appConfig.serviceDomain + '/app/viewer/' + viewerId, params);
    }

    getDefaultBaseMaps() {
        return this.helper.get(appConfig.serviceDomain + '/app/basemaps');
    }

    saveViewer(viewer, user) {
        return this.helper.post(appConfig.serviceDomain + '/app/viewer', {viewer, user});
    }

    saveProject(project) {
        return this.helper.post(appConfig.serviceDomain + '/app/project', {project});
    }

    saveLayer(layer) {
        return this.helper.post(appConfig.serviceDomain + '/app/layer', {layer});
    }

    saveSpatialDataSource(spatialDataSource) {
        return this.helper.post(appConfig.serviceDomain + '/app/spatial-data-source', {spatialDataSource});
    }

    removeSpatialDataSource(spatialDataSourceId) {
        return this.helper.del(appConfig.serviceDomain + '/app/spatial-data-source/' + spatialDataSourceId);
    }

    getSpatialDatasources () {
        return this.helper.get(appConfig.serviceDomain + '/app/spatial-data-source');
    }

    getLayersByDataSourceId (id) {
        return this.helper.get(appConfig.serviceDomain + '/app/spatial-data-source/' + id + '/layers');
    }

}