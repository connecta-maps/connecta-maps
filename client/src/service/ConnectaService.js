import axios from "axios";
import {appConfig} from "../app.config";
import ServiceBase from "../_base/abstract/ServiceBase";
import {MapsDataLoader} from "../MapsDataLoader";
import {scope} from "../scope";


export default class ConnectaService extends ServiceBase {

    connectaUrl;
    dashboardPublicKey;
    dashboardId;
    domainId;
    queue = [];

    setup(viewerId, options) {
        Object.assign(this, options);
        let appService = this.$parent.inject(scope.AppService);
        return appService.getViewer(viewerId)
            .then((response) => {
                let viewer = response.data;
                return this.setupViewer(viewer);
            });
    }

    setupViewer(viewer) {
        return this.getProject(viewer.project._id)
            .then((response) => {
                let project = response.data;
                let promises = project.richLayers.map((richLayer, richLayerIndex) => {
                    if (richLayer.layer.serviceType !== 'geo') {
                        return this.getAnalysis(richLayer.dataSourceIdentifier)
                            .then((response) => {
                                this.queue.push({
                                    richLayer,
                                    richLayerIndex,
                                    data: response.data
                                });
                            });
                    }
                });
                return Promise.all(promises);
            })
            .then(() => {
                return this.executeQueue().then((responses) => {
                    responses.map(({response, richLayerIndex}) => {
                        let id = viewer.project.richLayers[richLayerIndex].resultSetId;
                        MapsDataLoader.set(id, response.data);
                    });
                    return viewer;
                });
            });
    }

    getProject(id) {
        return axios.get(appConfig.serviceDomain + '/app/project/' + id);
    }

    getAnalysis(idAnalysis) {
        return axios.get(this.connectaUrl + '/analysis/' + idAnalysis,
            {
                headers: {
                    'portal.dashboard.publickey': this.dashboardPublicKey,
                    'portal.dashboard.id': this.dashboardId,
                    'portal.dashboard.validated': true,
                    'Domain': this.domainId
                }
            });
    }

    execute(richLayer, analysis, richLayerIndex) {
        let analysisExecuteRequest = this.paramsFactory(richLayer, analysis);
        return axios.post(
            this.connectaUrl + '/analysis/result',
            this.prepareAnalysisRequest(analysisExecuteRequest),
            {
                headers : {
                    'Domain': this.domainId
                }
            }
        )
            .then((response) => {
                return {response, richLayerIndex};
            });
    }

    prepareAnalysisRequest(analysisExecuteRequest) {
        let analysisExecuteRequestCopy = Object.assign({}, analysisExecuteRequest);

        if (!analysisExecuteRequestCopy.filters) {
            analysisExecuteRequestCopy.filters = [];
        }

        analysisExecuteRequestCopy.filters = analysisExecuteRequestCopy.filters.filter((filter) => {
            return filter.type && filter.analysisColumn && filter.value;
        }).map(function (filter) {
            return {
                operator: filter.type,
                columnName: filter.analysisColumn.name,
                value: filter.value
            };
        });

        return analysisExecuteRequestCopy;
    }

    paramsFactory(richLayer, analysis) {
        return {
            analysis: analysis,
            filters: [],
            drill: {
                columnToDrill: richLayer.crossingKeys.resultSetKey,
                columnsToSum: [],
                listPreviousColumns: []
            },
            pagination: {
                "page": 1,
                "count": 1
            }
        };
    }

    executeQueue(responses = []) {
        let promise = Promise.resolve();

        if (this.queue.length === 0) {
            return Promise.resolve(responses);
        }

        return promise.then(() => {
            let {richLayer, richLayerIndex, data} = this.queue.pop();
            return this.execute(richLayer, data, richLayerIndex).then((response) => {
                responses.push(response);
                return this.executeQueue(responses);
            });
        });
    }

}