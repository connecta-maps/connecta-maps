import React from "react";
import MapsLayers from 'material-ui/svg-icons/maps/layers';
import MapsTerrain from 'material-ui/svg-icons/maps/terrain';
import LanguageActionIcon from 'material-ui/svg-icons/action/language';
import {locale} from './i18n/Locale';
import ToolAnalyses from "./module/ToolAnalyses";
import ToolBasemap from "./module/ToolBasemap";
import ToolLayers from "./module/ToolLayers";
import ToolConfig from "./module/ToolConfig";
import AddLocation from 'material-ui/svg-icons/maps/add-location';
import theme from "./theme.config";
import {scope} from "./scope";
import SortableTree from "./module/SortableTree";

const color = theme.palette.primary1Color;

const iconStyle = {
    margin : '12px 10px'
};

export const toolsConfig = {
    currentTool : 'analyses',
    tools : [
        {
            name : 'analyses',
            label : locale.getLabel('L1001'),
            icon : <span className="maps-icon maps-icon-map3" style={{ fontSize : '19px', color }}/>,
            Class : ToolAnalyses,
            ButtonLabel : (({$parent}) =>
                <div
                    onClick={() => {
                        SortableTree.addGroupAtRoot($parent);
                    }}
                    className="cmaps-button-primary">
                    { locale.getLabel('L1014') }
                </div>
            )
        },
        {
            name : 'layers',
            label : locale.getLabel('L1011'),
            icon : <span className={'maps-icon maps-icon-stack'} style={{ fontSize : '19px', color }} />,
            Class : ToolLayers,
            ButtonLabel : (({$parent}) =>
                    <div
                        onClick={() => {
                            SortableTree.addGroupAtRoot($parent, true);
                        }}
                        className="cmaps-button-primary">
                        { locale.getLabel('L1014') }
                    </div>
            )
        },
        {
            name : 'basemap',
            label : locale.getLabel('L1002'),
            icon : <LanguageActionIcon color={color} style={iconStyle} />,
            Class : ToolBasemap
        },
        {
            name : 'config',
            label : locale.getLabel('L1031'),
            icon : <span className={'maps-icon maps-icon-cog'} style={{ fontSize : '19px', color }} />,
            Class : ToolConfig
        }
    ]
};