
export default class AppErrorI18n {

    constructor(code, params, type) {
        this.code = code;
        this.params = params;
        this.type = type;
    }

}