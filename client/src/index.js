/*global event, self*/
/*eslint no-restricted-globals: ["error", "event"]*/

import React from "react";
import ReactDOM from "react-dom";
import AppModule from "./module/index";
import {Dispatcher} from "flux";
import {scope} from "./scope";
import {appConfig} from "./app.config";
import {MapsDataLoader} from "./MapsDataLoader";
import injectTapEventPlugin from "react-tap-event-plugin";
import OBIEE from "./host-application/obiee/index";
import ConnectaService from "./service/ConnectaService";
import Connecta from "./host-application/connecta/index";
import "./style/app.scss";
import {EventEmitter} from "events";
import "./native/String";
import themeConfig from "./theme.config";

injectTapEventPlugin();

window['mapsInstances'] = [];
let MapAPI;
let HostApplication;
let pattern = /\#\/maps\-viewer\/([\d]+)\/domain\/([\d]+)/;

const buildMAP = () => {

    switch (appConfig.mapAPI) {

        case 'leaflet':
            MapAPI = require("./api/leaflet/index").MapAPI;
            break;

    }

};

const buildHostApplication = () => {

    switch (appConfig.hostApplication) {

        case 'obiee':
            HostApplication = OBIEE;
            break;

        case 'connecta' :
            HostApplication = Connecta;
            break;

    }


};

export default class ConnectaMaps extends EventEmitter {

    static MapsDataLoader = MapsDataLoader;
    static APP_CONFIG = appConfig;
    static ConnectaService = ConnectaService;
    static scope = scope;
    static themeConfig = themeConfig;

    dispatcher = new Dispatcher();
    _instancesScope = {};
    node;
    moduleInstance;
    requestHeader = {};
    rendered = false;
    hostApplication;
    DEV = false;

    /**
     *
     * @type {{Icon:React.Component, onClick:Function}[]}
     */
    extensions = [];

    _viewerId;

    link;

    set viewerId(value) {
        if (!this._viewerId) {
            this._viewerId = value;
        }
    }
    get viewerId() {
        return this._viewerId;
    }

    constructor(node,
                viewerId = '',
                serviceDomain = '',
                user = {}
    ) {
        super();
        this.node = node;
        this.viewerId = viewerId;
        this.user = user;

        buildHostApplication();

        if (HostApplication) {
            this.hostApplication = new HostApplication(this);
            this.hostApplication.build();
        }

        if (serviceDomain) {
            appConfig.serviceDomain = serviceDomain;
        }

        this.buildScopeInstance();
        window['mapsInstances'].push(this);
    }

    /**
     * Método usado para registrar/inputar extensões ao Maps.
     * @param extensionConfig
     */
    putExtension(extensionConfig = { Icon : null, onClick : null }) {
        this.extensions.push(extensionConfig);
    }

    _buildLink() {
        let domainId = this.requestHeader['c-maps-domain-id'];
        let config = this.DEV ? appConfig['DEV'] : appConfig['PROD'];
        this.link = config['staticServer'] + `#/maps-viewer/${this.viewerId}/domain/${domainId}`;
    }

    render() {
        this._buildLink();
        this.rendered = true;
        this.mapAPI = new MapAPI(this);
        this.moduleInstance = ReactDOM.render(
            <AppModule viewerId={this.viewerId} user={this.user} $parent={this}/>,
            this.node
        );
        this.changeCSSColors();
    }

    changeCSSColors = (themeConfig = ConnectaMaps.themeConfig) => {
        let styles = document.querySelectorAll('style');

        for (let style of styles) {
            if (/cmaps/.test(style.innerHTML)) {
                style.innerHTML = style.innerHTML.replace(/['"]\$PRIMARY_COLOR['"]/g, themeConfig.palette.primary1Color);
                style.innerHTML = style.innerHTML.replace(/['"]\$PRIMARY2_COLOR['"]/g, themeConfig.palette.primary2Color);
            }
        }
    };

    isShown(){
        return this.rendered;
    }

    updateViewer(){
        this.moduleInstance.updateViewer();
    }

    inject(scope) {
        return this._instancesScope[scope.namespace];
    }

    destroy() {
        this.moduleInstance.destroy();
        ReactDOM.unmountComponentAtNode(this.node);
    }

    setFilter(filters) {
        let $appAction = this.inject(scope.AppAction);
        $appAction.setExternalFilter(filters);
    }

    buildScopeInstance() {
        try {
            for (let key in scope) {
                if (typeof scope[key] !== 'function') {
                    let config = scope[key];
                    this._instancesScope[config.namespace] = new config.Class(this);
                }
            }

            for (let key in this._instancesScope) {
                if (typeof this._instancesScope[key] !== 'function') {
                    let instance = this._instancesScope[key];
                    if (instance.$prepare) {
                        instance.$prepare();
                    }
                }
            }
        } catch (error) {
            console.error(error);
        }
    }

}

buildMAP();

if (window['env_maps']) {
    let locationHash = window.location.hash;
    let viewerId = ''; // Será pego dinamicamente pelo hash: /#/map-viewer/:viewerId/domain/:domainId
    let domainId = '';// Será pego dinamicamente pelo hash: /#/map-viewer/:viewerId/domain/:domainId
    let connectaUrl = appConfig.serviceDomain + '/proxy/' + appConfig.hostApplicationDomain + '/connecta-presenter';
    // let connectaUrl = '//localhost:4100/proxy/' + appConfig.hostApplicationDomain + '/connecta-presenter';

    if (pattern.test(locationHash)) {
        viewerId = window.location.hash.replace(pattern, '$1');
        domainId = Number(window.location.hash.replace(pattern, '$2'));
    }

    if (!viewerId || !domainId) {
        window.location = '#/maps-viewer/{viewerID}/domain/{domainId}';
    } else {
        let instance = new ConnectaMaps(document.getElementById('root'));
        instance.requestHeader['c-maps-domain-id'] = domainId;
        let connectaService = new ConnectaService(instance);

        let options = {
            domainId : domainId,
            connectaUrl
        };

        connectaService.setup(viewerId, options)
            .then((viewer) => {
                instance.viewerId = viewer._id;
                MapsDataLoader.set(viewer._id, viewer);
                instance.DEV = true;
                instance.render();
            });
    }
}

window['ConnectaMaps'] = ConnectaMaps;
