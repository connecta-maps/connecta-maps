/*global event, fdescribe*/
/*eslint no-restricted-globals: ["error", "event", "fdescribe"]*/

export const choroplethMap = function choroplethMap() {


    /**
     * Emite um GeoJSON alterado, em cada feature possui uma propriedade $style para que seja aplicado em sua tematização
     * @param event
     */
    self.onmessage = function (event) {

        /**
         * @type {{
         * fillConfig : {},
         * sizeConfig : {},
         * fillColors : String[],
         * fillBreaks : Number[],
         * sizeBreaks : Number[],
         * uniqueValues : {value:*, symbology : {fillColor:String}}[],
         * uniqueValuesConfig : {field : String, colorRamp : []},
         * geoJSON : Object
         * }}
         */
        let config = event.data;
        let geoJSON = config.geoJSON,
            fillColors = config.fillColors,
            fillConfig = config.fillConfig,
            fillBreaks = config.fillBreaks,
            sizeConfig = config.sizeConfig,
            sizeBreaks = config.sizeBreaks,
            uniqueValues = config.uniqueValues,
            uniqueValuesConfig = config.uniqueValuesConfig,
            style = {
                weight: 0.5,
                opacity: 1,
                color: 'white',
                fillOpacity: 1
            },
            meters,
            i = 0;

        try {

            if (sizeConfig && sizeBreaks) {

                meters = ((meter) => {
                    let j = 0,
                        arr = [];

                    while (j <= sizeConfig.breakCount) {
                        arr.push(sizeConfig.minSize + (meter * j));
                        j += 1;
                    }

                    return arr;
                })((sizeConfig.maxSize - sizeConfig.minSize) / sizeConfig.breakCount);

            }

            while (i < geoJSON.features.length) {
                let feature = geoJSON.features[i];

                if (!feature.businessData) {
                    i += 1;
                    continue;
                }

                if (fillConfig && fillBreaks) {
                    let fillColor = doFill(fillBreaks, fillConfig, fillColors, feature);

                    if (fillColor !== undefined) {
                        feature.$style = Object.assign({}, style,
                            {fillColor: fillColor}
                        );
                    }
                }

                if (/Point/.test(geoJSON.geometryType)) {
                    if (sizeBreaks && sizeConfig) {
                        let radius = doSize(sizeBreaks, sizeConfig, feature, meters);

                        if (radius !== void 0) {
                            feature.$style = Object.assign({}, feature.$style,
                                {radius: radius}
                            );
                        }
                    }
                }

                if (uniqueValues) {
                    let fillColor = doSymbologyUniqueValues(uniqueValues, uniqueValuesConfig, feature);

                    if (fillColor !== undefined) {
                        feature.$style = Object.assign({}, style,
                            {fillColor: fillColor}
                        );
                    }
                }

                if (!feature.$style) {
                    feature.$style = {
                        opacity: 0,
                        fillOpacity: 0
                    };
                }

                i += 1;
            }

            self.postMessage({geoJSON, meters});

        } catch (error) {
            console.error('ERROR: worker/cloropleth-map', error.message);
            throw error;
        }
    };

    function doSymbologyUniqueValues (uniqueValues, uniqueValuesConfig, feature) {
        let _value = feature.businessData[uniqueValuesConfig.field];
        let config = uniqueValues.filter(uniqueValue => uniqueValue.value === _value).pop();

        return config.symbology.fillColor;
    }

    function doSize(sizeBreaks, sizeConfig, feature, meters) {
        let _value = feature.businessData[sizeConfig.classificationField];
        let value = (typeof _value === 'number') ? _value : Number(_value);
        let radius;

        if (sizeBreaks && value) {
            let i = 0;
            while (i < sizeBreaks.length) {
                if (value >= sizeBreaks[i] && value < sizeBreaks[i + 1]) {
                    radius = meters[i];
                    break;
                }
                i += 1;
            }

            // FIXME I'm scared about this shit
            if (i === meters.length) {
                radius = meters[i - 1];
            }
        }

        return radius;
    }

    function doFill(fillBreaks, fillConfig, fillColors, feature) {
        let _value = feature.businessData[fillConfig.classificationField];
        let value = ( (typeof _value === 'number') ? _value : Number(_value) ).toFixed(2);
        let fillColor;

        if (fillBreaks && value) {
            let i = 0;
            while (i < fillBreaks.length) {
                if (value >= fillBreaks[i] && value < fillBreaks[i + 1]) {
                    fillColor = fillColors[i];
                    break;
                }
                i += 1;
            }
            // FIXME I'm scared about this shit
            if (i === fillBreaks.length) {
                fillColor = fillColors[i - 1];
            }
        }

        return fillColor;
    }

};
