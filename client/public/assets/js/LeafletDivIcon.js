L.CustomDivIcon =  L.Icon.extend({
    options: {
        iconText: '',
        iconSize: [0, 0],
        fontSize : '',
        fontColor : '',
        pointerEvents : ''
    },

    createIcon: function () {
        var div = document.createElement('div');
        var img = this._createImg(this.options['iconUrl']);
        var text = document.createElement('div');
        div.style.pointerEvents = this.options.pointerEvents;
        img.style.width = (this.options.iconSize[0]) + 'px' ;
        img.style.height = (this.options.iconSize[1]) + 'px' ;
        text.innerHTML = this.options['iconText'] || '';
        text.style.position = 'absolute';
        text.style.textAlign = 'center';
        text.style.fontWeight = 'bold';
        text.style.color = this.options.fontColor || "#000";
        text.style.fontSize = (this.options.fontSize + "px") || "14px";
        text.style.borderRadius = "50%";
        text.style.padding = "0 2px";
        text.style.top = '46%';
        text.style.left = '48%';
        text.style.transform = 'translate(-50%, -50%)';
        text.style.whiteSpace = 'nowrap';


        if (this.options['iconUrl'] !== "") {
            div.appendChild(img);
        }

        div.appendChild(text);
        this._setIconStyles(div, 'icon');
        return div;
    }
});

L.customDivIcon = function(options){
    return new L.CustomDivIcon(options);
};
