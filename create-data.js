const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
let total = 50000;
let i = 0;
let pathFile = path.resolve(__dirname, 'sample.json');

if (fs.existsSync(pathFile)) {
    fs.unlinkSync(pathFile);
}
let keys = [1,2,3,4,5,6,7,8];
let arr = [];

while(i < total) {
    arr.push(
        {
            "AREA" : Math.floor(Math.random() * 10000),
            "b" : Math.random() * 100000,
            "c" : Math.random() * 100000,
            "d" : Math.random() * 100000,
            "ID" : keys[Math.floor(Math.random() * keys.length)]
        }
    );
    i += 1;
}

fse.writeJson(pathFile, arr);