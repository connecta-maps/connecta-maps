const crypto = require("crypto");
const path = require("path");
const fs = require("fs");
const fse = require("fs-extra");
const request = require("request");

const pathData = path.resolve(__dirname, '../', 'data');

class Proxy {

    build(server) {
        let cb = (requestObject, responseObject) => {
            let url = requestObject.params[0];
            let headers = requestObject.headers;

            if (headers.domain && /connecta-presenter/.test(url)) {
                headers = {
                    Domain : Number(headers.domain)
                };
            }

            if (/https/.test(url)) {
                headers = {
                    'User-Agent' : 'request',
                    'accept' : '*/*'
                };
            }

            let params = {
                url,
                headers,
            };

            if (requestObject.method === 'GET') {
                params.method = "GET";
                params.qs = requestObject.query;

            } else {
                params.method = "POST";
                params.json = true;
                params.body = requestObject.params;
            }

            let hash = this.createHash(params);
            let pathCache = path.resolve(pathData, hash + '.json');

            if (fs.existsSync(pathCache)) {
                responseObject.setHeader('content-type', 'application/json');
                return fs.createReadStream(pathCache).pipe(responseObject);
            }

            request(params, (error, res, data) => {
                if (error) {
                    return responseObject.sender.error(error);
                }

                process.nextTick(() => {
                    try {
                        if (/application\/json/.test(res.headers['content-type']) && res.statusCode === 200) {
                            if (typeof data === 'object') {
                                data = JSON.stringify(data);
                            }
                            if (!fs.existsSync(pathCache)) {
                                fse.ensureDir(pathData, (error) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        fs.writeFile(pathCache, data, (error) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                console.info('Cache gerado. Hash: ' + pathCache);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    } catch (error) {
                        console.error('CACHE_ERROR:proxy', error);
                    }
                });

            }).pipe(responseObject);
        };

        server.putRoute('get', /\/proxy\/(.*)/, cb);
        server.putRoute('post', /\/proxy\/(.*)/, cb);
    }

    createHash(...value) {
        let str = JSON.stringify(value);
        let md5sum = crypto.createHash('md5');
        md5sum.update(str);
        return md5sum.digest('hex');
    }

}

module.exports = Proxy;