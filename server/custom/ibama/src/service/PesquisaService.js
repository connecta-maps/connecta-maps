const oracledb = require('oracledb');
const connector = require('../helper/Connector');
const SQLHelper = require('../helper/SQLHelper');

class PesquisaService {

    search(params) {
        return this._search(params)
    }

    _search(params = {}) {
        let tipologia = "tipologia";
        let columns = {numeroProcesso: 'NU_PROCESSO_IBAMA',
                       empreendimento: 'UPPER(NO_EMPREENDIMENTO)',
                       empreendedor: 'UPPER(NO_EMPREENDEDOR)',
                       [tipologia]: 'CD_TIPOLOGIA' };
        let whereClauses = [];

        for(let key in params){
            let value = params[key];
            if(value && key !== tipologia){
                if(typeof value === 'string'){
                    value = value.toUpperCase();
                }

                whereClauses.push({column: columns[key], value: value});
            }
        }

        return new Promise((resolve, reject) => {
            let str = SQLHelper.getSql('SQL_PESQUISA.sql', {});

            if (whereClauses.length > 0) {
                str += " AND (" + whereClauses.map((whereClause) => whereClause.column + " LIKE '%"+ whereClause.value + "%'")
                            .join(" OR ") + ")";
            }

            let tipologiaValue = params[tipologia];
            if (tipologiaValue) {
                str +=  " AND "+ columns[tipologia] + " =" + tipologiaValue;
            }

            connector.execute(str, [], { outFormat: oracledb.OBJECT })
                .then((result) => {
                    if(result.rows){
                        resolve(result.rows);
                    }else{
                        reject("Nenhum resultado foi encontrado");
                    }
                })
                .catch(reject);
        });
    }
}

module.exports = PesquisaService;
