const Router = require('./helper/Router');
const PesquisaService = require('./service/PesquisaService');


class PesquisaController {

    constructor(server) {
        this.server = server;
        this.router = new Router(server);
        this.pesquisaService = new PesquisaService();
        this.router.get('/client/search', ['numeroProcesso', 'empreendimento', 'empreendedor', 'tipologia'], this.search.bind(this));
    }

    search(params, req, res) {
        try {
            let {numeroProcesso, empreendimento, empreendedor, tipologia} = params;

            if (!numeroProcesso && !empreendimento && !empreendedor && !tipologia) {
                throw new Error(`Parâmetro: Obrigatório um dos parâmetros Número do Processo,
                                 Nome do Empreendimento, Nome do Empreendedor ou Tipologia`);
            }

            this.pesquisaService.search(params)
                .then(res.sender.end.bind(res.sender))
                .catch(res.sender.error.bind(res.sender));

        } catch (error) {
            res.sender.error(error);
        }
    }


}

module.exports = PesquisaController;
