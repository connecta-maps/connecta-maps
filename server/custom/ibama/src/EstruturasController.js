const Router = require('./helper/Router');
const EstruturasService = require('./service/EstruturasService');
const ViewerService = require('./service/ViewerService');

class EstruturasController {

    constructor(server) {
        this.server = server;
        this.router = new Router(server);
        this.viewerService = new ViewerService(server);
        this.router.get('/client/create-viewer', ['processIds', 'tipologiaIds'], this.createViewer.bind(this));
        this.router.get('/client/estruturas', ['processId'], this.getEstruturasByProcessId.bind(this));
        this.router.get('/client/setor', [], this.getSetor.bind(this));
        this.router.get('/client/tipologias', ['setorId'], this.getTipologiasBySetor.bind(this));
    }

    getSetor() {
        return EstruturasService.getSetor()
            .then(result => result.rows);
    }

    getTipologiasBySetor(params) {
        return EstruturasService.getTipologiasBySetor(params.setorId)
            .then(result => result.rows);
    }

    createViewer(params) {
        let processIds = params['processIds'];
        let tipologiaIds = params['tipologiaIds'];

        if (!processIds) {
            throw new Error(`Parâmetro: processIds é obrigátorio. 
                    Envie uma string contendo os números dos processos, separados por vírgula`);
        }

        processIds = processIds ? processIds.split(',') : '';
        tipologiaIds = tipologiaIds ? tipologiaIds.split(',') : '';

        return this.viewerService.createViewer(processIds, tipologiaIds);
    }

    getEstruturasByProcessId(params) {
        if (!params.processId) {
            throw new Error(`Parâmetro: processId é obrigátorio.`);
        }
        return EstruturasService.getEstruturasByProcessId(params.processId)
            .then(result => result.rows);
    }

}

module.exports = EstruturasController;
