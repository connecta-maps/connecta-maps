const EstruturasController = require('./EstruturasController');
const PesquisaController = require('./PesquisaController');
const MiddlewareIBAMA = require('./MiddlewareIBAMA');

class IBAMA {

    static load(Server) {
        Server.requestMiddleware.push(MiddlewareIBAMA);
    }

    build(server) {
        new EstruturasController(server);
        new PesquisaController(server);
    }

}

module.exports = IBAMA;
