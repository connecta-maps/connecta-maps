const oracledb = require('oracledb');
const config = require('../config');

let instance;

class Connector {

    constructor() {
        if (instance) {
            return instance;
        }
        instance = this;
    }

    getConnection() {
        return this._exec(oracledb.getConnection, {
            user          : config.dbConfig.user,
            password      : config.dbConfig.password,
            connectString : config.dbConfig.connectString
        });
    }

    _closeConnection(connection) {
        if (connection) {
            connection.close((err) => {
                if (err) {
                    console.error(err.message);
                }
            });
        }
    }

    _exec(fn, ...args) {
        return new Promise((resolve, reject) => {
            args.push((error, ...args) => {
                if (error) {
                    reject(error);
                } else {
                    resolve.apply(this, args);
                }
            });
            fn.apply(oracledb, args);
        });
    }

    execute(...args) {
        return this.getConnection().then(connection => {
            return new Promise((resolve, reject) => {
                args.unshift(connection.execute.bind(connection));

                if (typeof args[args.length - 1] === 'function') {
                    args.splice(args.length - 1, 1);
                    console.warn('Use Promise');
                }

                this._exec.apply(this, args)
                    .then(result => {
                        resolve(result);
                        this._closeConnection(connection);
                    })
                    .catch(error => {
                        this._closeConnection(connection);
                        reject(error);
                    });
            });
        });
    }
}

instance = new Connector();
module.exports = instance;