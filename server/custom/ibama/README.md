## Install:

- Python 2.7
- rpm -ivh oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
- rpm -ivh oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm

## Opcional
- Para instalar no RPM no UBUNTU: sudo apt-get install alien 
- Talvez precise instalar os pacotes(UBUNTU): sudo apt-get install libaio1 libaio-dev
- Talvez precice instalar os pacotes essenciais do C++ => yum groupinstall 'Development Tools'

- Sera necessario exportar as seguintes variaveis. 
-- Se for linux edite o arquivo .profile__ ou .bash_profile no home:

> export ORACLE_HOME="/usr/lib/oracle/12.2/client:$ORACLE_HOME"

> export LD_LIBRARY_PATH="/usr/lib/oracle/12.2/client64/lib:$LD_LIBRARY_PATH"

Siga os passos:
https://github.com/oracle/node-oracledb/blob/master/INSTALL.md

