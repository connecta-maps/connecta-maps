const OcorrenciasController = require('./controller/OcorrenciasController');

class VALEC {

    build(server) {
        new OcorrenciasController(server);
    }

}

module.exports = VALEC;