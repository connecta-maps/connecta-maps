const Conversor = require('../helper/Conversor');
const Connector = require('../helper/Connector');
const oracledb = require('oracledb');

class OcorrenciasController {

    constructor(server) {
        this.server = server;
        this.tbName = 'TB_OCORRENCIA_SIOCA';

        server.putRoute('get', '/valec/ocorrencias', this.getOcorrencias.bind(this));
    }

    getOcorrencias(req, res) {
        try {
            let {
                filter
            } = req.query;

            if (filter && filter.length) {
                filter = JSON.parse(filter);
            }

            let sql = Conversor.getSQLString(this.tbName, filter);

            Connector.execute(sql, [], { outFormat: oracledb.OBJECT })
                .then(({rows}) => {
                    res.sender.end({ sql, result : rows });
                })
                .catch(res.sender.error.bind(res.sender));

        } catch (error) {
            res.sender.error(error);
        }
    }

}

module.exports = OcorrenciasController;