const gulp = require('gulp');
const ts = require('gulp-typescript');
const gulpSourcemaps = require('gulp-sourcemaps');
const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const del = require('del');
const uglify = require('gulp-uglify-es').default;

const tsConfig = JSON.parse(fs.readFileSync(
    path.resolve(__dirname, 'tsconfig.json')
).toString());

const ConfigJSON = JSON.parse(fs.readFileSync(
    path.resolve(__dirname, '../', '../', 'Config.json')
).toString());

const pathTarget = path.resolve(__dirname, '../', 'target');

const emptyTarget = () => {
    let sources = [
        './helper/data'
    ];

    fse.removeSync(path.resolve(pathTarget, '../', 'custom', 'proxy', 'data'));

    for (let sourcePath of sources) {
        let currentPath = path.resolve(pathTarget, sourcePath);
        fse.ensureDirSync(currentPath);
        fse.copySync(sourcePath, currentPath);
    }


    let removeDirectories = (currentPath) => {
        let directories = fs.readdirSync(currentPath);

        for (let directoryPath of directories) {
            let iterPath = path.resolve(currentPath, directoryPath);
            if (fs.statSync(iterPath).isDirectory()) {
                removeDirectories(iterPath);
            } else {
                if (!(/node_modules|package\.json|helper\/data/.test(iterPath))) {
                    fse.removeSync(iterPath);
                }
            }
        }
    };

    removeDirectories(pathTarget);
};

emptyTarget();

gulp.task('compile', () => {
    let paths = ['./**/*.ts', '!./node_modules/**'];

    if (ConfigJSON.server.DEV) {
        return gulp.src(paths)
            .pipe(gulpSourcemaps.init())
            .pipe(ts(tsConfig.compilerOptions))
            .pipe(gulpSourcemaps.write())
            .pipe(gulp.dest(pathTarget));

    } else {
        return gulp.src(paths)
            .pipe(ts(tsConfig.compilerOptions))
            .pipe(uglify({
                output: {
                    beautify: false,
                    comments : false
                }
            }))
            .pipe(gulp.dest(pathTarget));
    }
});

gulp.task('default', ['compile']);