///<reference path="../enum/GeoCacheOperationName.ts"/>
import {ServerException} from "../global/ServerException";
import Config from "../Config";
import * as mongoose from "mongoose";
import {Layer} from "./schema/Layer";
import {SpatialDataSource, SpatialDataSourceSchema} from "./schema/SpatialDataSource";
import {GeoCacheOperationName} from "../enum/GeoCacheOperationName";
import {ServerType} from "../enum/ServerType";
import {Viewer} from "./schema/Viewer";
import INDECrowler from "../helper/INDECrowler";

export abstract class Connector {

    static dbConnector:any;
    static geoDB:any;

    static updateDB(){
        // this.updateViewer();
        Connector.saveINDE();
    }

    static saveINDE() {
        new INDECrowler().init();
    }

    //TODO: remover
    static updateViewer(){
        Viewer.Model.find({}, (err, viewers) => {
            if (err) {
                console.error("Error to update viewers")
            } else {
                Viewer.Model.remove({}, (err) => {
                    if (err) {
                        console.error("Error to update viewers");
                        return;
                    }
                    viewers.forEach((item) => {
                        let {viewer, updated} = Connector._updateViewer(item);

                        if (updated) {
                            item.toolsConfig = viewer.toolsConfig;
                        }

                        item._id = String(item._id);
                        new Viewer.Model(item.toObject()).save();
                    });
                });
            }
        });
    }

    static _updateViewer(viewer){
        let updated = false;
        if (viewer && viewer.toolsConfig && viewer.toolsConfig) {
            if (viewer.toolsConfig.analysisConfig && viewer.toolsConfig.analysisConfig.length > 0
                && viewer.toolsConfig.analysisConfig[0].analyses) {
                viewer.toolsConfig.analysisConfig = [{
                    _id: String(new Date().getTime()),
                    type: "GROUP",
                    title: "Grupo",
                    children: viewer.toolsConfig.analysisConfig[0].analyses
                }];
                updated = true;
            }

            if(viewer.toolsConfig.layersConfig && viewer.toolsConfig.layersConfig.layers){
                viewer.toolsConfig.layersConfig = [{
                    _id: String(new Date().getTime()),
                    type: "GROUP",
                    title: "Grupo",
                    children: viewer.toolsConfig.layersConfig.layers
                }];
                updated = true;
            }
        }

        return {viewer, updated};
    }

    static load(dbConfigName:string = 'mainDB'):Promise<mongoose.Connection> {
        return new Promise<mongoose.Connection>(function (resolve, reject) {
            try {
                let user = Config[dbConfigName].dbUser && Config[dbConfigName].dbPass ?
                    `${Config[dbConfigName].dbUser}:${Config[dbConfigName].dbPass}@`
                    : '';

                let connector;

                connector = mongoose.createConnection((`mongodb://
                        ${user}
                        ${Config[dbConfigName].dbHost}:${Config[dbConfigName].dbPort}/${Config[dbConfigName].dbName}
                    `).replace(/[\n\t\r\s]+/g, ''));

                if (dbConfigName === 'mainDB') {
                    Connector.dbConnector = connector;

                } else if (dbConfigName === 'geoDB') {
                    Connector.geoDB = connector;

                }

                connector.on('connected', () => {
                    if (connector.readyState === 1) {
                        resolve(connector);

                    } else if (connector.readyState === 0) {
                        connector.open(Config[dbConfigName].dbHost, Config[dbConfigName].dbName, Config[dbConfigName].dbPort);

                    } else {
                        reject(new ServerException('SERVER_ERROR_01'));
                    }
                });

                connector.on('error', error => {
                    console.info('Trying connection with MongoDB again...');

                    setTimeout(() => {
                        Connector.load(dbConfigName).then(resolve, reject);
                    }, 4000);
                });

            } catch (error) {
                reject(error);
            }

        });
    }

    static getConnection() {
        if (!Connector.dbConnector) {
            throw new ServerException('SERVER_ERROR_01');
        }
        return Connector.dbConnector;
    }

}