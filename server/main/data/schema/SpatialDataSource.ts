import {ServerType} from "../../enum/ServerType";
import {SchemaBuilder} from "../../helper/SchemaBuilder";
import * as mongoose from 'mongoose';
import {ServerException} from "../../global/ServerException";
let Project;

/**
 *
 */
export class SpatialDataSourceSchema {

    _id: any = {
        type: String,
        default: function(value){
            let str = String(Math.random() * 100000 * Date.now());
            return str.substr(str.length - 5);
        }
    };

	/**
	 * Identificador do tipo de servidor espacial que este documento representará.
	 * @type {String}
	 */
	serverType: any = {
		type: String,
		required: true,
		enum: [
			ServerType.ArcGIS,
			ServerType.GeoServer,
			ServerType.MapServer,
			ServerType.MapViewer,
			ServerType.OGC,
			ServerType.Oracle,
			ServerType.PostGIS
		]
	};

	/**
	 *
	 * @type {{type: StringConstructor; required: boolean}}
	 */
	title: any = {
		type: String,
		required: true
	};

	/**
	 *
	 * @type {{type: StringConstructor}}
	 */
	description: any = {
		type: String
	};

	/**
	 * DSN de acesso a origem dos dados espaciais.
	 * @type {String}
	 */
	dsn: any = {
		type: String,
		required: true
	};

	/**
	 *
	 * @type {{type: StringConstructor}}
	 */
	user: any = {
		type: String
	};

	/**
	 *
	 * @type {{type: StringConstructor}}
	 */
	password: any = {
		type: String
	};

    /**
     *
     * @type {String}
     */
	token:any = {
        type : String
    };


	/**
	 *
	 * @type {{type: ObjectConstructor}}
	 */
	info: any = {
		type: Object
	};

    /**
     *
     * @type {{type: StringConstructor; required: boolean}}
     */
	domainId: any = {
        type: String,
        required: true
    };

}


/**
 *
 */
export abstract class SpatialDataSource {
	static Model: mongoose.Model<any>;

	static load(): Promise<any> {
		return new Promise((resolve, reject) => {
			let schemaBuilder: SchemaBuilder = new SchemaBuilder();
			schemaBuilder.buildSchema(SpatialDataSourceSchema, {
				collection: 'spatial_data_source',
				_id: false
			});

			schemaBuilder.buildPre('remove', function (next) {
				try {
					Project = require('./Project').Project;

					Project.Model.find({richLayers: { $elemMatch: {'layer.spatialDataSourceId': this._id}}}, (err, docs) => {
						try {
							if (docs.length) {
								throw new ServerException('RES_ERROR_16', ['spatialDataSource', 'projeto']);
							}
							next();
						} catch (err) {
							next(err);
						}
					});
				} catch (err) {
					next(err);
				}
			});

			schemaBuilder.buildModel('spatialDataSource');
			SpatialDataSource.Model = schemaBuilder.getModel();
			resolve();
		});

	}
}