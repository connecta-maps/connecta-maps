import {SchemaBuilder} from "../../helper/SchemaBuilder";
import * as mongoose from "mongoose";

export class GeoCacheSchema {

    hash:any = {
        type : String,
        unique : true
    };

    params:any = {
        type : Object
    };

    info:any = {
        type : mongoose.Schema.Types.Mixed
    };

    createdAt:any = {
        type: Date,
        'default' : Date.now,
        index: {
            expires: '2d'
        }
    };

}

export class GeoCache {

    static Model:mongoose.Model<any>;

    static load():Promise<any> {
        return new Promise((resolve, reject) => {
            let schemaBuilder:SchemaBuilder = new SchemaBuilder();
            schemaBuilder.buildSchema(GeoCacheSchema, {
                collection : 'geo_cache'
            });
            schemaBuilder.buildModel('geo_cache');
            GeoCache.Model = schemaBuilder.getModel();

            resolve();
        });
    }

}
