export enum FormatImage{

    PNG = <any>"png",
    PNG8 = <any>"png8",
    PNG24 = <any>"png24",
    png32 = <any>"png32",
    JPG = <any>"jpg",
    PDF = <any>"pdf",
    BMP = <any>"bmp",
    GIF = <any>"gif",
    SVG = <any>"svg"

}
