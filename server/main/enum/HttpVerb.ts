/**
 * Verbos do HTTP
 * @enum
 */
export enum HttpVerb{
    GET = <any>'get',
    POST = <any>'post',
    PUT = <any>'put',
    DELETE = <any>'del',
    ALL = <any>'head',
    PATCH = <any>'patch',
    OPTIONS = <any>'opts'
}