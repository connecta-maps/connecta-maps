
export enum GeoCacheOperationName {

    GET_MAP = <any>"getMap",
    QUERY = <any>"query",
    GET_BREAKS = <any>"getBreaks"

}
