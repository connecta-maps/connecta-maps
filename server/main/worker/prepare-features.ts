/*global self*/

process.on('message', ({config, geoJSON, businessData}) => {
    let features = [];

    try {
        let i = geoJSON.features.length - 1;

        while (i >= 0) {
            let geoFeature = geoJSON.features[i];

            geoFeature.properties = {
                [config.richLayer.crossingKeys.geoKey]: geoFeature.properties[config.richLayer.crossingKeys.geoKey]
            };

            toChain([
                prepareGeometry,
                prepareBusinessData
            ], geoFeature);

            if (geoFeature.businessData) {
                features.push(geoFeature);
            }

            i -= 1;
        }

        geoJSON.type = "FeatureCollection";
        geoJSON.features = features;
        process.send(geoJSON);

    } catch (error) {
        console.log('ERROR: worker/arcgis-to-geojson', error.message);
        throw error;
    }

    function toChain(fns, geoFeature) {
        let current = 0;

        execute();

        function execute() {
            (fns[current] || function () {}).call(geoFeature, next);
        }

        function next() {
            current += 1;
            execute();
        }
    }

    function prepareGeometry(next) {
        this.id = String(Math.floor(Date.now() + Math.random() * 1000));
        next();
    }

    function prepareBusinessData(next) {
        let geoValueKey = this.properties[config.richLayer.crossingKeys.geoKey];

        for (let featureSet of businessData) {
            let featureSetValueKey = featureSet[config.richLayer.crossingKeys.resultSetKey];

            if (String(featureSetValueKey) === String(geoValueKey)) {
                this.businessData = featureSet;
                break;
            }
        }

        next();
    }
});
