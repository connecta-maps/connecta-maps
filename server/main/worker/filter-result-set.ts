/*global scope*/

const OPERATORS_NAMES = {
    EQUAL : "EQUAL",
    DIFFERENT : "DIFFERENT",
    GREATER : "GREATER",
    GREATER_OR_EQUAL : "GREATER_OR_EQUAL",
    LESS : "LESS",
    LESS_OR_EQUAL : "LESS_OR_EQUAL",
    LIKE : "LIKE",
    BETWEEN : "BETWEEN",
    NOT_BETWEEN : "NOT_BETWEEN",
    DISTINCT : "DISTINCT",
    IN : "IN"
};

let scope:any = {};

scope[OPERATORS_NAMES.EQUAL] = EQUAL;
scope[OPERATORS_NAMES.DIFFERENT] = DIFFERENT;
scope[OPERATORS_NAMES.GREATER] = GREATER;
scope[OPERATORS_NAMES.GREATER_OR_EQUAL] = GREATER_OR_EQUAL;
scope[OPERATORS_NAMES.LESS] = LESS;
scope[OPERATORS_NAMES.LESS_OR_EQUAL] = LESS_OR_EQUAL;
scope[OPERATORS_NAMES.LIKE] = LIKE;
scope[OPERATORS_NAMES.BETWEEN] = BETWEEN;
scope[OPERATORS_NAMES.NOT_BETWEEN] = NOT_BETWEEN;
scope[OPERATORS_NAMES.DISTINCT] = DISTINCT;
scope[OPERATORS_NAMES.IN] = IN;
scope['reorderFilters'] = reorderFilters;
scope['whereClauseBuilder'] = whereClauseBuilder;

process.on('message', (data) => {
    try {
        let businessData = data.businessData;
        let paramFilters = data.filterConfig;
        let richLayer = data.richLayer;
        let resultSetKey = richLayer.crossingKeys.resultSetKey;

        let operator,
            newResultSet = Object.assign([], businessData),
            filters = [].concat(paramFilters);

        if (filters.length) {
            filters = filters.sort(scope.reorderFilters);

            filters.forEach((filter) => {
                operator = filter.operator;

                newResultSet = scope[operator]().filter(filter, newResultSet);
            });
        }

        process.send({
            businessData: newResultSet,
            where: newResultSet.length ? scope.whereClauseBuilder(newResultSet, resultSetKey, richLayer) : ''
        });
    } catch (error) {
        console.error(error);
    }
});

function EQUAL () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                let value = tuple[filter.field];
                let field;
                if (value) {
                    field = value;
                    if (field == filter.value) {
                        return tuple;
                    }
                }
            });
        }
    }
}

function DIFFERENT () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                let value = tuple[filter.field];
                let field;
                if (value) {
                    field = value;
                    if (field != filter.value) {
                        return tuple;
                    }
                }
            });
        }
    }
}

function GREATER () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] > filter.value) {
                    return tuple;
                }
            });
        }
    }
}

function GREATER_OR_EQUAL () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] >= filter.value) {
                    return tuple;
                }
            });
        }
    }
}

function LESS () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] < filter.value) {
                    return tuple;
                }
            });
        }
    }
}

function LESS_OR_EQUAL () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] <= filter.value) {
                    return tuple;
                }
            });
        }
    }
}

function LIKE () {
    return {
        filter : (filter, resultSet) => {
            let regex;

            if (!(/%/.test(filter.value))) {
                regex = new RegExp("^" + filter.value + "$");
            } else {
                regex = new RegExp("^" + filter.value.replace(new RegExp("%", "gi"), "(.+)?") + "$");
            }

            return resultSet.filter((tuple) => {
                let value = tuple[filter.field];

                if (value) {
                    let field = value.trim();
                    if (regex.test(field)) {
                        return tuple;
                    }
                }
            });
        }
    }
}

function BETWEEN () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] >= filter.initialValue && tuple[filter.field] <= filter.finalValue) {
                    return tuple;
                }
            });
        }
    }
}

function NOT_BETWEEN () {
    return {
        filter : (filter, resultSet) => {
            return resultSet.filter((tuple) => {
                if (tuple[filter.field] < filter.initialValue && tuple[filter.field] > filter.finalValue) {
                    return tuple;
                }
            });
        }
    }
}

function DISTINCT () {
    return {
        filter : (filter, resultSet) => {
            let mapDistinctKeys = {};
            let newResultSet = [];

            resultSet.forEach((tuple, index) => {
                if (!mapDistinctKeys[tuple[filter.field]]) {
                    newResultSet.push(tuple);
                    mapDistinctKeys[tuple[filter.field]] = index;
                } else {
                    newResultSet.splice(mapDistinctKeys[tuple[filter.field]], 1 , tuple);
                }
            });
            return newResultSet;
        }
    }
}

function IN () {
    return {
        filter : (filter, resultSet) => {
            let newResultSet = [];

            resultSet.forEach((tuple) => {
                filter.value.forEach((item) =>{
                    if(item == tuple[filter.field]){
                        newResultSet.push(tuple);
                    }
                });
            });

            return newResultSet;
        }
    }
}

function reorderFilters (a) {
    if (a.operator === OPERATORS_NAMES.DISTINCT) {
        return 1;
    }
    return -1;
}

function whereClauseBuilder (resultSet, resultSetKey, richLayer) {
    if (resultSet.length) {
        let where:any = [];
        let typeField = richLayer.layer.layerFields.filter(({name})=>{
            return name === richLayer.crossingKeys.geoKey;
        })[0].type;

        for (let index in resultSet) {
            let tuple = resultSet[index];

            if (!where.includes(String("'" + tuple[resultSetKey] + "'"))) {
                if (typeField === "esriFieldTypeString") {
                    where.push(String("'" + tuple[resultSetKey] + "'"));
                } else {
                    where.push(String(tuple[resultSetKey]));
                }
            }
        }

        where = " IN (" + where.join(",") + ")";

        return where;
    }
}

