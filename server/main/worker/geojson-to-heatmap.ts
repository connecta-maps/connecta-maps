/*global self*/

declare let self:any;

export const geoJSONToHeatMap = function geoJSONToHeatMap () {

    self.onmessage = function (event) {
        let config = event.data.config || {};
        let colors = config.colors || ['blue', 'green', 'yellow', 'red'];
        let geoJSON = event.data.geoJSON;
        let features = geoJSON.features || [];
        let legendInfo:any = {
            colors: colors
        };
        let heatmapOptions:any = {
            radius: config.blurRadius
        };
        let field = config.field;
        let heatmapData:any = [];

        try {

            if (field) {
                legendInfo.min = legendInfo.max = features[0].businessData[field];
            }

            features.forEach((feature) => {
                let geometry = feature.geometry;

                if (geometry.type === 'Point') {
                    let coord = geometry.coordinates;

                    if (field) {

                        let fieldValue = feature.businessData[field];

                        if (legendInfo.min > fieldValue) {
                            legendInfo.min = fieldValue;
                        }

                        if (legendInfo.max < fieldValue) {
                            legendInfo.max = fieldValue;
                        }

                        heatmapData.push([coord[1], coord[0], fieldValue]);
                    } else {
                        heatmapData.push([coord[1], coord[0]]);
                    }
                }
            });

            if (legendInfo.max) {
                heatmapOptions.max = legendInfo.max;
            } else {
                heatmapOptions.max = 1;
                legendInfo.min = 1;
                legendInfo.max = features.length;
            }

            let gradient = getGradient(colors, heatmapOptions.max);

            if (gradient) {
                heatmapOptions.gradient = gradient;
            }

            self.postMessage({data: heatmapData, options: heatmapOptions, legendInfo: legendInfo});

        } catch (error) {
            console.log('ERROR: worker/geojson-to-heatmap', error.message);
            throw error;
        }
    };

    function getGradient(colors, maxValue) {

        if (!colors || !colors.length) {
            return;
        }
        let length = colors.length;
        let interval = maxValue / length;
        let current = interval / maxValue;

        let gradient = {};

        for (let i = 0; i < length - 1; i++) {
            gradient[String(current)] = colors[i];
            current += interval;
        }

        gradient[String(1)] = colors[length - 1];
        return gradient;
    }

};