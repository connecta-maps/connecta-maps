import {basemaps} from "./basemaps.config";
const fs = require('fs');
const path = require('path');
let messagePtBr = require("./i18n/pt-br/message");
let messageEnUs = require("./i18n/message");
let ConfigAll = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, '../', '../', 'Config.json')).toString()
);

const Config:any = {

    /**
     * Mapeamento das linguagens disponíveis no servidor.
     * @type {Object}
     */
    messages : {
        'pt-br' : messagePtBr,
        'en-us' : messageEnUs
    },

    /**
     * Lista de mapa base padrão da aplicação.
     * @type {Array}
     */
    basemaps : basemaps(ConfigAll),

    externalModule : ConfigAll.serverInject && ConfigAll.serverInject.module ? Object.keys(ConfigAll.serverInject.module).map(moduleName =>
        $require(path.resolve(__dirname, '../', '../', ConfigAll.serverInject.module[moduleName]))
    ) : []

};

export default Config;
Object.assign(Config, ConfigAll.server);
Object.freeze(Config);

function $require(pathModule) {
    console.log('Inject external module: ', pathModule);
    return require(pathModule);
}