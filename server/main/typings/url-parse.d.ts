
declare module "url-parse" {

    interface ObjectURL{
        hostname:string;
        port:number;
        pathname:string;
    }

    function urlParse(url:string, opt:boolean):ObjectURL;
    
    namespace urlParse {}
    
    export = urlParse;
}
