import {IModule} from "../_base/IModule";
import * as FS from "fs";
import * as PATH from "path";

export class App implements IModule {

    prepare():void {
        let directories = FS.readdirSync(PATH.resolve(__dirname));
        let configs = [];
        let pattern = /\-module/;

        for (let file of directories) {
            if (pattern.test(file)) {
                let module = require(PATH.resolve(__dirname, file, 'config.js'));
                configs.push(module[file.replace(pattern, '').replace(/\-(.)/g, (v, v1) => { return v1.toUpperCase(); } ) + 'Config']);
            }
        }

        for (let j in configs) {
            for (let verb in configs[j]) {
                let routes = configs[j][verb];

                for (let n in routes) {
                    server.putRoute(verb, routes[n].route, routes[n].execute);
                }
            }
        }
    }

}
