import {SpatialDataSource} from "../../data/schema/SpatialDataSource";
import {ServerException} from "../../global/ServerException";
import {Layer} from "../../data/schema/Layer";
import {SpatialDataSourceController as SpatialDataSourceControllerADMIN} from "../admin/controller/SpatialDataSourceController";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";

export class SpatialDataSourceController {

    constructor(public request) {}

    @EndPoint({
        verb : HttpVerb.POST,
        route : '/app/spatial-data-source',
        response : {},
        params : {
            spatialDataSource : {
                type : 'object',
                required : true
            }
        }
    })
    saveSpatialDataSource(params) {
        return new Promise((resolve, reject) => {
            try {
                let {
                    spatialDataSource
                } = params;

                if (!spatialDataSource || typeof spatialDataSource !== 'object') {
                    throw new ServerException('RES_ERROR_06', ['spatialDataSource']);
                }

                SpatialDataSourceControllerADMIN.saveAllLayersFromSpatialDataSource(spatialDataSource, this.request)
                    .then((result) => {
                        resolve(result);
                    })
                    .catch(error => reject(error));

            } catch (error) {
                reject(error);
            }
        });
    }

    @EndPoint({
        verb : HttpVerb.DELETE,
        route : '/app/spatial-data-source/:id',
        response : {}
    })
    removeSpatialDataSource(params) {
        return new Promise((resolve, reject) => {
            try {
                let {id} = params;

                SpatialDataSource.Model.findByIdAndRemove(id, (error, result) => {
                    if (error) {
                        reject(error);
                    } else {
                        if (result) {
                            Layer.Model.remove({ spatialDataSourceId : result._id }, (error) => {
                                if (error) {
                                    reject(new ServerException('RES_ERROR_21'));
                                } else {
                                    resolve(result);
                                }
                            });
                        } else {
                            reject(new ServerException('RES_ERROR_21'));
                        }
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    }

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/app/spatial-data-source',
        response : {}
    })
    getSpatialDataSourceLayers(params) {
        return new Promise((resolve, reject) => {
            try {
                let conditional = {};

                if (this.request.headers['c-maps-domain-id']) {
                    conditional['domainId'] = this.request.headers['c-maps-domain-id'];
                }

                let promise: any = SpatialDataSource.Model.find(conditional, {
                    title: 1,
                    id: 1,
                    serverType : 1,
                    dsn : 1
                });
                promise.catch(error => reject(error));
                promise.then((spatialDataSources) => {
                    resolve(spatialDataSources);
                });
            }catch(error){
                reject(error);
            }
        });
    }

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/app/spatial-data-source/:id/layers',
        response : {}
    })
    findLayersBySpatialDataSource(params){
        return new Promise((resolve, reject) => {
            try {
                Layer.Model.find({spatialDataSourceId: params.id}, {info: 0})
                    .then(resolve)
                    .catch(reject);
            } catch (error) {
                reject(error);
            }
        });
    }
}