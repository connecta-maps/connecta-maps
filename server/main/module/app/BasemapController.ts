import {Sender} from "../../middleware/response/Sender";
import Config from "../../Config";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";

export class BasemapController {

    @EndPoint({
        route : '/app/basemaps',
        response : {},
        verb:HttpVerb.GET
    })
    getDefaultBasemaps = (params) => {
        return Promise.resolve(Config.basemaps);
    };

}