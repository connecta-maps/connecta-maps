import {DataSourceServiceLoader} from "../datasource-service/helper/DataSourceServiceLoader";
import {ServerException} from "../../global/ServerException";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";

export class DataSourceController {

    @EndPoint({
        route : '/app/data-source',
        verb : HttpVerb.POST,
        response : {},
        params : {
            project : {
                type : 'object',
                required : true
            },
            filterConfig : {
                type : 'array',
                required : false
            },
            richLayer : {
                type : 'object',
                required : true
            }
        }
    })
    getResultSet = (params) => {
        return new Promise((resolve, reject) => {
            try {

                let {project, filterConfig, richLayer}:any = params;

                let serviceType = richLayer.layer.serviceType || project.serviceType;

                DataSourceServiceLoader.loadAndExecute(
                    serviceType as any,
                    'getResultSet',
                    { richLayer, project, filterConfig }
                )
                    .then(resolve, reject)
                    .catch(reject);

            } catch (error) {
                reject(error);
            }
        });
    };

}