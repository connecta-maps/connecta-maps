import * as mongoose from "mongoose";
import {ServerException} from "../../../global/ServerException";

export abstract class ControllerBase {

    static requestMapping: any;

    domainId:string;

    constructor(protected _request, protected _response) {
        this.domainId = this._request.headers['c-maps-domain-id'];

         if (!this.domainId) {
             throw new ServerException('RES_ERROR_06', ['domainId']);
         }
    }

    /**
     *
     * @param id
     * @param model
     * @returns {Promise<T>}
     * @private
     */
    protected _getDocument(id: string, model: mongoose.Model<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                if (!id) {
                    throw new ServerException('RES_ERROR_06', ['id']);
                }

                let promise: any = model.findOne({_id: id, domainId: this.domainId});
                promise.catch(reject);
                promise.then((doc: mongoose.Document) => {
                    try {
                        if (!doc) {
                            throw new ServerException('RES_ERROR_12', [id]);
                        }
                        resolve(doc);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     *
     * @param {String} id
     * @param {Object} params
     * @param {mongoose.Model} model
     * @returns {Promise<any>}
     * @private
     */
    protected _updateDocument(id: string, params: any, model: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                if (!id) {
                    throw new ServerException('RES_ERROR_06', ['id']);
                }

                model.findOne({_id: id, domainId: this.domainId}, (error, doc) => {
                    try {
                        if (error) {
                            throw error;
                        }

                        if (!doc) {
                            throw new ServerException('RES_ERROR_12', [id]);
                        }

                        if (params._id) {
                            delete params._id;
                        }

                        doc.update({$set: params}, (error) => {
                            try {
                                if (error) {
                                    throw error;
                                }

                                model.findOne({_id: id}, (error, doc) => {
                                    if (error) {
                                        reject(error);
                                    } else {
                                        resolve(doc);
                                    }
                                })

                            } catch (error) {
                                reject(error);
                            }
                        });
                    } catch (error) {
                        reject(error);
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     *
     * @param params
     * @param model
     * @returns {Promise<T>}
     * @private
     */
    protected _saveDocument(params: any, model: mongoose.Model<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                params['domainId'] = this.domainId;

                let doc: mongoose.Document = new model(params);
                doc.save((err: any, doc: mongoose.Document) => {
                    try {
                        if (err) {
                            reject(err);
                        }
                        resolve(doc);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     *
     * @param id
     * @param model
     * @returns {Promise<T>}
     * @private
     */
    protected _deleteDocument(id: string, model: mongoose.Model<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                if (!id) {
                    throw new ServerException('RES_ERROR_06', ['id']);
                }

                let promise: any = model.findOne({_id: id, domainId: this.domainId});
                promise.catch(reject);
                promise.then((doc: mongoose.Document) => {
                    try {
                        if (!doc) {
                            throw new ServerException('RES_ERROR_12', [id]);
                        }

                        doc.remove(err => {
                            if (err) {
                                reject(err);
                            }
                            resolve(doc);
                        });
                    } catch (err) {
                        reject(err);
                    }

                });
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     *
     * @param {Object} params
     * @returns {Promise<ResultListPagination>}
     * @private
     */
    protected _getDocumentsPaged(params: {
        model: mongoose.Model<any>;
        page: any;
        size: any;
        filter?: any;
        outFields?: any;
    }): Promise<ResultListPagination> {
        return new Promise<ResultListPagination>((resolve, reject) => {
            try {

                params.filter = params.filter ? JSON.parse(params.filter) : {};
                params.outFields = params.outFields || {};

                if (params.size != '*') {
                    params.size = Number(params.size === void 0 ? 10 : params.size);
                }
                if (params.page != '*') {
                    params.page = Number(params.page === void 0 ? 1 : params.page);
                }

                let skip = params.size * (params.page - 1);

                //Valida se o filtro é um objeto puro
                if (params.filter.__proto__ === Object.prototype) {
                    params.filter['domainId'] = this.domainId;
                }

                let promises = [];

                if (params.size != '*' && params.page != '*') {
                    promises.push(findWithPromise(params.model.find(params.filter, params.outFields).skip(skip).limit(params.size)));
                } else {
                    promises.push(findWithPromise(params.model.find(params.filter, params.outFields)));
                }

                promises.push(findWithPromise(params.model.find(params.filter).count()));

                let promise = Promise.all(promises);
                promise.catch(reject);
                promise.then((resultFind: any[]) => {
                    try {
                        let [docs, totalDocuments] = resultFind;

                        let result: ResultListPagination = {
                            size: params.size,
                            currentPage: params.page,
                            totalDocuments: totalDocuments as number,
                            content: docs as mongoose.Document[]
                        };

                        resolve(result);

                    } catch (error) {
                        reject(error);
                    }
                });

            } catch (error) {
                reject(error);
            }

            function findWithPromise(query) {
                return new Promise((resolve, reject) => {
                    try {
                        query.exec((error, docs) => {
                            try {
                                if (error) {
                                    throw error;
                                }
                                resolve(docs);
                            } catch (error) {
                                reject(error);
                            }
                        });
                    } catch (error) {
                        reject(error);
                    }
                });
            }
        });
    }

}

export interface ResultListPagination {

    currentPage: number;
    totalDocuments: number;
    content: mongoose.Document[];
    size: number;

}