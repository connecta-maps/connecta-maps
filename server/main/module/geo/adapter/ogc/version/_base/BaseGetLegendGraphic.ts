import {FormatImage} from "../../../../../../enum/FormatImage";
import {LayerSchema} from "../../../../../../data/schema/Layer";

export class BaseGetLegendGraphic {

    toParams(params:IGetLegendGraphic, layer:LayerSchema) {
        let parameters:any = {};
        parameters.request = 'GetLegendGraphic';
        parameters.version = params.version;
        parameters.format = 'image/' + FormatImage.PNG;
        parameters.width = params.width;
        parameters.height = params.height;
        parameters.layer = layer.layerIdentifier;
        return parameters;
    }

}

export interface IGetLegendGraphic {
    request:string;
    version:string;
    format:string;
    layer:string;
    height: any;
    width: any;
}
