import {IOperationParams} from "../../../../operation/_base/OperationBase";

export interface IVersion {
    
    toParams(spatialDataSource:any, layer:any, params:IOperationParams):any;
    
}