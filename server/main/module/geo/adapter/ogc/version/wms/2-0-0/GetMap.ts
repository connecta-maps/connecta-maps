import {GetMap as GetMap1_3_0, IGetMap as IGetMap1_3_0} from "../1-1-1/GetMap";

export class GetMap extends GetMap1_3_0 {

    version:string = '2.0.0';

}

export interface IGetMap extends IGetMap1_3_0 {

}
