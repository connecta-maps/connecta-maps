

import {GetMap as GetMap1_0_0, IGetMap as IGetMap1_0_0} from "../1-0-0/GetMap";

/**
 * @class
 * @classdesc Classe responsável pelo mapeamento dos parâmetros de requisição da operação GetMap do WMS-OGC
 * na versão 1.1.0
 */
export class GetMap extends GetMap1_0_0 {

    version:string = '1.1.1';

}

export interface IGetMap extends IGetMap1_0_0 {

    crs:string;

}