import {IVersion} from "../../_base/IVersion";
import {QueryParams} from "../../../../../operation/QueryOperation";
import {ArcGISSpatialRel} from "../../../../../../../enum/ArcGISEnum";
import {OGCServiceType, OGCSpatialRel, OGCOperationName} from "../../../../../../../enum/OGCEnum";
import {OGCParamsVersionBase} from "../../_base/OGCParamsVersionBase";

export class GetFeature implements IVersion {

    version:string = '1.0.0';

    toParams(spatialDataSource:any, layer:any, params:QueryParams|any):any {
        let parameters:GetFeatureParams = new GetFeatureParams(spatialDataSource, layer);
        // parameters.featureID = params;
        parameters.cql_filter = params;
        parameters.maxFeatures = params;
        parameters.propertyName = params;
        parameters.sortBy = params;
        parameters.srsName = params;
        parameters.typeNames = params as any;
        parameters.version = this.version;
        delete parameters.spatialDataSource;
        delete parameters.layer;
        return OGCParamsVersionBase.toObject(parameters);
    }

    toResponse(result) {
        let queryResult:any = {
            features  : []
        };
        
        for (let feature of result.features) {
            queryResult.features.push({
                'attributes' : feature['properties'],
                'geometry' : feature['geometry']
            });
        }
        return queryResult;
    }

}

export class GetFeatureParams extends OGCParamsVersionBase {

    request:OGCOperationName = OGCOperationName.GET_FEATURE;
    service:OGCServiceType = OGCServiceType.WFS;
    version:string = '1.0.0';
    outputFormat:string = 'application/json';

    private _maxFeatures:number;

    private _featureID:string;

    private _sortBy:string;

    private _propertyName:string;

    private _srsName:string;

    private _typeNames:string;

    private _cql_filter:string;

    constructor(public spatialDataSource, public layer) {
        super();
    }

    set cql_filter(params: any) {
        let geometryField = this.layer.geometryField;
        let filter:Array<string> = [];
        let spatialRel;

        if (params.where) {

            let pattern = /(?:\s?)(\w+)\s*=\s+'(\d{4}-\d{2}-\d{2})'/g;

            params.where.replace(pattern, function () {
                let fullMatch = arguments[4];
                let field = arguments[1];
                let date = arguments[2];
                params.where = params.where.replace(fullMatch, field + " >= '" + date + " 00:00:00' and " + field +  " <= '" + date + " 23:59:59'");
            });

            filter.push(params.where);
        }

        if (params.geometry) {
            let paramsSpatialRel = params.spatialRel,
                mappingGeometryType,
                geometry;

            mappingGeometryType = {
                [ArcGISSpatialRel.CONTAIN] : OGCSpatialRel.CONTAIN,
                [ArcGISSpatialRel.CROSSES] : OGCSpatialRel.CROSSES,
                [ArcGISSpatialRel.ENVELOPE] : OGCSpatialRel.ENVELOPE,
                [ArcGISSpatialRel.INTERSECT] : OGCSpatialRel.INTERSECT,
                [ArcGISSpatialRel.OVERLAP] : OGCSpatialRel.OVERLAP,
                [ArcGISSpatialRel.WITHIN] : OGCSpatialRel.WITHIN,
                [ArcGISSpatialRel.TOUCH] : OGCSpatialRel.TOUCH
            };

            geometry = params.geometry;
            spatialRel = mappingGeometryType[paramsSpatialRel];

            if (geometry.x && geometry.y) {
                filter.push(spatialRel + '(' + geometryField +
                    ', POINT(' + geometry.y + ' ' + geometry.x + '))');
            }

            if (geometry.paths) {
                let lines = [];
                geometry.paths.forEach(path => {
                    let line = [];
                    path.forEach(_line => {
                        line.push(_line[0] + ' ' + _line[1]);
                    });
                    lines.push('(' + line + ')');
                });
                filter.push(spatialRel + '(' + geometryField +
                    ', MULTILINESTRING(' + lines.join(',') + '))');
            }

            if (geometry.center) {
                // DISJOINT(buffer(the_geom, 10) , POINT(1 2))
                filter.push(`${spatialRel}(${geometryField}, BUFFER(POINT(${geometry.center.y} ${geometry.center.x}), ${geometry.radius}))`);
                // filter.push(`${spatialRel}(BUFFER(${geometryField}, ${geometry.radius/1000}), POINT(${geometry.center.x} ${geometry.center.y}))`);
            } else if (geometry.rings) {
                let rings = [];
                geometry.rings.forEach(ring => {
                    let polygon = [];
                    ring.forEach(_polygon => {
                        polygon.push(_polygon[0] + ' ' + _polygon[1]);
                    });
                    rings.push('(' + polygon + ')');
                });
                filter.push(spatialRel + '(' + geometryField +
                    ', POLYGON(' + rings + '))');
            }

            if (geometry.xmin && geometry.ymin && geometry.xmax && geometry.ymax) {

                if (spatialRel === OGCSpatialRel.ENVELOPE) {
                    filter.push(spatialRel + '(' + geometryField + ', ' + geometry.xmin + ', ' +
                        geometry.ymin + ', ' + geometry.xmax + ', ' + geometry.ymax + ')');
                } else {
                    filter.push(spatialRel + '(' + geometryField + ', POLYGON((' +
                        geometry.xmin + ' ' + geometry.ymin + ', ' +
                        geometry.xmin + ' ' + geometry.ymax + ', ' +
                        geometry.xmax + ' ' + geometry.ymax + ', ' +
                        geometry.xmax + ' ' + geometry.ymin + ', ' +
                        geometry.xmin + ' ' + geometry.ymin + ')))');
                }
            }

        }
        let cql_filter = filter.join(' AND ');
        if (cql_filter) {
            this._cql_filter = cql_filter;
        }
    }

    get cql_filter() {
        return this._cql_filter;
    }

    get maxFeatures(): any {
        return this._maxFeatures;
    }

    set maxFeatures(value:any) {
        this._maxFeatures = value.resultRecordCount;
    }

    get featureID(): string {
        return this._featureID;
    }

    set featureID(params: string) {
        this._featureID = params;
    }

    get sortBy(): any {
        return this._sortBy;
    }

    set sortBy(params:any) {
        this._sortBy = params.orderByFields;
    }

    get propertyName(): any {
        return this._propertyName;
    }

    set propertyName(params:any) {
        this._propertyName = params.outFields ? params.outFields.join(',') : '';
    }

    get srsName(): any {
        return this._srsName;
    }

    set srsName(params:any) {
        this._srsName = String(params.inSR);
        let sr = params.outSR === '102100' || params.outSR === 102100 ? ' ' : params.outSR;
        sr = (/\:/.test(sr) ?  '' : 'EPSG:') + (sr || '3857');
        this._srsName = sr;
    }

    get typeNames(): string {
        return this._typeNames;
    }

    set typeNames(params:string) {
        this._typeNames = this.layer.layerIdentifier;
    }
}
