import {GetFeature as GetFeature1_1_0} from "../1-1-0/GetFeature";
import {OGCParamsVersionBase} from "../../_base/OGCParamsVersionBase";

export class GetFeature extends GetFeature1_1_0 {

    version:string = '2.0.0';

    toParams(...args) {
        let params = super.toParams.apply(this, args);
        params.count = params.maxFeatures;
        delete params.maxFeatures;
        return OGCParamsVersionBase.toObject(params);
    }

}
