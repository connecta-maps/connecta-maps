import {IAdapter} from "../_base/IAdapter";
import {SpatialDataSource, SpatialDataSourceSchema} from "../../../../data/schema/SpatialDataSource";
import {QueryParams} from "../../operation/QueryOperation";
import {GetMap as GetMap1_0_0} from "./version/wms/1-0-0/GetMap";
import {GetMap as GetMap1_1_0} from "./version/wms/1-1-1/GetMap";
import {GetMap as GetMap1_3_0} from "./version/wms/1-3-0/GetMap";
import {GetMap as GetMap2_0_0} from "./version/wms/2-0-0/GetMap";
import {OGCOperationName, OGCServiceType} from "../../../../enum/OGCEnum";
import * as Q from "q";
import {Request} from "../../../../helper/Request";
import {ServerException} from "../../../../global/ServerException";
import {GetFeature as GetFeature1_0_0} from "./version/wfs/1-0-0/GetFeature";
import {GetFeature as GetFeature1_1_0} from "./version/wfs/1-1-0/GetFeature";
import {GetFeature as GetFeature2_0_0} from "./version/wfs/2-0-0/GetFeature";
import {ArcGISGeometryType, ArcGISTypeField} from "../../../../enum/ArcGISEnum";
import {ISpatialDataSourceParams} from "../../../admin/controller/SpatialDataSourceController";
import {IField, Layer, LayerSchema} from "../../../../data/schema/Layer";
import {GetLegendGraphic1_0_0} from "./version/wms/1-0-0/GetLegendGraphic1_0_0";
import {GetLegendGraphic1_1_0} from "./version/wms/1-1-1/GetLegendGraphic1_1_0";
import {GetLegendGraphic1_3_0} from "./version/wms/1-3-0/GetLegendGraphic1_3_0";
import {GetLegendGraphic2_0_0} from "./version/wms/2-0-0/GetLegendGraphic2_0_0";
import {Converter} from "../../../../util/Converter";
import {GeoCacheOperationName} from "../../../../enum/GeoCacheOperationName";
import {ResponseFormat} from "../../../../enum/ResponseFormat";
import {ProjectSchema, RichLayer} from "../../../../data/schema/Project";
import {DataSourceServiceLoader} from "../../../datasource-service/helper/DataSourceServiceLoader";
import WorkerHelper from "../../../../helper/WorkerHelper";
import Config from "../../../../Config";
import parseXML = require("xml2js");

let versions = {

    'wms' : {
        '1.0.0' : {
            'GetMap' : GetMap1_0_0,
            'GetLegendGraphic' : GetLegendGraphic1_0_0
        },
        '1.1.1' : {
            'GetMap' : GetMap1_1_0,
            'GetLegendGraphic' : GetLegendGraphic1_1_0
        },
        '1.3.0' : {
            'GetMap' : GetMap1_3_0,
            'GetLegendGraphic' : GetLegendGraphic1_3_0
        },
        '2.0.0': {
            'GetMap' : GetMap2_0_0,
            'GetLegendGraphic' : GetLegendGraphic2_0_0
        }

    },
    'wfs' : {
        '1.0.0' : {
            'GetFeature' : GetFeature1_0_0
        },
        '1.1.0' : {
            'GetFeature' : GetFeature1_1_0
        },
        '2.0.0' : {
            'GetFeature' : GetFeature2_0_0
        }
    }

};

/**
 * @class
 * @classdesc Classe responsável por abstrair as implementações da aplicação moldando-as de acordo com a padronização OGC.
 */
export class OGCAdapter implements IAdapter {

    /**
     * Classe de mapemanto dos parâmetros que será usada para consumir a operação externa.
     * @type {Function}
     * @private
     */
    private _ClassVersion:any;

    /**
     *
     * @param [request]
     * @ignore
     */
    constructor(private request){}

    /**
     * Método responsável por preparar a classe de mapeamento que será usada no consumo externo.
     * @param ClassVersion
     */
    prepare(ClassVersion:any) {
        this._ClassVersion = ClassVersion;
    }

    testConnection(spatialDataSource: SpatialDataSourceSchema): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }

    getLayers(spatialDataSource: SpatialDataSourceSchema): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let parametersDescribeFeatureType = {
                    service: OGCServiceType.WFS,
                    request: OGCOperationName.DESCRIBE_FEATURE_TYPE,
                    outputFormat: 'application/json'
                };

                Request.post(spatialDataSource.dsn, parametersDescribeFeatureType, {returnJSON: true})
                    .then(([resultDescribeFeatureType]) => {
                        resultDescribeFeatureType = JSON.parse(resultDescribeFeatureType);

                        return new Promise((resolve, reject) => {
                            let mappingFeatureTypes = {};

                            Layer.Model.find({ spatialDataSourceId : spatialDataSource._id.toString() }, (error, docs) => {
                                if (error) {
                                    reject(error);
                                } else {
                                    for (let featureType of resultDescribeFeatureType.featureTypes) {
                                        for (let i in docs) {
                                            let layerDoc = docs[i];
                                            if (layerDoc.title === featureType.typeName) {
                                                mappingFeatureTypes[featureType.typeName] = layerDoc;
                                                docs.splice(Number(i), 1);
                                                break;
                                            }
                                        }
                                    }

                                    resolve([resultDescribeFeatureType, mappingFeatureTypes]);
                                }
                            });
                        });
                    })
                    .then(([resultDescribeFeatureType, mappingFeatureTypes]) => {
                        let layers: any[] = [];
                        let current = 0;
                        let limit = 20;
                        let _results = [];

                        let exec = (featureTypes) => {
                            current += limit;
                            let promises:any[] = [];

                            featureTypes.forEach((featureType) => {
                                let geometryField = featureType.properties.filter((property) => /gml/.test(property.type))[0];

                                let layerDoc = mappingFeatureTypes[featureType.typeName];

                                if (!layerDoc) {
                                    promises.push(Request.post(spatialDataSource.dsn, {
                                        request : 'GetFeature',
                                        service : 'WFS',
                                        version : '2.0.0',
                                        outputFormat:'application/json',
                                        typeName: spatialDataSource.info.title + ':' + featureType.typeName,
                                        count : 1
                                    }, { returnJSON : true }));
                                }

                                let properties = {};

                                featureType.properties.forEach((property) => {
                                    properties[property.name] = property.type;
                                });

                                layers.push(Object.assign({}, layerDoc ? layerDoc.toObject() : {}, {
                                    layerIdentifier: resultDescribeFeatureType.targetPrefix + ':' +  featureType.typeName,
                                    title: featureType.typeName,
                                    name: featureType.typeName,
                                    spatialDataSourceId: spatialDataSource._id,
                                    info : {
                                        wfs : {
                                            currentVersion : '2.0.0'
                                        }
                                    },
                                    geometryField : geometryField.name,
                                    isVector : true,
                                    capabilities : [GeoCacheOperationName.QUERY, GeoCacheOperationName.GET_MAP],
                                    geoCache : {
                                        queryCache : true
                                    },
                                    layerFields : OGCAdapter.buildFields(properties)
                                }));
                            });

                            return Promise.all(promises)
                                .then(resultsGetFeatures => {
                                    _results = _results.concat(resultsGetFeatures.map((result) =>
                                        result instanceof Array ? result[0] : result));

                                    if (current < resultDescribeFeatureType.featureTypes.length) {

                                        return exec(resultDescribeFeatureType.featureTypes.slice(current, current + limit));

                                    } else {
                                        return {layers, results : _results};
                                    }
                                })
                        };

                        return exec(resultDescribeFeatureType.featureTypes.slice(current, limit));
                    })
                    .then(({layers, results}:any) => {
                        let fullLayers = [];
                        let layersNoSaved = [];

                        layers.forEach((layer) => {
                            if (layer._id) {
                                fullLayers.push(layer);
                            } else {
                                layersNoSaved.push(layer);
                            }
                        });

                        for (let i in results) {
                            let result;
                            try {
                                result = JSON.parse(results[i]);
                            } catch (error) {
                                console.warn('Não pode construir a camada: ', layersNoSaved[i].title);
                            } finally {
                                if (result && result.features.length && layersNoSaved[i]) {
                                    layersNoSaved[i].geometryType = OGCAdapter.getGeometryType(result.features[0].geometry.type);
                                    fullLayers.push(layersNoSaved[i]);
                                }
                            }
                        }

                        resolve(fullLayers);
                    })
                    .catch(reject)
            } catch (err) {
                reject(err);
            }
        });
    }

    buildLayer(spatialDataSource: SpatialDataSourceSchema, params: LayerSchema): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let layer: any = params;
                let parametersGetCapabilities = {
                    service : OGCServiceType.WFS,
                    request : OGCOperationName.GET_CAPABILITIES
                };

                let promise = Request.post(spatialDataSource.dsn, parametersGetCapabilities, {returnJSON : false, isString: true});
                promise.catch(reject);
                promise.then(([getCapabilitiesResultXML, response]) => {
                    try {
                        parseXML.parseString(getCapabilitiesResultXML, (error, getCapabilitiesResult) => {
                            try {
                                if (error) {
                                    throw error;
                                }

                                let currentVersion = getCapabilitiesResult[Object.keys(getCapabilitiesResult)[0]].$.version;

                                layer.info = layer.info || {};
                                layer.info.wfs = {
                                    currentVersion: currentVersion
                                };

                                let parametersGetFeature: any = {
                                    service: OGCServiceType.WFS,
                                    request: OGCOperationName.GET_FEATURE,
                                    outputFormat: 'application/json',
                                    typeNames: params.layerIdentifier,
                                    version: currentVersion
                                };

                                if (currentVersion === '2.0.0') {
                                    parametersGetFeature.count = 1;
                                } else {
                                    parametersGetFeature.maxFeatures = 1;
                                }

                                let promise = Request.post(spatialDataSource.dsn, parametersGetFeature,  {returnJSON : false, isString: true});
                                promise.catch(reject);
                                promise.then(([getFeaturesResult, response]) => {
                                    try {
                                        let feature = JSON.parse(getFeaturesResult).features[0];
                                        layer.geometryField = feature.geometry_name;
                                        layer.isVector = true;
                                        layer.geometryType = OGCAdapter.getGeometryType(feature.geometry.type);
                                        layer.layerFields = OGCAdapter.buildFields(feature.properties);
                                        resolve(layer);
                                    } catch (error) {
                                        reject(error);
                                    }
                                });

                            } catch (error) {
                                reject(error);
                            }
                        });
                    } catch (err) {
                        reject(err);
                    }

                });
            } catch (err) {
                reject(err);
            }
        });
    }

    buildSpatialDataSource(params: ISpatialDataSourceParams): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                let spatialDataSource: any = params;
                let pattern = /(https?:\/\/[\w\d\.\:]+\/geoserver)\/([^\/|^\?]+)\/(wms|wfs|ows)/;
                let parts = spatialDataSource.dsn.match(pattern);

                if (!parts) {
                    throw new ServerException('RES_ERROR_10', ['dsn']);
                }

                spatialDataSource.dsn = parts[1] + '/' + parts[2] + '/wfs';
                spatialDataSource.info = {
                    title : parts[2]
                };

                let parametersGetCapabilities = {
                    service : OGCServiceType.WFS,
                    request : OGCOperationName.GET_CAPABILITIES
                };

                Request.post(spatialDataSource.dsn, parametersGetCapabilities,
                    {returnJSON : false, isString: true})
                    .then(([getCapabilitiesResultXML, response]) => {
                        return new Promise<any>((resolve) => {
                            try {
                                parseXML.parseString(getCapabilitiesResultXML, (error, json) => {
                                    if (error) {
                                        reject(error);
                                    } else {
                                        resolve(json);
                                    }
                                });
                            } catch (error) {
                                reject(error);
                            }
                        });
                    })
                    .then((getCapabilitiesResult) => {
                        let currentVersion = getCapabilitiesResult[Object.keys(getCapabilitiesResult)[0]].$.version;
                        spatialDataSource.info.wfs = { currentVersion };
                        return spatialDataSource;
                    })
                    .then(spatialDataSource => {
                        if (spatialDataSource._id) {
                            return spatialDataSource;
                        }
                        return Promise.resolve(new SpatialDataSource.Model(spatialDataSource));
                    })
                    .then((spatialDataSource:any) => {
                        this.getLayers(spatialDataSource)
                            .then(layers => {
                                resolve({
                                    spatialDataSource,
                                    layers
                                });
                            })
                            .catch(reject);
                    })
                    .catch(reject);
            } catch (err) {
                reject(err);
            }
        });
    }

    getLegendGraphic(spatialDataSource: SpatialDataSourceSchema, layer: LayerSchema, parameters: any): Q.Promise<any> {
        let deferred = Q.defer();

        let {params} = parameters;

        try {
            let currentVersion = '1.1.1';
            let Class = this.getClassVersion(OGCServiceType.WMS, currentVersion, OGCOperationName.GET_LEGEND_GRAPHIC);
            let paramsInstance = new Class();
            params.version = currentVersion;
            let queryParams = paramsInstance.toParams(params, layer);
            let dsn = layer.info.INDE ? layer.info.dsn.replace(/\?.+/, '') : spatialDataSource.dsn.replace('wfs', 'wms');

            for (let key in queryParams) {
                queryParams[key.toUpperCase()] = queryParams[key];
            }

            let promise = Request.get(dsn.trim(), queryParams, {}, {useNotify: true});

            promise.then((data) => {
                let [result, response] = data;

                try {
                    if (result && /xml/.test(result.toString())) {
                        console.error(result);
                        throw new ServerException('RES_ERROR_08');
                    }

                    deferred.resolve(data);
                } catch (error) {
                    deferred.reject(error);
                }
            }, deferred.reject, deferred.notify);
        }catch (error) {
            console.error(error);
            deferred.reject(error);
        }

        return deferred.promise;
    }

    getMap(spatialDataSource: SpatialDataSourceSchema, layer: LayerSchema, parameters: any): Q.Promise<any> {
        let deferred = Q.defer();

        let {params} = parameters;

        try {
            let currentVersion = '1.1.1';
            let Class = this.getClassVersion(OGCServiceType.WMS, currentVersion, OGCOperationName.GET_MAP);
            let paramsInstance = new Class();
            params.version = currentVersion;
            let queryParams = paramsInstance.toParams(params, layer);
            let dsn = layer.info.INDE ? layer.info.dsn.replace(/\?.+/, '') : spatialDataSource.dsn.replace('wfs', 'wms');

            queryParams.styles = "";

            let promise = Request.get(dsn.trim(), queryParams, {}, {useNotify: true});
            promise.then((data) => {
                let [result, response] = data;

                try {
                    if (result && /xml/.test(result.toString())) {
                        console.error(result);
                        throw new ServerException('RES_ERROR_08');
                    }

                    deferred.resolve(data);
                } catch (error) {
                    deferred.reject(error);
                }
            }, deferred.reject, deferred.notify);
        }catch (error) {
            console.error(error);
            deferred.reject(error);
        }

        return deferred.promise;
    }

    query(spatialDataSource: SpatialDataSourceSchema, layer: LayerSchema, queryParams: QueryParams = {} as any): Q.Promise<any> {
        let deferred = Q.defer();
        let project:ProjectSchema;
        let filterConfig:any[];
        let serviceType = '';

        try {
            project = queryParams.project;
            filterConfig = queryParams.filterConfig;

            serviceType = layer.serviceType || (project && project.serviceType);

            if (serviceType === 'geo' && filterConfig && filterConfig.length) {
                let where = queryParams.where;

                if (filterConfig) {
                    where = (where ? ' AND ' : '') + Converter.buildWhereClause(filterConfig);
                }

                queryParams.where = where;
            }

            let currentVersion = this.getCurrentVersion(layer, spatialDataSource);
            let Class = this.getClassVersion(OGCServiceType.WFS, currentVersion, OGCOperationName.GET_FEATURE);
            let paramsInstance = new Class();
            let parameters = paramsInstance.toParams(spatialDataSource, layer, queryParams);
            let dsn = spatialDataSource.dsn.replace(/\/(wms|wfs|ows)$\/?/, '') + '/wfs';

            queryParams.f = queryParams.f || ResponseFormat.JSON;
            parameters.outputFormat = OGCReponseFormat[queryParams.f];

            if (queryParams.f === ResponseFormat.JSON) {
                Request.post(dsn, parameters, {returnJSON : true})
                    .then(callbackGetFeature.bind(this), deferred.reject);
            } else if (queryParams.f === ResponseFormat.SHAPE) {
                Request.post(dsn, parameters, {useNotify : true})
                    .then(deferred.resolve, deferred.reject, deferred.notify);
            } else {
                Request.get(dsn, parameters, {},  {useNotify : true})
                    .then(deferred.resolve, deferred.reject, deferred.notify);
            }
        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;

        function callbackGetFeature([result, response]) {
            try {

                let str = result;

                if (str && /xml/.test(str)) {
                    console.error(str);
                    throw new ServerException('RES_ERROR_08');
                }

                let geoJSON:any = JSON.parse(str);

                if (queryParams.returnGeometry === false) {
                    for (let feature of geoJSON.features) {
                        delete feature.geometry;
                    }
                }

                geoJSON.fields = layer.layerFields;
                geoJSON.geometryType = layer.geometryType;

                if (project) {
                    let richLayer:RichLayer = project.richLayers.filter(richLayer => String(richLayer.layer._id) === String(layer['_id'])).pop();
                    DataSourceServiceLoader.loadAndExecute(
                        serviceType as any,
                        'getResultSet',
                        {
                            richLayer,
                            project,
                            filterConfig,
                            geoJSON
                        },
                        true)
                        .then(
                            (result) => {
                                let {businessData} = result;

                                if (result.geoJSON) {
                                    geoJSON = result.geoJSON;
                                }

                                let worker = new WorkerHelper('prepare-features.js');
                                worker.execute({
                                    geoJSON,
                                    businessData,
                                    config : {
                                        richLayer,
                                        href : Config['connecta'].host
                                    }
                                })
                                    .then(deferred.resolve)
                                    .catch(deferred.reject);
                            },
                            deferred.reject,
                            deferred.notify
                        )
                        .catch(deferred.reject);
                } else {
                    deferred.resolve(geoJSON);
                }
            } catch (error) {
                deferred.reject(error);
            }
        }
    }

    /**
     * Método responsável por retornar a classe de mapeamento já carregada.
     * @param {OGCServiceType} serviceName
     * @param {string} version
     * @param {OGCOperationName} operationName
     * @returns {any}
     */
    private getClassVersion(serviceName:OGCServiceType, version:string, operationName:OGCOperationName):any {
        if (this._ClassVersion) {
            return this._ClassVersion;
        }
        return versions[serviceName][version][operationName];
    }

    static buildFields(ogcFields:any):IField[] {
        let fields:IField[] = [];

        for (let field in ogcFields) {
            fields.push({
                name : field,
                type : OGCAdapter.getTypeField(typeof ogcFields[field]),
                alias : field
            });
        }

        fields.push({
            name : '_id',
            type : ArcGISTypeField.STRING,
            alias : 'APP_OBJECT_ID'
        });

        return fields;
    }

    getCurrentVersion(layer: LayerSchema, spatialDataSource: SpatialDataSourceSchema) {
        if(!layer || !spatialDataSource){
            return;
        }

        let currentVersion;
        if (layer.info && layer.info.wfs && layer.info.wfs.currentVersion) {
            currentVersion = layer.info.wfs.currentVersion;
        } else if (spatialDataSource.info && spatialDataSource.info.wfs && spatialDataSource.info.wfs.currentVersion) {
            currentVersion = spatialDataSource.info.wfs.currentVersion;
        } else {
            currentVersion = '1.0.0';
        }

        return currentVersion;
    }

    static getTypeField(type:string):ArcGISTypeField {
        type = type.toLowerCase();

        if (type === 'string') {
            return ArcGISTypeField.STRING;

        } else if (type === 'number') {
            return ArcGISTypeField.DOUBLE;

        } else if (/gml/.test(type)) {
            return ArcGISTypeField.GEOMETRY;

        } else {
            //TODO: rever esta implementação, pois deve tratar melhor o tipo dos campos
            return ArcGISTypeField.STRING;
        }
    }

    /**
     *
     * @param {String} type
     * @returns {ArcGISGeometryType}
     */
    static getGeometryType(type:string):ArcGISGeometryType {
        if (new RegExp('polygon', 'i').test(type.toLowerCase())) {
            return ArcGISGeometryType.POLYGON;

        } else if (new RegExp('point', 'i').test(type.toLowerCase())) {
            return ArcGISGeometryType.POINT;

        } else if (new RegExp('line', 'i').test(type.toLowerCase())) {
            return ArcGISGeometryType.POLYLINE;
        }
    }

}

export interface OGCField{
    name:string;
    maxOccurs: number;
    minOccurs: number;
    nillable: boolean;
    type: string;
    localType: string;
}


export enum OGCReponseFormat {

    SHAPE = <any>"SHAPE-ZIP",
    GEO_JSON = <any>"application/json",
    KML = <any>"application/vnd.google-earth.kml xml",
    CSV = <any>"csv",
    json =  <any>"json"

}
