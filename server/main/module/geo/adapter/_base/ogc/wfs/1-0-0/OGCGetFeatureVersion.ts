
import {OGCServiceType} from "../../../../../../../enum/OGCEnum";
import {OGCOperationName} from "../../../../../../../enum/OGCEnum";

export class OGCGetFeatureParams1_0_0 {

    version : string = '1.0.0';

    service:OGCServiceType = OGCServiceType.WFS;

    request:OGCOperationName = OGCOperationName.GET_FEATURE;

    maxFeatures:number;

    featureID : string;

    sortBy : string;

    propertyName : string;

    bbox : string;

    srsName : string;

}
