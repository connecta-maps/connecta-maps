import * as Q from 'q';

/**
 * @class
 * @classdesc Interface que representa as classes de versão da OGC.
 */
export interface IOGCVersion {

    /**
     * Método responsável por converter os parâmetros da aplicação, para os parâmetros da OGC.
     * Este método deve resolver a promessa com uma instância da classe de mapeamento da operação em que
     * representa.
     * @param {Object} params
     * @returns {Object}
     */
    toParams(params:Object):Object;

    /**
     * Método responsável por converter o resultado da requisição ao serviço externo, para a
     * estrutura da aplicação.
     * @param {*} result
     */
    toResponse(result:any):Q.Promise<any>;

}
