import {AppJSON} from "../../../mapping/AppJSON";
import {GeoJSON} from "../../../mapping/GeoJSON";
import * as async from "async";
import {Field} from "../../../mapping/Field";
import {Feature} from "../../../mapping/Feature";
import {ArcGISTypeField} from "../../../enum/ArcGISEnum";
import {OGCAdapter} from "../adapter/ogc/OGCAdapter";
import * as arcgisUtil from "arcgis-to-geojson-utils";

export abstract class BuilderAppJSON {

    static buildGeometryPolygonMapViewer(geometry:{type:string, coordinates:any[]}):any {
        let {coordinates} = geometry;
        let doRings = (rings, coordinates) => {
            if(coordinates[0] instanceof Array){
                coordinates.reduce(doRings,[]).forEach(ring => rings.push(ring));
            } else {
                rings.push(coordinates.reduce(reduceToRings,[]));
            }
            return rings;
        };

        let reduceToRings = (rings,coordinate) => {
            let lastRing = rings[rings.length-1];
            if (lastRing && lastRing.length != 2) {
                lastRing.push(coordinate);
            } else {
                rings.push([coordinate]);
            }
            return rings;
        };

        if (/point/i.test(geometry.type)) {
            return { x : geometry.coordinates[0], y : geometry.coordinates[1] };
        } else if (/polygon/i.test(geometry.type)) {
            return {
                rings :  coordinates.reduce(doRings, [])
            };
        } else {
            return {
                paths : coordinates.reduce(doRings, [])
            }
        }
    }

    /**
     * Método responsável por converter um GeoJSON para a estrutura de JSON esperada pela aplicação.
     * @param {GeoJSON} geoJSON
     * @param {Object} fns
     * @returns {Promise<AppJSON>}
     */
    static geoJSONToAppJSON(geoJSON:GeoJSON, fns:{convertCoordinates?:(geometry:{coordinates:any[]})=>void} = {}):Promise<AppJSON> {
        return new Promise((resolve, reject) => {
            try {
                if (!geoJSON.features.length) {
                    return resolve({
                        geometryType: '',
                        features : [],
                        fields : [],
                        spatialReference : {}
                    } as any);
                }

                // let fields:Field[] = BuilderAppJSON.buildFieldsFromGeoJSON(geoJSON.features[0]);
                let features:Feature[] = [];

                async.each(geoJSON.features,
                    (featureGeoJSON, done) => {
                        try {
                            if (featureGeoJSON.geometry) {
                                let feature:any = {
                                    attributes : featureGeoJSON.properties,
                                    geometry : fns.convertCoordinates ? fns.convertCoordinates(featureGeoJSON.geometry) : arcgisUtil.geojsonToArcGIS(featureGeoJSON.geometry)
                                };
                                features.push(feature);
                            }
                            done();
                        } catch (error) {
                            done(error);
                        }
                    },
                    error => {
                        try {
                            if (error) {
                                throw error;
                            }

                            let sr:any = {};
                            let srs = geoJSON.srs || geoJSON.crs;

                            if (String(srs) === '3857' || !srs) {
                                sr.wkid = '102100';
                                sr.latestWkid = '3857';

                            } else {
                                sr.wkid = srs;
                            }

                            let result = {
                                geometryType: OGCAdapter.getGeometryType(geoJSON.features[0].geometry.type),
                                features : features,
                                // fields : fields,
                                spatialReference : sr
                            };

                            resolve(result as any);

                        } catch (error) {
                            reject(error);
                        }
                    }
                );


            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * Método responsável por construir as <i>fields</i> na estrutura esperada pela aplicação.
     * @param {{ properties:Object }} featureGeoJSON
     * @returns {Field[]}
     */
    static buildFieldsFromGeoJSON(featureGeoJSON:{properties:any}):Field[] {
        let fields:Field[] = [];

        if (featureGeoJSON && featureGeoJSON.properties) {
            let mapping = {
                'string': ArcGISTypeField.STRING,
                'number': ArcGISTypeField.DOUBLE
            };
            for (let key in featureGeoJSON.properties) {
                if (featureGeoJSON.properties[key] !== '') {
                    fields.push({
                        alias: key,
                        name: key,
                        type: mapping[typeof featureGeoJSON.properties[key]] || ArcGISTypeField.STRING
                    });
                }
            }
        }

        return fields;
    }

}
