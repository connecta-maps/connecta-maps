import {Layer, LayerSchema} from "../../../data/schema/Layer";
import {AdapterLoader} from "../../../loader/AdapterLoader";
import {IAdapter} from "../adapter/_base/IAdapter";
import {ServerException} from "../../../global/ServerException";
import * as Q from 'q';
import * as http from 'http';
import * as mongoose from 'mongoose';
import {Converter} from "../../../util/Converter";
import {GeoCache, GeoCacheSchema} from "../../../data/schema/GeoCache";
import {SpatialDataSourceSchema, SpatialDataSource} from "../../../data/schema/SpatialDataSource";
import {GeoCacheOperationName} from "../../../enum/GeoCacheOperationName";
import {customValidator} from "../../../util/Validator";
import {type} from "os";
import reject = Q.reject;

/**
 * @class
 * @classdesc Classe assistente responsável por prover métodos estáticos que podem ser reutilizados pelas operações
 * do módulo GEO.
 */
export abstract class AdapterHelper {

    /**
     * Método responsável por executar uma operação parametrizada no Adapter respectivo ao SpatialDataSource parametrizado.
     * @param {String|Object} spatialDataSource
     * @param {String} operationName
     * @param {http.ClientRequest} request
     * @param {*} [params]
     * @returns {Promise<T>}
     * @static
     */
    static executeWithSpatialDataSource(spatialDataSource:string|any, operationName:string, request:http.ClientRequest, params?:any):Q.Promise<any> {
        return Q.Promise<any>((resolve, reject, notify) => {
            try {
                if (!spatialDataSource) {
                    throw new ServerException('RES_ERROR_01', ['RES_ERROR_06']);
                }

                if (!operationName) {
                    throw new ServerException('RES_ERROR_01', ['RES_ERROR_06']);
                }

                let execute = (spatialDataSource:SpatialDataSourceSchema) => {
                    try {
                        let serverType = spatialDataSource.serverType;
                        customValidator.verifyExistsNReturn(`module/geo/adapter/${serverType.toLowerCase()}/`, /Adapter\.js$/, /Adapter$/)
                            .catch(reject)
                            .then((Adapter) => {
                                try {
                                    let adapter = new Adapter(request);
                                    let promise = adapter[operationName](spatialDataSource, params);
                                    promise.then(resolve, reject, notify);
                                } catch (error) {
                                    reject(error);
                                }
                            });
                    } catch (error) {
                        reject(error);
                    }
                };

                if (typeof spatialDataSource === 'string') {
                    SpatialDataSource.Model.findById(spatialDataSource, (error, document) => {
                        if (error) {
                            reject(error);
                        } else {
                            execute(document);
                        }
                    });
                } else {
                    execute(spatialDataSource);
                }

            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * Método responsável por executar uma operação parametrizada no Adapter respectivo a Layer parametrizada.
     * @param {String|LayerSchema} layer
     * @param {String} operationName
     * @param {*} [params]
     * @param {http.ClientRequest} [request]
     * @returns {Promise<any>}
     */
    static executeWithLayer(layer:string|any, operationName:string, params?:any, request?:http.ClientRequest):any {
        return Q.Promise<any>((resolve, reject, notify) => {
            try {
                if (!layer) {
                    throw new ServerException('RES_ERROR_01', ['RES_ERROR_06']);
                }

                if (!operationName) {
                    throw new ServerException('RES_ERROR_01', ['RES_ERROR_06']);
                }

                let layerId = typeof layer === 'string' ? layer : layer._id;
                let has = false;
                let collection = server.db.db.collection('geo_cache');
                let hash = AdapterHelper.generateHashToLayer(layerId, operationName, params);
                let cursor = collection.find({hash}).limit(1);

                cursor.on('data', chunk => {
                    has = true;
                    notify([chunk.info, {
                        headers : {
                            'Content-Type' : 'application/json'
                        },
                        statusCode : 200
                    }]);
                });
                cursor.on('end', () => {
                    if (!has) {
                        findSpatialDataSourceAndExecute.call(this);
                    } else {
                        resolve(true);
                    }
                });

                function findSpatialDataSourceAndExecute() {
                    if (typeof layer === 'string') {
                        Layer.Model.findById(layer, (error, layer) => {
                            try {
                                if (error) {
                                    reject(error);
                                } else {
                                    SpatialDataSource.Model.findById(layer.spatialDataSourceId, (error, spatialDataSource:SpatialDataSourceSchema) => {
                                        try {
                                            if (error) {
                                                reject(error)
                                            } else {
                                                initAdapterFlow.call(this, spatialDataSource, layer);
                                            }
                                        } catch (error) {
                                            reject(error);
                                        }
                                    });
                                }
                            } catch (error) {
                                reject(error);
                            }
                        });
                    } else {
                        SpatialDataSource.Model.findById(layer.spatialDataSourceId, (error, spatialDataSource:SpatialDataSourceSchema) => {
                            if (error) {
                                reject(error);
                            } else {
                                initAdapterFlow.call(this, spatialDataSource, layer);
                            }
                        });
                    }
                }

                function initAdapterFlow(spatialDataSource:SpatialDataSourceSchema, layer:LayerSchema) {
                    try {
                        let serverType = spatialDataSource.serverType.toLowerCase();
                        let promise = customValidator.verifyExistsNReturn(`module/geo/adapter/${serverType}/`, /Adapter\.js$/, /Adapter$/);
                        promise.catch(reject);
                        promise.then((Adapter) => {
                            try {
                                let adapter = new Adapter(request);
                                let promise = adapter[operationName](spatialDataSource, layer, params);
                                promise.then((result) => {
                                    try {
                                        resolve(result);

                                        if (result instanceof Array && result[0]) {
                                            AdapterHelper._generateGeoCache(result[0], layer, operationName, params);

                                        } else if (!(result instanceof Array)) {
                                            AdapterHelper._generateGeoCache(result, layer, operationName, params);
                                        }

                                    } catch (error) {
                                        reject(error);
                                    }
                                }, reject, notify);
                            } catch (error) {
                                reject(error);
                            }
                        });
                    } catch (error) {
                        reject(error);
                    }
                }

            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * Método responsável por padronizar e gerar o hash com os parâmetros da operação da camada.
     * @param layerId
     * @param operationName
     * @param params
     * @returns {string}
     * @static
     */
    static generateHashToLayer(layerId:string, operationName:string, params?:any) {
        return Converter.createHash([layerId, operationName, params]);
    }

    /**
     * Método responsável por verificar se um existe cache para as informações parametrizadas, se exisitir ela as retorna.
     * @param {Function} next
     * @param {LayerSchema} layerId
     * @param {String} operationName
     * @param {*} [params]
     * @static
     */
    static verifyNGetFromCache(next:(error:Error, result?:any) => void, layerId:string, operationName:string, params?:any):void {
        GeoCache.Model.findOne({ hash : AdapterHelper.generateHashToLayer(layerId, operationName, params) }, (error, doc) => {
            try {
                if (error) {
                    throw error;
                }

                next(null, doc);

            } catch (error) {
                next(error);
            }
        });
    }

    /**
     * Método responsável em gerar o cache do resultado de uma operação da camada.
     * @param {String|Buffer|Object} valueToCache
     * @param {LayerSchema} layer
     * @param {String} operationName
     * @param {*} [params]
     * @static
     */
    static _generateGeoCache(valueToCache:any, layer:any, operationName:string, params?:any):void {
        process.nextTick(() => {
            try {
                if (layer.geoCache[operationName + 'Cache']) {
                    let hash = AdapterHelper.generateHashToLayer(layer._id.toString(), operationName, params);
                    let info:any;

                    try {
                        if (typeof valueToCache === 'string') {
                            valueToCache = valueToCache.trim();
                        }

                        if (typeof valueToCache === 'string' && /^[\{\[]/.test(valueToCache)) {
                            valueToCache = JSON.parse(valueToCache);
                        }

                    } catch (error) {
                        console.warn(error);

                    } finally {
                        info = valueToCache;
                    }

                    GeoCache.Model.findOne({
                        hash : hash
                    }, (error, doc) => {
                        try {
                            if (!error) {

                                if (!doc) {
                                    doc = new GeoCache.Model();
                                }

                                doc.hash = hash;
                                doc.info = info;
                                doc.params = Object.assign({}, params, {
                                    layerId : layer._id.toString()
                                });
                                doc.save((error) => {
                                    if (error) {
                                        console.error(error);
                                    }
                                });
                            }
                        } catch (error) {
                            console.error(error);
                        }
                    });
                }
            } catch (error) {
                console.error(error);
            }
        });
    }

}
