
import {IOperationParams, OperationBase} from "./_base/OperationBase";
import {AdapterHelper} from "../helper/AdapterHelper";
import * as Q from "q";
import {EndPoint} from "../../../decorator/EndPoint";
import {HttpVerb} from "../../../enum/HttpVerb";
import request = require("request");
import {Layer} from "../../../data/schema/Layer";
import {FormatImage} from "../../../enum/FormatImage";

export class GetLegendGraphic extends OperationBase{

    @EndPoint({
        verb: [HttpVerb.GET, HttpVerb.POST],
        route: '/geo/:layerId/getLegendGraphic',
        params : {
            height:{
                type : 'string',
                required : true
            },
            width:{
                type : 'string',
                required : true
            }
        },
        response : {}
    })
    execute(params:any):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let {layerId} = params as any;

            Layer.Model.findById(layerId, (error, doc) => {
                if (error) {
                    deferred.reject(error);
                } else {

                    if (doc && doc.info && doc.info.INDE && doc.info.dsn) {
                        params.layer = doc.layerIdentifier;
                        request({
                            method: 'GET',
                            url: doc.info.dsn.replace(/\/wfs/i, '/wms'),
                            qs: {
                                service : 'WMS',
                                request  : 'GetLegendGraphic',
                                version : '1.1.1',
                                format : 'image/' + FormatImage.PNG,
                                width : Number(params.width),
                                height : Number(params.height),
                                layer : doc.layerIdentifier
                            }
                        }, (error) => {
                            if (error) {
                                console.error(error);
                            }
                        }).pipe(this.response);
                    } else {
                        let promise = AdapterHelper.executeWithLayer(layerId, 'getLegendGraphic', {params}, this.request);
                        promise.catch(deferred.reject);
                        promise.then(deferred.resolve, deferred.reject, deferred.notify);
                    }
                }
            });


        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

}

export class GetLegendGraphicParams implements IOperationParams {
    params:any;
    constructor(request:any){
        this.params = request.params;
    }

    validate(values:any):void {

    }
}

export class GetLegendGraphicResponse {
    //
}