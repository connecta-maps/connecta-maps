import * as http from 'http';
import * as Q from 'q';

/**
 * @classdesc Classe principal da operação, esta será instanciada e executada para cada requisição feita à ela.
 * @class
 */
export abstract class OperationBase{

    constructor(protected request, protected response) { }

    /**
     * Método responsável por executar a operação.
     * @param {Object} params
     * @param {String} dataSourceId
     * @returns {Promise}
     */
    abstract execute(params:Object, dataSourceId?:string):Q.Promise<any>|Promise<any>;

}

/**
 * @class
 * @classdesc Classe responsável por mapear os parâmetros de requisição para a operação em que representa.
 */
export interface IOperationParams {

    /**
     * Método responsável por validar os parâmetros de requisição.
     * @return {Boolean}
     * @throws {Error} dispara erro caso alguma validaçao estiver incorreta.
     */
    validate(values:Object):void;

}