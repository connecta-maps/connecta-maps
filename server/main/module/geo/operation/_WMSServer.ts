import {OperationBase, IOperationParams} from "./_base/OperationBase";
import Q = require('q');
import {HttpVerb} from "../../../enum/HttpVerb";
import {Request} from "../../../helper/Request";
import {ServerException} from "../../../global/ServerException";
import {IModuleRoute} from "../../_base/IModule";
import Config from "../../../Config";

export class WMSServer extends OperationBase {

    static defineRoute():IModuleRoute {
        return {
            route : '/geo/:$name/MapServer/WMSServer',
            verb : HttpVerb.GET
        };
    }

    /**
     *
     * @param parametersRequest
     * @param dataSourceId
     * @returns {Promise<T>}
     * @override
     */
    execute(parametersRequest:any, dataSourceId?:string):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let basemapConfig:any;
            let name = this.request.getter.getParam('$name');

            Config.basemaps.forEach((base) => {
                if (base.name === name) {
                    basemapConfig = base;
                }
            });

            if (!basemapConfig) {
                throw new ServerException('RES_ERROR_12', ['name']);
            }

            let params:any = {};

            for (let key in this.request.params) {
                if (!(/^\$/.test(key))) {
                    params[key.toLowerCase()] = this.request.params[key];
                }
            }

            delete params.crs;
            params.srs = 'EPSG:3857';
            Object.assign(params, basemapConfig.customParams);
            let promise = Request.get(basemapConfig.url, params, {}, {useNotify : true});
            promise.then((result) => {
                this.response.sender.end(result);
            }, (error) => {
                this.response.sender.error(error);
            }, (chunk) => {
                this.response.sender.chunk(chunk);
            });

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

}

export class WMSServerParams implements IOperationParams {

    validate(values:Object):void {

    }

}

export class WMSServerResponse {

}