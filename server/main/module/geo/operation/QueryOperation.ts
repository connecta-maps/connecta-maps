
import {OperationBase, IOperationParams} from "./_base/OperationBase";
import {ArcGISGeometryType, ArcGISTypeField} from "../../../enum/ArcGISEnum";
import {AdapterHelper} from "../helper/AdapterHelper";
import * as Q from "q";
import {ResponseFormat} from "../../../enum/ResponseFormat";
import {ServerException} from "../../../global/ServerException";
import {EndPoint} from "../../../decorator/EndPoint";
import {HttpVerb} from "../../../enum/HttpVerb";

export class Query extends OperationBase {

    @EndPoint({
        route : '/geo/:layerId/query',
        verb : [HttpVerb.GET, HttpVerb.POST],
        response : {},
        params : {
            f : {
                type : 'string',
                required : false,
                'default' : ResponseFormat.JSON
            },
            project:{
                type : 'object',
                required: true
            },
            filterConfig:{
                type : 'array',
                required: false
            },
            where:{
                type : 'string',
                required: false
            },
            objectIds:{
                type : 'array',
                required: false
            },
            geometry:{
                type : 'object',
                required: false
            },
            geometryType : {
                type : 'string',
                required: false
            },
            inSR : {
                type : 'string',
                required: false
            },
            outSR:{
                type : 'string',
                required: false
            },
            returnGeometry:{
                type : 'boolean',
                required: false
            },
            returnIdsOnly:{
                type : 'boolean',
                required: false
            },
            returnCountOnly:{
                type : 'boolean',
                required: false
            },
            outFields:{
                type : 'array',
                required: false
            },
            orderByFields:{
                type : 'string',
                required: false
            },
            returnDistinctValues:{
                type : 'boolean',
                required: false
            },
            resultRecordCount:{
                type : 'boolean',
                required: false
            },
            text:{
                type : 'string',
                required: false
            }
        }
    })
    execute(params:any):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let {layerId} = params as any;
            let promise = AdapterHelper.executeWithLayer(layerId, 'query', params, this.request);
            promise.catch(deferred.reject);
            promise.then(deferred.resolve, deferred.reject, deferred.notify);
        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }
}

export interface QueryParams {
    f:ResponseFormat;
    project:any;
    filterConfig:any[];
    where:string;
    objectIds:string;
    layerDefs:Array<Object>;
    geometry:any;
    geometryType;
    inSR:string|number;
    spatialRel;
    outSR:string|number;
    returnGeometry:boolean;
    returnIdsOnly:boolean;
    returnCountOnly:boolean;
    outFields:any;
    orderByFields:string;
    groupByFieldsForStatistics: string;
    outStatistics: Array<any>;
    returnZ:boolean;
    returnM:boolean;
    returnDistinctValues:boolean;
    resultRecordCount:number;
    text:string;
}

export class SpatialReference {

    private _wkid : string | number;

    constructor(obj) {
        Object.assign(this, obj);
    }

    get wkid():string|number {
        return this._wkid;
    }

    set wkid(value:string|number) {
        this._wkid = value;
    }
}

export class QueryField {

    private _name : string;
    private _type : ArcGISTypeField;
    private _alias : string;
    private _length : number;
    private _domain : string;

    constructor(obj) {
        Object.assign(this, obj);
    }

    set name(value:string) {
        this._name = value;
    }

    set type(value:ArcGISTypeField) {
        this._type = value;
    }

    set alias(value:string) {
        this._alias = value;
    }

    set length(value:number) {
        this._length = value;
    }

    set domain(value:string) {
        this._domain = value;
    }

    get name():string {
        return this._name;
    }

    get type():ArcGISTypeField {
        return this._type;
    }

    get alias():string {
        return this._alias;
    }

    get length():number {
        return this._length;
    }

    get domain():string {
        return this._domain;
    }
}

export class QueryFeature {

    private _attributes : Object;
    private _geometry : Object | Array<any>;

    constructor(obj) {
        Object.assign(this, obj);
    }

    set attributes(value:Object) {
        this._attributes = value;
    }

    set geometry(value:Object|Array<any>) {
        this._geometry = value;
    }

    get attributes():Object {
        return this._attributes;
    }

    get geometry():Object|Array<any> {
        return this._geometry;
    }
}