import {OperationBase} from "./_base/OperationBase";
import {AdapterHelper} from "../helper/AdapterHelper";
import * as Q from "q";
import {EndPoint} from "../../../decorator/EndPoint";
import {HttpVerb} from "../../../enum/HttpVerb";
import {Layer} from "../../../data/schema/Layer";
import * as request from "request";

export class GetMap extends OperationBase {

    @EndPoint({
        route : '/geo/:layerId/getMap',
        verb : [HttpVerb.POST, HttpVerb.GET],
        response : {},
        params : {
            service:{
                type : 'string',
                required : true
            },
            request:{
                type : 'string',
                required : true
            },
            layers: {
                type : 'string',
                required : false
            },
            styles: {
                type : 'string',
                required : false
            },
            format:{
                type : 'string',
                required : true
            },
            transparent:{
                type : 'string',
                required : false,
                'default' : true
            },
            version:{
                type : 'string',
                required : false
            },
            height:{
                type : 'string',
                required : true
            },
            width:{
                type : 'string',
                required : true
            },
            srs:{
                type : 'string',
                required : false
            },
            bbox:{
                type : 'string',
                required : false
            }
        }
    })
    execute(params:any):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let {layerId} = params as any;

            Layer.Model.findById(layerId, (error, doc) => {
                if (error) {
                    deferred.reject(error);
                } else {

                    if (doc && doc.info && doc.info.INDE && doc.info.dsn) {
                        params.layers = doc.layerIdentifier;
                        request({
                            method : 'GET',
                            url : doc.info.dsn.replace(/\/wfs/i, '/wms'),
                            qs : params
                        }).pipe(this.response);
                    } else {
                        let promise = AdapterHelper.executeWithLayer(layerId, 'getMap', {params}, this.request);
                        promise.catch(deferred.reject);
                        promise.then(deferred.resolve, deferred.reject, deferred.notify);
                    }

                }
            });

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }
}