import {IDataSourceService, IFilterConfig} from "../_base/IDataSourceService";
import {ViewerSchema} from "../../../../data/schema/Viewer";
import {ProjectSchema, RichLayer} from "../../../../data/schema/Project";

export class OBIEEAdapter implements IDataSourceService {


    getResultSet(params: { project: ProjectSchema; richLayer: RichLayer }): Promise<any> {
        return null;
    }

    /**
     *
     * @param params
     * @returns {Promise<T>}
     */
    buildDataSource(params: any): Promise<any> {
        return new Promise((resolve, reject) =>{

            try {

                //nao apagar
                // let pattern = /(https?:\/\/.+\/analytics)/;
                // if(!pattern.test(params.dsn)){
                //     throw new ServerException('RES_ERROR_10', ['dsn']);
                // }

                let dataSource:any = params;
                resolve(dataSource);

            } catch (error) {
                reject(error);
            }

        });

    }

    /**
     * Método responsável por construir e salvar o Viewer do OBIEE.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    saveViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then(resolve);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Método responsável por deletar o Viewer do OBIEE.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    deleteViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then(resolve);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Método responsável por atualizar o Viewer do OBIEE.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    updateViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then(resolve);
            } catch (err) {
                reject(err);
            }
        });
    }


    getDistinctValues(param: { richLayer?: RichLayer; domainId: string; field: string; dataSourceIdentifier: string; filterConfig: IFilterConfig[] }): Promise<{ [p: string]: any }> {
        return null;
    }
}
