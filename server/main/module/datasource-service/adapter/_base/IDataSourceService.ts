import {ViewerSchema} from "../../../../data/schema/Viewer";
import {ProjectSchema, RichLayer} from "../../../../data/schema/Project";
import {GeoJSON} from "../../../../mapping/GeoJSON";
/**
 * @classdesc Interface que estabelece os contratos de implementação para todas os Adapters do módulo
 * Datasource Service
 */
export interface IDataSourceService {

    /**
     * Método responsável por construir os Meta Dados dos serviços que provêm os dados analíticos.
     * @param {Object} params
     * @returns {Promise}
     */
    buildDataSource(params:any):Promise<any>;

    /**
     * Método responsável por construir e salvar o Viewer específico de cada Datasource.
     * @param {Object} params
     * @returns {Promise}
     */
    saveViewer(operationFunction: Function): Promise<ViewerSchema>;

    /**
     * Método responsável por deletar o Viewer específico de cada Datasource.
     * @param {Object} params
     * @returns {Promise}
     */
    deleteViewer(operationFunction: Function): Promise<ViewerSchema>;

    /**
     * Método responsável por atualizar o Viewer específico de cada Datasource.
     * @param {Object} params
     * @returns {Promise}
     */
    updateViewer(operationFunction: Function): Promise<ViewerSchema>;

    /**
     *
     * @param params
     */
    getResultSet(params:{
        project:ProjectSchema,
        richLayer:RichLayer,
        geoJSON?:GeoJSON,
        filterConfig?:IFilterConfig[]
    }):Promise<{
        businessData:Object[],
        where:string
    }>;

    getDistinctValues(param:{richLayer?:RichLayer, domainId:string, field:string, dataSourceIdentifier:string, filterConfig:IFilterConfig[]}):Promise<{[distinctFieldName:string]:any}>;

}

export interface IFilterConfig { operator:string; field:string; value?:any; values?:any; }