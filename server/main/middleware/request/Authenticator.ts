import * as restify from "restify";
import {IRequestMiddleware} from "./_base/IRequestMiddleware";
import Config from "../../Config";
import {Request} from "../../helper/Request";
import {ServerException} from "../../global/ServerException";

/**
 * @class
 * @classdesc
 */
export class Authenticator implements IRequestMiddleware {

    /**
     *
     * @param request
     * @param response
     * @param next
     * @override
     */
    execute(request: restify.Request, response: restify.Response|any, next: Function): void {
        next();
    }

}


