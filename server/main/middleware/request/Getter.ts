import * as restify from "restify";

export class Getter{

    constructor(private request:restify.Request) {}

    getParam(paramName:string):any {
        var param:any;
        if (this.request.params && paramName in this.request.params) {
            param = this.request.params[paramName];

        } else if (this.request.query && paramName in this.request.query) {
            param = this.request.query[paramName];

        } else if (this.request.params && paramName in this.request.params) {
            param = this.request.params[paramName];

        }
        return param;
    }

}
