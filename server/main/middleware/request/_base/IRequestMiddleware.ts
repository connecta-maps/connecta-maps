import * as restify from "restify";

export interface IRequestMiddleware {

    /**
     * Método que será executado quando for feito qualquer requisição ao servidor.
     * @param request
     * @param response
     * @param next
     */
    execute(request:restify.Request, response:restify.Response, next:Function):void;

}
