import {customValidator} from "../../util/Validator";
import {Locale} from "../../helper/Locale";
import {ServerException} from "../../global/ServerException";

const http = require('http');

/**
 * @class
 * @classdesc Classe responsável em prover métodos de resposta para requisições, de forma que todas sejam padronizadas.
 */
export class Sender{

    /**
     * Status HTTTP padrão para erros do servidor.
     * @type {Number}
     *
     */
    private STATUS_DEFAULT_ERROR:Number = 500;

    /**
     * Identifica se a resposta já foi quebrada em pedaços, se isto já ocorreu os métodos: sendError e sendData,
     * devem seguir outro fluxo de resposta.
     * @type {boolean}
     */
    private CHUNKED:Boolean = false;

    /**
     * Usuário que fez a requisição.
     * @type {Object}
     */
    private user:{
        language:string
    };

    /**
     * Propriedade que armazenará a resposta <i>chuncked</i>. Utilizada no processo de <i>callback</i>.
     * @type {String}
     */
    private _chunks:string = '';

    /**
     *
     * @param {http.ClientRequest} request
     * @param {http.ServerResponse} response
     */
    constructor(private request:any, private response:any) {

        //TODO: implementar usuário
        this.user = {
            language : ''
        };
        response.charSet('utf-8');
    }

    /**
     * Método responsável em enviar em pedaços a resposta.
     * @param {*} params
     */
    chunk(params) {
        let [chunk, response] = params;

        try {
            let callback = this.request.getter.getParam('callback');

            if(!this.CHUNKED && response){
                this.response.writeHead(response.statusCode, response.headers);
            }

            this.CHUNKED = true;

            if (callback) {
                this._chunks += chunk;
                return;
            }

            if (customValidator.isObject(chunk)) {
                chunk = JSON.stringify(chunk);
            }

            if (!(chunk instanceof Buffer) && typeof chunk !== 'string') {
                console.error('Tipo do chunk não válido, somente string ou buffer');
                return;
            }

            this.response.write(chunk);
        } catch (error) {
            console.error(error);
            this.error(error);
        }
    }

    /**
     * Método responsável em enviar uma resposta
     * @param {* | [Object, {headers : {contentType:String}  }] } params
     */
    end(params:any) {
        let data, response;

        try {
            if (Array.isArray(params) && params.length === 2 && params[1] instanceof http.ServerResponse) {
                [data, response] = params;

                if (response && !response.statusCode) {
                    data = params;
                }
            } else {
                data = params;
            }

            let callback = this.request.getter.getParam('callback');

            if (this.CHUNKED) {

                if (!this._chunks) {
                    this.response.end();

                } else {
                    this.response.end(callback + '(' + this._chunks + ');');
                    this._chunks = null;
                }

                return;
            }

            if (callback) {
                let response = '';

                if (typeof data === 'string') {
                    response = data;

                } else if (typeof data === 'object') {
                    response = JSON.stringify(data);

                } else {
                    console.log('Apenas objetos e strings podem ser enviados para requisições no padrão JSONP');

                }

                this.response.end(callback + '(' + response + ');');

            } else {
                this.response.writeHead(200, {
                    'Content-Type' : (response && response.headers) ?
                        response.headers['content-type'] || response.headers['contentType']
                        : 'application/json; charset=utf-8'
                });

                if (typeof data === 'object') {
                    data = JSON.stringify(data);
                }

                this.response.end(data);
            }

        } catch (error) {
            console.error(error);
            this.error(error);
        }
    }

    /**
     * Método responsável em enviar uma resposta de erro.
     * @param {Error|ServerException} error
     * @param {Number} status
     */
    error(error:any, status?:Number) {
        let response = {};
        let message:string;
        let messageDetails:string;

        try {
            if (this.CHUNKED) {
                console.log('Após pedaços terem sidos enviados à requisição, não é mais possível modificar o status da resposta');
                this.response.end();

            } else {
                status = status || this.STATUS_DEFAULT_ERROR;

                if (error.__proto__ === Error.prototype) {
                    throw error;
                }

                if (typeof error.errors === 'object') {
                    messageDetails = JSON.stringify(error.errors);

                } else {

                    messageDetails = (error instanceof ServerException && error.message) ? error.message : '';
                }

                message = error instanceof ServerException ? Locale.getMessage(error.code, error.params, this.user.language) : error.message;

                response = {
                    details : {
                        stack : JSON.stringify(error),
                        message : messageDetails
                    },
                    message : message
                };
            }

        } catch (error) {

            response = {
                message :  Locale.getMessage('RES_ERROR_01'),
                details : {
                    stack : JSON.stringify(error),
                    message : error.message
                }
            };

        } finally {

            this.response.json(status, response);

            let stack = error.stack || error.message;

            console.error('Erro Controlado==========\n\n',
                message, '\n', stack,
                '\n\n==========Erro Controlado');
        }
    }

}
