import {HttpVerb} from "../enum/HttpVerb";
import {ServerException} from "../global/ServerException";

interface Config {
    route:string;
    params?:{
        [paramName:string] : {
            type: string;
            required: boolean;
            cast?: boolean;
            default?: any;
        }
    };
    response:any;
    verb:HttpVerb|HttpVerb[];
}

let pathsMapping = {};

export function EndPoint(config:Config) {

    if (!config.route) {
        throw new Error('Configuração:route é obrigatória');
    }

    if (!config.verb) {
        throw new Error('Configuração:verb é obrigatória');
    }

    if (!config.response) {
        throw new Error('Configuração:response é obrigatória');
    }

    return function (target, methodName: string) {
        let callback = (req, res) => {
            try {
                if (config.params) {
                    for (let paramName in config.params) {
                        let paramDefinitions = config.params[paramName];
                        let value = typeof req.params[paramName] !== 'undefined' ? req.params[paramName] : paramDefinitions.default;
                        value = validateRequired(paramDefinitions, paramName, value);

                        if (paramDefinitions.required === true || value !== void 0) {
                            value = validateCast(paramDefinitions, paramName, value);
                            value = validateType(paramDefinitions, paramName, value);
                            value = validateENUM(paramDefinitions, paramName, value);
                        }

                        req.params[paramName] = value;
                    }
                }

                new target.constructor(req, res)[methodName](req.params)
                    .then(
                        res.sender.end.bind(res.sender),
                        res.sender.error.bind(res.sender),
                        res.sender.chunk.bind(res.sender)
                    )
                    .catch(res.sender.error.bind(res.sender));
            } catch (error) {
                res.sender.error(error);
            }
        };

        let verbs = config.verb instanceof Array ? config.verb : [config.verb];

        for (let verb of verbs) {
            let route = config.route.replace(/\:([^\/]+)/g, (a, group1) => '{' + group1 + '}');

            if (!pathsMapping[route]) {
                pathsMapping[route] = {};
            }
            pathsMapping[route][verb] = {
                "consumes": [
                    "application/json"
                ],
                "responses" : {
                    "200": {
                        "description": "OK",
                        "schema": config.response
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    },
                    "404": {
                        "description": "Not Found"
                    }
                }
            };

            if (config.params) {
                pathsMapping[route][verb].parameters = Object.keys(config.params).map(key => {
                    return {
                        "name": key,
                        "in": String(verb) === 'GET' ? "path" : 'body',
                        "description": key,
                        "required": config.params[key].required,
                        "type": config.params[key].type
                    };
                });
            }

            server.putRoute(verb, config.route, callback);
        }
    }
}

export function getConfigurations () {
    return pathsMapping;
}

function validateRequired(paramDefinitions, paramName, value) {
    if (paramDefinitions.required) {
        if (value === void 0) {
            throw new ServerException('RES_ERROR_06', [paramName]);
        }
    }
    return value;
}

function validateCast(paramDefinitions, paramName, value) {
    if (paramDefinitions.type === 'object' || paramDefinitions.type === 'array') {
        value = typeof value === 'string' ? JSON.parse(value) : value;
    } else {
        value = eval(`${paramDefinitions.type.replace(/(.)/, (a, b) => b.toUpperCase())}("${value}")`);
    }
    return value;
}

function validateENUM(paramDefinitions, paramName, value) {
    if (paramDefinitions.enum instanceof Array) {
        if (paramDefinitions.enum.indexOf(value) < 0) {
            throw new ServerException('RES_ERROR_10', [paramName]);
        }
    }
    return value;
}

function validateType(paramDefinitions, paramName, value) {
    if (paramDefinitions.type) {
        if (paramDefinitions.type === 'array') {
            if (!(value instanceof Array)) {
                throw new ServerException('RES_ERROR_10', [paramName]);
            }

        } else if (typeof value !== paramDefinitions.type) {
            throw new ServerException('RES_ERROR_10', [paramName]);

        }
    }
    return value;
}