import * as path from "path";
import * as fs from "fs";
import has = Reflect.has;

const pathCache = path.resolve(__dirname, '../../', 'target', 'cache');

if (!fs.existsSync(pathCache)) {
    fs.mkdirSync(pathCache);
}

let getFileCacheName = (hash) => {
    return path.resolve(pathCache, hash + '.json')
};

export class CacheFileSystem {

    static createCacheSync(hash:string, data:any) {
        let filePath = getFileCacheName(hash);

        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
        }

        fs.writeFileSync(filePath, JSON.stringify({data}));
    }

    static getCacheSync(hash:string):object {
        if (fs.existsSync(getFileCacheName(hash))) {
            return JSON.parse(fs.readFileSync(getFileCacheName(hash)).toString()).data;
        }
    }

    static existsSync(hash:string) {
        return fs.existsSync(getFileCacheName(hash));
    }

}