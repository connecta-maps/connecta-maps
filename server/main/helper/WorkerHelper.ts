const fork = require('child_process').fork;
const path = require('path');

export default class WorkerHelper {

    /**
     * @type {Worker}
     */
    _worker;

    /**
     *
     * @param {String} workerName
     */
    constructor(workerName) {
        this._worker = fork(path.resolve(__dirname, '../', 'worker', workerName));
    }

    /**
     *
     * @param {*} data
     */
    postMessage(data) {
        this._worker.send(data);
    }

    execute(data) {
        return new Promise((resolve, reject) => {
            try {
                this._worker.on('exit', (error) => {
                    reject(error);
                    this._worker.kill();
                });

                this._worker.on('message', (response) => {
                    if (response && response.error) {
                        reject(response.error);
                        this._worker.kill();

                    } else {
                        resolve(response);
                        this._worker.kill();
                    }
                });

                this.postMessage(data);
            } catch (error) {
                reject(error);
            }
        });
    }

}