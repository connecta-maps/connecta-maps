
import {ArcGISTypeField} from "../enum/ArcGISEnum";

export interface Field {

    alias:string;

    name:string;

    type:ArcGISTypeField;

}
