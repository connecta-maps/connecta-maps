
export interface GeoJSON {

    type:string;

    collectionName:string;

    srs : string;
    crs : string;

    features:{
        properties:any;
        businessData?:any;
        geometry:any;
        type:string;
    }[]

}
