const pingHttp = require('node-http-ping');
const ping = require('ping');
const EventEmitter = require('events');
const NodeSSH = require('node-ssh');
const path = require('path');

const RSA_PRIVATE_KEY = {
    privateKey: path.resolve(__dirname, 'cluster'),
    passphrase : 'cluster',
    password : 'cluster'
};

const CONFIG = {
    master : {
        host : '192.168.2.15',
        port : 27017,
        user : 'root'
    },
    slaves : [
        {
            host : '192.168.2.10',
            port : 27017,
            user : 'root'
        }
    ]
};

class ControlCluster {

    constructor() {
        this._masterMachineIsRunning = false;
        this._masterIsRunning = false;
        this._anySlaveIsRunning = false;
        this._currentSlave = 0;
        this._event = new EventEmitter();
        this._timerMaster = null;
        this._timerSlave = [];
        this.listen();
        this._timerMaster = this._pingIntermittent('master', CONFIG.master.host, CONFIG.master.port);
        this._pingIntermittent('machine-master', CONFIG.master.host);
    }

    listen() {
        this._event.on('machine-master:on', () => {
            this._masterMachineIsRunning = true;

            if (this._anySlaveIsRunning) {
                this.downSlave()
                    .then(() => {
                        this.upMaster();
                    });
            } else if (!this._masterIsRunning) {
                this.upMaster();
            }
        });
        this._event.on('machine-master:off', () => {
            this._masterMachineIsRunning = false;
            this.upAnySlave();
        });
        this._event.on('master:on', () => {
            this._masterIsRunning = true;
        });
        this._event.on('master:off', () => {
            this._masterIsRunning = false;
            clearInterval(this._timerMaster);
        });
    }

    upMaster() {
        return this._controlRemoteService('on', CONFIG.master)
            .then(() => {
                this._masterIsRunning = true;
                this._timerMaster = this._pingIntermittent('master', CONFIG.master.host, CONFIG.master.port);
                return true;
            });
    }

    upAnySlave() {
        if (this._timerSlave[this._currentSlave]) {
            clearInterval(this._timerSlave[this._currentSlave]);
        }
        let config = CONFIG.slaves[this._currentSlave];
        return this._controlRemoteService('on', config)
            .then(() => {
                this._anySlaveIsRunning = true;
                this._timerSlave[ this._currentSlave] =
                    this._pingIntermittent('slave:' + this._currentSlave,
                        CONFIG.slaves[this._currentSlave].host,
                        CONFIG.slaves[this._currentSlave].port
                    );
            });
    }

    downSlave() {
        let config = CONFIG.slaves[this._currentSlave];
        return this._controlRemoteService('off', config)
            .then(() => {
                this._anySlaveIsRunning = false;

                if (this._timerSlave[ this._currentSlave]) {
                    clearInterval(this._timerSlave[ this._currentSlave]);
                }

                return true;
            });
    }

    _pingIntermittent(name, host, port) {
        return setInterval(() => {
            (
                port ? pingHttp(host, port) : ping.promise.probe(host)
            )
                .then(time => {
                    console.log(name + ':on');
                    this._event.emit(name + ':on', time);
                })
                .catch(() => {
                    console.log(name + ':off');
                    this._event.emit(name + ':off', 0);
                });
        }, 2000);
    }

    _controlRemoteService(toggle = 'on', config) {
        let ssh = new NodeSSH();

        return ssh.connect(Object.assign({
            host: config.host,
            username: config.user,
        }, RSA_PRIVATE_KEY)).then(() => {
            let command = {
                'on' : 'service mongod start',
                'off' : 'service mongod stop'
            };
            return ssh.execCommand(command[toggle])
                .then((result) => {
                    console.log(result.stderr);
                    console.log('Connected on:', config.host);
                    return result;
                })
        });
    }

}

new ControlCluster();