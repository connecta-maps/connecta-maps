import React from "react";
import {
    CircularProgress,
    MenuItem,
    RaisedButton,
    SelectField,
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
    TextField,
    Chip, Checkbox, Avatar, FontIcon, Popover
} from "material-ui";
import IbamaService from "../Service";
import update from 'react-addons-update';

const locale = window['mapsLocale'];

class FormularioBuscarProcessos extends React.Component {

    _baseClass = "ibama-formulario-busca-processos";

    _columnToSort = "";

    _currentTargetAnchorInfoChip = {};

    _processToBuildInfoChipContent = {};

    constructor (props) {
        super(props);

        this.state = {
            searchDialogOpen: false,
            searchTipologiaOpen : false,
            processosSelecionados : props.processosSelecionados,
            processos : [],
            processosMarcadosTabela: [],
            tipologias: [],
            searchParams : {},
            loading: false,
            infoChipIsOpen : false,
            mapaColunas : {
                NU_PROCESSO_IBAMA : {},
                NO_TIPOLOGIA : {},
                NO_EMPREENDIMENTO : {},
                NO_EMPREENDEDOR : {}
            }
        };
    }

    componentDidMount () {
        IbamaService.tipologias().then((response) => {
            this.setState({tipologias: response.data.sort((a, b)=> a['NO_TIPOLOGIA'].localeCompare(b['NO_TIPOLOGIA']))});
        });
    }

    buildTableRows = (row, index) => {
        return (
            <TableRow key={index}>
                <TableRowColumn style={{width : 5}}>
                    <Checkbox onCheck={(event, checked) => {this.handleRowSelection(index, checked)}}
                              checked={row.checked !== undefined ? row.checked : this.hasChip(row)}/>
                </TableRowColumn>
                <TableRowColumn title={row.NU_PROCESSO_IBAMA}>
                    {row.NU_PROCESSO_IBAMA}
                </TableRowColumn>
                <TableRowColumn title={row.NO_TIPOLOGIA}>
                    {row.NO_TIPOLOGIA}
                </TableRowColumn>
                <TableRowColumn title={row.NO_EMPREENDIMENTO}>
                    {row.NO_EMPREENDIMENTO}
                </TableRowColumn>
                <TableRowColumn title={row.NO_EMPREENDEDOR}>
                    {row.NO_EMPREENDEDOR}
                </TableRowColumn>
            </TableRow>
        );
    };

    renderTipologias = () => {
        return this.state.tipologias.map((item) => {
            return (
                <MenuItem key={item.ID_TIPOLOGIA}
                          value={item.ID_TIPOLOGIA}
                          title={item.NO_TIPOLOGIA}
                          primaryText={item.NO_TIPOLOGIA} />
            );
        });
    };

    onClickInfoChip = (event, processo) => {
        this._currentTargetAnchorInfoChip = event.currentTarget;
        this._processToBuildInfoChipContent = processo;

        this.setState({
            infoChipIsOpen : true
        });
    };

    renderChips = () => {
        let hidePopovers = () => {
            let keys = Object.keys(this.refs);

            for (let key of keys) {
                if (/popover/.test(key)) {
                    this.refs[key].style.display = 'none';
                }
            }
        };

        return this.state.processosSelecionados.map((processo, index) => {
            let ref = 'popover' + processo.NU_PROCESSO_IBAMA;
            return (
                <span
                    key={index}
                    onMouseLeave={(event) => {
                        event.stopPropagation();
                        hidePopovers();
                        console.log(event);
                    }}
                    onMouseEnter={event => {
                        hidePopovers();
                        this.refs[ref].style.display = 'block';
                        this.onClickInfoChip(event, processo);
                    }}>

                    <div ref={ref}
                         style={{display : 'none'}}
                         className={this._baseClass + "-popover"}>
                                {this.renderPopoverInfoContent()}
                            </div>

                    <Chip style={{margin: '2px 0', position : 'relative'}}
                          labelStyle={{textOverflow : 'ellipsis', overflow : 'hidden', maxWidth: '150px'}}
                          key={index}
                          onRequestDelete={this.handleDeleteChip.bind(this, processo, index)}>
                        {processo.NO_EMPREENDIMENTO}
                    </Chip>

                </span>
            )
        });
    };

    handleDeleteChip = (processo, chipIndex) => {
        let indexProcessoDeletado = -1;

        let processoDeletado = this.state.processos.filter((proc, index) => {
            if (proc.NU_PROCESSO_IBAMA === processo.NU_PROCESSO_IBAMA) {
                indexProcessoDeletado = index;
                return true;
            }
            return false;
        })[0];


        let newState = update(this.state, {
            processosSelecionados : {
                $splice : [[chipIndex, 1]]
            }
        });

        if (processoDeletado) {
            processoDeletado.checked = false;
            newState.processos.splice(indexProcessoDeletado, 1, processoDeletado);
        }

        this.setState(newState);
        this.props.onChange(newState.processosSelecionados);
    };

    handleSearchClick = () => {
        this.setState({loading: true});

        IbamaService.search(this.state.searchParams)
            .then((response) => {
                this.setState({
                    processos: response.data,
                    processosMarcadosTabela: [],
                    loading: false
                });
            })
            .catch((error) => {
                window['ConnectaMaps'].MapsDataLoader.set('IBAMA_MESSAGE', error.message);
            });
    };

    handleRowSelection = (index, checked) => {
        let command = checked ? "$push" : "$splice";
        let processo = this.state.processos[index];

        let parameter = checked ? [processo] : [[this.getIndexProcessoSelecionadoPeloNumero(processo.NU_PROCESSO_IBAMA), 1]];

        processo.checked = checked;

        let newState = update(this.state, {
            processosSelecionados : {
                [command] : parameter
            }
        });

        this.setState(newState);
        this.props.onChange(newState.processosSelecionados);
    };

    updateSearchParams = (param, value) => {
        this.setState(update(this.state, {
            searchParams: {
                [param] : {
                    $set: value
                }
            }
        }));
    };

    handleSubmitForm = (event) => {
        event.preventDefault();
        if(!this.state.loading) {
            this.handleSearchClick();
        }
        return false;
    };

    getIndexProcessoSelecionadoPeloNumero = (numeroProcesso) => {
        let _index;

        this.state.processosSelecionados.filter((processoSelecionado, index) => {
            if (numeroProcesso === processoSelecionado.NU_PROCESSO_IBAMA) {
                _index = index;
            }
        });

        return _index;
    };

    hasChip = (processo) => {
        let hasChip = this.state.processosSelecionados.filter((item) => item.NU_PROCESSO_IBAMA === processo.NU_PROCESSO_IBAMA);

        return hasChip.length > 0;
    };

    validateForm = () => {
        return !this.state.searchParams.numeroProcesso &&
            !this.state.searchParams.empreendimento &&
            !this.state.searchParams.empreendedor &&
            !this.state.searchParams.tipologia;
    };

    sortByColumn = (columnName) => {
        let order = !this.state.mapaColunas[columnName].orderAsc;
        this._columnToSort = columnName;

        let arr = this.state.processos.sort((a, b) => {
            if (!a[columnName] || !b[columnName]) return 0;

            return a[columnName].localeCompare(b[columnName]);
        });

        if (!order) arr.reverse();

        this.setState(update(this.state, {
            mapaColunas : {
                [columnName] : {
                    orderAsc : {
                        $set : order
                    }
                }
            },
            processos : {
                $set : arr
            }
        }));
    };

    defineTableHeaderClassName = (columnName) => {
        let iconClassName = columnName === "NU_PROCESSO_IBAMA" ? "maps-icon-sort-numeric-" : "maps-icon-sort-alpha-";

        if (!this.state.mapaColunas[columnName].orderAsc && columnName === "NU_PROCESSO_IBAMA") iconClassName = "maps-icon-sort-numberic-";

        return "maps-icon " + iconClassName + (this.state.mapaColunas[columnName].orderAsc ? "asc" : "desc");
    };

    defineTableHeaderIconSortColor = (columnName) => {
        let color = this.state.mapaColunas[columnName].orderAsc !== undefined ? "#00796b" : "#e0e0e0";

        if (this._columnToSort !== columnName) {
            color = "#e0e0e0";
        }

        return color;
    };

    renderPopoverInfoContent = () => {
        return <PopoverInfoContent {...this._processToBuildInfoChipContent}/>
    };

    render() {
        let tableHeaderColumnStyle = {
            display : "flex",
            justifyContent : "space-between"
        };

        return (
            <div className={this._baseClass}>

                <div className={this._baseClass + '-search-header'}>

                </div>

                <div className={this._baseClass + '-search-content'} style={{display: 'flex'}}>
                    <div style={{width : '20%', position : 'relative'}}>
                        <h5>Busca Avançada</h5>

                        <div style={{width : '100%'}}>
                            <form onSubmit={this.handleSubmitForm}>
                                <TextField style={{width : '85%'}}
                                           floatingLabelText="Nº do processo"
                                           value={this.state.searchParams.numeroProcesso}
                                           onChange={(event) => {
                                               this.updateSearchParams("numeroProcesso", event.target.value);
                                           }} />

                                <TextField style={{width : '85%'}}
                                           floatingLabelText="Empreendimento"
                                           onChange={(event) => {
                                               this.updateSearchParams("empreendimento", event.target.value);
                                           }} />

                                <TextField style={{width : '85%'}}
                                           floatingLabelText="Empreendedor"
                                           onChange={(event) => {
                                               this.updateSearchParams("empreendedor", event.target.value);
                                           }} />

                                <SelectField style={{width : '85%'}}
                                             floatingLabelText="Tipologia"
                                             value={this.state.searchParams.tipologia}
                                             onChange={(e, i, v) => {
                                                 if (v === 'NO') {
                                                     v = null;
                                                 }
                                                 this.updateSearchParams("tipologia", v);
                                             }}>
                                    <MenuItem value={'NO'}
                                              title={'Escolha uma tipologia'}
                                              primaryText={'Escolha uma tipologia'} />
                                    {this.renderTipologias()}
                                </SelectField>

                                <RaisedButton
                                    disabled={this.validateForm()}
                                    type="submit"
                                    label={locale.getLabel('L0079')}
                                    primary={true} />
                            </form>
                        </div>
                    </div>

                    <div style={{
                        width: 'calc(75%)',
                        paddingRight : '15px',
                        cursor : "pointer"
                    }}>
                        {this.state.loading && (
                            <div style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                                <div className={this._baseClass + ' loading ' + (this.state.loading ? 'active' : 'hidden')}>
                                    <CircularProgress size={30} />
                                </div>
                            </div>
                        )}

                        {!this.state.loading && (
                            <Table>
                                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                    <TableRow>
                                        <TableHeaderColumn style={{width : 5}}>&nbsp;</TableHeaderColumn>
                                        <TableHeaderColumn>
                                            <div onClick={() => {this.sortByColumn("NU_PROCESSO_IBAMA")}}
                                                 style={tableHeaderColumnStyle}>
                                                <span>Nº Processo</span>
                                                <div className={this.defineTableHeaderClassName("NU_PROCESSO_IBAMA")}
                                                     style={{color : this.defineTableHeaderIconSortColor("NU_PROCESSO_IBAMA")}}>&nbsp;</div>
                                            </div>
                                        </TableHeaderColumn>
                                        <TableHeaderColumn>
                                            <div onClick={() => {this.sortByColumn("NO_TIPOLOGIA")}}
                                                 style={tableHeaderColumnStyle}>
                                                <span>Tipologia</span>
                                                <div className={this.defineTableHeaderClassName("NO_TIPOLOGIA")}
                                                     style={{color : this.defineTableHeaderIconSortColor("NO_TIPOLOGIA")}}>&nbsp;</div>
                                            </div>
                                        </TableHeaderColumn>
                                        <TableHeaderColumn>
                                            <div onClick={() => {this.sortByColumn("NO_EMPREENDIMENTO")}}
                                                 style={tableHeaderColumnStyle}>
                                                <span>Empreendimento</span>
                                                <div className={this.defineTableHeaderClassName("NO_EMPREENDIMENTO")}
                                                     style={{color : this.defineTableHeaderIconSortColor("NO_EMPREENDIMENTO")}}>&nbsp;</div>
                                            </div>
                                        </TableHeaderColumn>
                                        <TableHeaderColumn>
                                            <div onClick={() => {this.sortByColumn("NO_EMPREENDEDOR")}}
                                                 style={tableHeaderColumnStyle}>
                                                <span>Empreendedor</span>
                                                <div className={this.defineTableHeaderClassName("NO_EMPREENDEDOR")}
                                                     style={{color : this.defineTableHeaderIconSortColor("NO_EMPREENDEDOR")}}>&nbsp;</div>
                                            </div>
                                        </TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody displayRowCheckbox={false}>>
                                    {
                                        this.state.processos.map((row, index) => {
                                            return (
                                                this.buildTableRows(row, index)
                                            );
                                        })
                                    }
                                </TableBody>
                            </Table>
                        )}
                    </div>

                    <div style={{width : '20%'}}>
                        <h5>Selecionados</h5>
                        <div className={this._baseClass + "-chips"} style={{display : 'flex', flexDirection : 'column'}}>
                            {this.renderChips()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

class PopoverInfoContent extends React.Component {

    _popoverStyle = {
        padding : '10px',
        position : 'relative',
        flexDirection: 'column',
        display : 'flex',
        fontSize : '12px',
        justifyContent : 'center',
        alignItems : 'flex-start'
    };

    render () {
        return (
            <div style={this._popoverStyle}>
                {
                    Object.keys(this.props).map((field) => {
                        return ibamaMapField[field] && <div key={field}>{ibamaMapField[field] + " : " + (this.props[field] || "")}</div>
                    })
                }
            </div>
        );
    }
}

const ibamaMapField = {
    NU_PROCESSO_IBAMA: "Nº do processo",
    NO_EMPREENDEDOR: "Empreendedor",
    CD_EMPREENDIMENTO: "Código empreendimento",
    NO_EMPREENDIMENTO: "Nome empreendimento",
    NO_TIPOLOGIA: "Tipologia"
};

export default FormularioBuscarProcessos;