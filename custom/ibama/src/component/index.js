import React from "react";
import NavBar from "./NavBar";

const ConnectaMaps = window['ConnectaMaps'];
let mapsInstance = null;

export default class IBAMAComponent extends React.Component {

    _oldProcessos = [];
    _processosVisualizadosNoMapa = [];
    _processosParaExcluir = [];
    viewer;

    constructor(props) {
        super(props);

        this.state = {
            centralMessage : 'Pesquise acima os processos em que deseja visualizar.'
        };
    }

    onCreateViewer = ({viewer, businessData, processosSelecionados}) => {
        this._processosVisualizadosNoMapa = processosSelecionados;

        for (let nomeEstrutura in businessData) {
            ConnectaMaps.MapsDataLoader.set(nomeEstrutura, businessData[nomeEstrutura]);
        }

        this.setState({
            centralMessage : 'Loading...'
        });

        setTimeout(() => {

            if (!mapsInstance) {
                mapsInstance = new ConnectaMaps(this.refs.mapsNode);
                mapsInstance.viewerId = viewer._id;
                ConnectaMaps.MapsDataLoader.set(viewer._id, viewer);
            }

            if (!mapsInstance.isShown()) {
                mapsInstance.render();
                this._oldProcessos = this._processosVisualizadosNoMapa;

            } else {
                viewer = this.updateViewer(this.viewer, viewer);
                ConnectaMaps.MapsDataLoader.set(viewer._id, viewer);

                setTimeout(() => {
                    mapsInstance.updateViewer();
                    setTimeout(this.excluirProcessos.bind(this, viewer), 10);
                },10);
            }

            this.viewer = viewer;

            this.setState({
                centralMessage : ''
            });
        }, 100);
    };

    updateViewer(oldViewer, newViewer){
        oldViewer.project.richLayers = newViewer.project.richLayers;

        let toAdd = this.getViewersToAdd(oldViewer, newViewer);
        oldViewer.toolsConfig.analysisConfig[0].children = oldViewer.toolsConfig.analysisConfig[0].children.concat(toAdd);

        if(this._oldProcessos.length > 0){
            this._processosParaExcluir = this.getProcessosParaExcluir();
        }else{
            this._processosParaExcluir = [];
        }

        this._oldProcessos = this._processosVisualizadosNoMapa;

        return oldViewer;
    }

    getViewersToAdd(oldViewer, newViewer){
        let oldsGroups = this.getGroups(oldViewer);
        let newGroups = this.getGroups(newViewer);

        return newGroups.filter((item) => {
            let canFilter = true;

            for(let oldGroup of oldsGroups){
                if(oldGroup._id === item._id){
                    this.updateAnalysis(oldGroup, item);
                    canFilter = false;
                }
            }

            return canFilter;
        });
    }

    updateAnalysis(oldGroup, newGroup){
        oldGroup.children.map((item, index) => {
            if(item.type === 'GROUP'){
                this.updateAnalysis(item, newGroup.children[index]);
            }else{
                item.richLayerId = newGroup.children[index].richLayerId;
            }
        });
    }

    excluirProcessos(viewer){
        let groups = this.getGroups(viewer);

        for(let item of groups) {
            for(let i in this._processosParaExcluir) {
                if(this._processosParaExcluir[i].NU_PROCESSO_IBAMA === item._id) {
                    setTimeout(mapsInstance.moduleInstance.$viewerAction.deleteGroup.bind(mapsInstance.moduleInstance.$viewerAction, item._id), 0);
                    break;
                }
            }
        }
    }

    getProcessosParaExcluir(){
        return this._oldProcessos.filter((item) => {
            let canFilter = true;
            for(let processo of this._processosVisualizadosNoMapa){
                if(processo.NU_PROCESSO_IBAMA === item.NU_PROCESSO_IBAMA){
                    canFilter = false;
                    break;
                }
            }

            return canFilter;
        });
    }

    getGroups(viewer){
        return viewer.toolsConfig.analysisConfig[0].children.map((item) => {
            return item;
        });
    }

    render() {
        return (
            <section style={{
                width : '100%',
                height : '100%',
                display : 'flex',
                flexDirection : 'column',
                position: 'relative'
            }}>
                <div>
                    <NavBar onLoad={this.onCreateViewer} />
                </div>

                <div style={{
                    display: 'flex',
                    alignContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    textAlign: 'center'
                }}>
                    <div style={{width: '100%'}}>{this.state.centralMessage}</div>
                </div>

                <div ref="mapsNode" style={{
                    width : '100%',
                    height : '100%',
                    position : 'relative',
                    zIndex : 10
                }} />
            </section>
        );
    }

}