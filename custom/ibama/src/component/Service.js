import axios from "axios";

const ConnectaMaps = window['ConnectaMaps'];

class IbamaService {

    static search(params) {
        return axios.get(ConnectaMaps.APP_CONFIG.serviceDomain + '/client/search', {params});
    }

    static tipologias() {
        return axios.get(ConnectaMaps.APP_CONFIG.serviceDomain + '/client/tipologias');
    }

    /**
     *
     * @param {String[]} processIds
     * @returns {*}
     */
    static createViewer(processIds) {
        return axios.get(ConnectaMaps.APP_CONFIG.serviceDomain + '/client/create-viewer', {
            params : {
                processIds : processIds.join(',')
            }
        }).then(({data}) => data);
    }

    static getTipologias() {
        return axios.get(ConnectaMaps.APP_CONFIG.serviceDomain + '/client/tipologias')
            .then(({data}) => data);
    }

}

export default IbamaService;