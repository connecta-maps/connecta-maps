 const FieldNames = (key = "") => {
    let map = {
        nomeDasFerrovias : "NOM_TRECHO",
        tiposDeOcorrencias : "NOM_TIPO_OCORRENCIA",
        lotes : "NOM_LOTE",
        gravidadesDaOcorrencia : "TIP_GRAVIDADE",
        status : "STA_ESTADO_OCORRENCIA"
    };

    return map[key] || map;
 };

export default FieldNames;