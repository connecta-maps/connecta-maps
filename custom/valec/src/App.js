import React, { Component } from 'react';
import update from "react-addons-update";
import './App.css';
import {Dialog, FlatButton, SelectField, MenuItem, TextField} from "material-ui";
import {FindFieldOptions} from "./helper/FindFieldOptions";
import FieldNames from "./constant/FieldNames";

const newFilter = (field, value) => {
    return {
        value,
        operator : "EQUAL",
        valueType : typeof value,
        field : FieldNames(field),
        externalFilter : true
    };
};

class App extends Component {

    _filtroAEditar = {};

    _filtroSalvo = {};

    _teste = [];

    state = {
        open: false
    };

    /**
     * @type {ConnectaMaps}
     */
    instanceMaps;

    constructor (props) {
        super(props);

        let fields = Object.keys(FieldNames());

        FindFieldOptions.getDistinctValues()
            .then((result) => {
                fields.forEach((field) => {
                    this.state[field] = result[FieldNames(field)];
                });
            });
    }

    onExtensionClick = (instanceMaps) => {
        this.instanceMaps = instanceMaps;

        this._filtroAEditar = this._filtroSalvo;

        this.setState({ open : true });
    };

    onCancel = () => {
        this.setState({open: false, filtro : {}});
    };

    onChangeFiltro = (nomedoFiltro, value) => {
        let filtro = update(this._filtroAEditar, {
            [nomedoFiltro] : {
                $set : value
            }
        });

        this._filtroAEditar = filtro;

        this.forceUpdate();

        this._teste.push(newFilter(nomedoFiltro, value));
    };

    buildCombo = (lista = [], nomedoFiltro, title) => {
        return (
            <SelectField
                style={{ width : '100%' }}
                floatingLabelText={title}
                value={this._filtroAEditar[nomedoFiltro]}
                onChange={(event, key, payload) => {
                    this.onChangeFiltro(nomedoFiltro, payload);
                }}
            >
                { lista.map((value, index) => {
                    return (
                        <MenuItem key={index} value={value} primaryText={value} />
                    );
                }) }
            </SelectField>
        );
    };

    onClickFilter = () => {
        this.setState({open: false});

        this._filtroSalvo = this._filtroAEditar;

        this.instanceMaps.setFilter(this._teste);
    };

    render() {
        const actions = [
            <FlatButton
                label="Cancelar"
                primary={true}
                onClick={this.onCancel}
            />,
            <FlatButton
                label="Filtrar"
                primary={true}
                keyboardFocused={true}
                onClick={this.onClickFilter.bind(this)}
            />,
        ];

        return (
            <div className="App">
                <Dialog
                    title="Filtrar Ocorrências"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.onCancel}
                >
                    <div className="c-row">
                        <div className="c-col-6">
                            {this.buildCombo(this.state.nomeDasFerrovias, 'nomeDasFerrovias', 'Nome da Ferrovia')}
                        </div>
                        <div className="c-col-6">
                            {this.buildCombo(this.state.tiposDeOcorrencias, 'tiposDeOcorrencias', 'Tipos de Ocorrência')}
                        </div>
                    </div>
                    <div className="c-row">
                        <div className="c-col-6">
                            {this.buildCombo(this.state.lotes, 'lotes', 'Lotes')}
                        </div>
                        <div className="c-col-6">
                            {this.buildCombo(this.state.gravidadesDaOcorrencia, 'gravidadesDaOcorrencia', 'Gravidades Da Ocorrencia')}
                        </div>
                    </div>
                    <div className="c-row">
                        <div className="c-col-6">
                            <TextField
                                type={"number"}
                                value={this._filtroAEditar.kmInicial}
                                onChange={(event, value) => {
                                    this.onChangeFiltro('kmInicial', value);
                                }}
                                floatingLabelText={"KM Inicial"}
                            />
                        </div>
                        <div className="c-col-6">
                            <TextField
                                type={"number"}
                                value={this._filtroAEditar.kmFinal}
                                onChange={(event, value) => {
                                    this.onChangeFiltro('kmFinal', value);
                                }}
                                floatingLabelText={"KM Final"}
                            />
                        </div>
                    </div>
                    <div className="c-row">
                        <div className="c-col-6">
                            {this.buildCombo(this.state.status, 'status', 'Status')}
                        </div>
                    </div>
                </Dialog>
            </div>
        );
    }
}

export default App;
